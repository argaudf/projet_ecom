package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.Address;
import com.mycompany.myapp.domain.Command;
import com.mycompany.myapp.domain.CommandProduct;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.repository.CommandRepository;
import com.mycompany.myapp.repository.ShopCartRepository;
import com.mycompany.myapp.repository.search.CommandSearchRepository;
import com.mycompany.myapp.service.AddressService;
import com.mycompany.myapp.service.CommandProductService;
import com.mycompany.myapp.service.CommandQueryService;
import com.mycompany.myapp.service.CommandService;
import com.mycompany.myapp.service.ShopCartService;
import com.mycompany.myapp.service.UserService;
import com.mycompany.myapp.service.dto.CommandDTO;
import com.mycompany.myapp.service.mapper.CommandMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

/**
 * Integration tests for the {@link CommandResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class CommandResourceIT {

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;
    private static final Integer SMALLER_STATUS = 1 - 1;

    @Autowired
    private CommandRepository commandRepository;

    @Autowired
    private CommandMapper commandMapper;

    @Autowired
    private CommandService commandService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.CommandSearchRepositoryMockConfiguration
     */
    @Autowired
    private CommandSearchRepository mockCommandSearchRepository;

    @Autowired
    private CommandQueryService commandQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;


    @Mock
    private UserService mockUserService;
    
    @Mock
    private ShopCartService mockShopCartService;
    
    @Mock
    private CommandProductService mockCommandProductService;
    
    @Mock
    private AddressService mockAddressService;

    @Mock
    private ShopCartRepository mockShopCartRepository;

    @Mock
    private CommandRepository mockCommandRepository;

    private MockMvc restCommandMockMvc;

    private Command command;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        

        final CommandResource commandResource = new CommandResource(commandService, commandQueryService, mockUserService, mockShopCartService, mockCommandProductService, mockAddressService, mockShopCartRepository, mockCommandRepository);
        this.restCommandMockMvc = MockMvcBuilders.standaloneSetup(commandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Command createEntity(EntityManager em) {
        Command command = new Command()
            .status(DEFAULT_STATUS);
        return command;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Command createUpdatedEntity(EntityManager em) {
        Command command = new Command()
            .status(UPDATED_STATUS);
        return command;
    }

    @BeforeEach
    public void initTest() {
        command = createEntity(em);
    }

    @Test
    @Transactional
    public void createCommand() throws Exception {
        int databaseSizeBeforeCreate = commandRepository.findAll().size();

        // Create the Command
        CommandDTO commandDTO = commandMapper.toDto(command);
        restCommandMockMvc.perform(post("/api/commands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandDTO)))
            .andExpect(status().isCreated());

        // Validate the Command in the database
        List<Command> commandList = commandRepository.findAll();
        assertThat(commandList).hasSize(databaseSizeBeforeCreate + 1);
        Command testCommand = commandList.get(commandList.size() - 1);
        assertThat(testCommand.getStatus()).isEqualTo(DEFAULT_STATUS);

        // Validate the Command in Elasticsearch
        verify(mockCommandSearchRepository, times(1)).save(testCommand);
    }

    @Test
    @Transactional
    public void createCommandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = commandRepository.findAll().size();

        // Create the Command with an existing ID
        command.setId(1L);
        CommandDTO commandDTO = commandMapper.toDto(command);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommandMockMvc.perform(post("/api/commands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Command in the database
        List<Command> commandList = commandRepository.findAll();
        assertThat(commandList).hasSize(databaseSizeBeforeCreate);

        // Validate the Command in Elasticsearch
        verify(mockCommandSearchRepository, times(0)).save(command);
    }


    @Test
    @Transactional
    public void getAllCommands() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList
        restCommandMockMvc.perform(get("/api/commands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(command.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }
    
    @Test
    @Transactional
    public void getCommand() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get the command
        restCommandMockMvc.perform(get("/api/commands/{id}", command.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(command.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getAllCommandsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList where status equals to DEFAULT_STATUS
        defaultCommandShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the commandList where status equals to UPDATED_STATUS
        defaultCommandShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCommandsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCommandShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the commandList where status equals to UPDATED_STATUS
        defaultCommandShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCommandsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList where status is not null
        defaultCommandShouldBeFound("status.specified=true");

        // Get all the commandList where status is null
        defaultCommandShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllCommandsByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList where status is greater than or equal to DEFAULT_STATUS
        defaultCommandShouldBeFound("status.greaterThanOrEqual=" + DEFAULT_STATUS);

        // Get all the commandList where status is greater than or equal to UPDATED_STATUS
        defaultCommandShouldNotBeFound("status.greaterThanOrEqual=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCommandsByStatusIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList where status is less than or equal to DEFAULT_STATUS
        defaultCommandShouldBeFound("status.lessThanOrEqual=" + DEFAULT_STATUS);

        // Get all the commandList where status is less than or equal to SMALLER_STATUS
        defaultCommandShouldNotBeFound("status.lessThanOrEqual=" + SMALLER_STATUS);
    }

    @Test
    @Transactional
    public void getAllCommandsByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList where status is less than DEFAULT_STATUS
        defaultCommandShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the commandList where status is less than UPDATED_STATUS
        defaultCommandShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCommandsByStatusIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        // Get all the commandList where status is greater than DEFAULT_STATUS
        defaultCommandShouldNotBeFound("status.greaterThan=" + DEFAULT_STATUS);

        // Get all the commandList where status is greater than SMALLER_STATUS
        defaultCommandShouldBeFound("status.greaterThan=" + SMALLER_STATUS);
    }


    @Test
    @Transactional
    public void getAllCommandsByEntryIsEqualToSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);
        CommandProduct entry = CommandProductResourceIT.createEntity(em);
        em.persist(entry);
        em.flush();
        command.addEntry(entry);
        commandRepository.saveAndFlush(command);
        Long entryId = entry.getId();

        // Get all the commandList where entry equals to entryId
        defaultCommandShouldBeFound("entryId.equals=" + entryId);

        // Get all the commandList where entry equals to entryId + 1
        defaultCommandShouldNotBeFound("entryId.equals=" + (entryId + 1));
    }


    @Test
    @Transactional
    public void getAllCommandsByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);
        UserInfo user = UserInfoResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        command.setUser(user);
        commandRepository.saveAndFlush(command);
        Long userId = user.getId();

        // Get all the commandList where user equals to userId
        defaultCommandShouldBeFound("userId.equals=" + userId);

        // Get all the commandList where user equals to userId + 1
        defaultCommandShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllCommandsByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);
        Address address = AddressResourceIT.createEntity(em);
        em.persist(address);
        em.flush();
        command.setAddress(address);
        commandRepository.saveAndFlush(command);
        Long addressId = address.getId();

        // Get all the commandList where address equals to addressId
        defaultCommandShouldBeFound("addressId.equals=" + addressId);

        // Get all the commandList where address equals to addressId + 1
        defaultCommandShouldNotBeFound("addressId.equals=" + (addressId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommandShouldBeFound(String filter) throws Exception {
        restCommandMockMvc.perform(get("/api/commands?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(command.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));

        // Check, that the count call also returns 1
        restCommandMockMvc.perform(get("/api/commands/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommandShouldNotBeFound(String filter) throws Exception {
        restCommandMockMvc.perform(get("/api/commands?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommandMockMvc.perform(get("/api/commands/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCommand() throws Exception {
        // Get the command
        restCommandMockMvc.perform(get("/api/commands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCommand() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        int databaseSizeBeforeUpdate = commandRepository.findAll().size();

        // Update the command
        Command updatedCommand = commandRepository.findById(command.getId()).get();
        // Disconnect from session so that the updates on updatedCommand are not directly saved in db
        em.detach(updatedCommand);
        updatedCommand
            .status(UPDATED_STATUS);
        CommandDTO commandDTO = commandMapper.toDto(updatedCommand);

        restCommandMockMvc.perform(put("/api/commands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandDTO)))
            .andExpect(status().isOk());

        // Validate the Command in the database
        List<Command> commandList = commandRepository.findAll();
        assertThat(commandList).hasSize(databaseSizeBeforeUpdate);
        Command testCommand = commandList.get(commandList.size() - 1);
        assertThat(testCommand.getStatus()).isEqualTo(UPDATED_STATUS);

        // Validate the Command in Elasticsearch
        verify(mockCommandSearchRepository, times(1)).save(testCommand);
    }

    @Test
    @Transactional
    public void updateNonExistingCommand() throws Exception {
        int databaseSizeBeforeUpdate = commandRepository.findAll().size();

        // Create the Command
        CommandDTO commandDTO = commandMapper.toDto(command);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommandMockMvc.perform(put("/api/commands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Command in the database
        List<Command> commandList = commandRepository.findAll();
        assertThat(commandList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Command in Elasticsearch
        verify(mockCommandSearchRepository, times(0)).save(command);
    }

    @Test
    @Transactional
    public void deleteCommand() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);

        int databaseSizeBeforeDelete = commandRepository.findAll().size();

        // Delete the command
        restCommandMockMvc.perform(delete("/api/commands/{id}", command.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Command> commandList = commandRepository.findAll();
        assertThat(commandList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Command in Elasticsearch
        verify(mockCommandSearchRepository, times(1)).deleteById(command.getId());
    }

    @Test
    @Transactional
    public void searchCommand() throws Exception {
        // Initialize the database
        commandRepository.saveAndFlush(command);
        when(mockCommandSearchRepository.search(queryStringQuery("id:" + command.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(command), PageRequest.of(0, 1), 1));
        // Search the command
        restCommandMockMvc.perform(get("/api/_search/commands?query=id:" + command.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(command.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Command.class);
        Command command1 = new Command();
        command1.setId(1L);
        Command command2 = new Command();
        command2.setId(command1.getId());
        assertThat(command1).isEqualTo(command2);
        command2.setId(2L);
        assertThat(command1).isNotEqualTo(command2);
        command1.setId(null);
        assertThat(command1).isNotEqualTo(command2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommandDTO.class);
        CommandDTO commandDTO1 = new CommandDTO();
        commandDTO1.setId(1L);
        CommandDTO commandDTO2 = new CommandDTO();
        assertThat(commandDTO1).isNotEqualTo(commandDTO2);
        commandDTO2.setId(commandDTO1.getId());
        assertThat(commandDTO1).isEqualTo(commandDTO2);
        commandDTO2.setId(2L);
        assertThat(commandDTO1).isNotEqualTo(commandDTO2);
        commandDTO1.setId(null);
        assertThat(commandDTO1).isNotEqualTo(commandDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(commandMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(commandMapper.fromId(null)).isNull();
    }
}

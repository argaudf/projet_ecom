package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.domain.Brand;
import com.mycompany.myapp.domain.CommandProduct;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.WishList;
import com.mycompany.myapp.domain.Cpu;
import com.mycompany.myapp.domain.Harddrive;
import com.mycompany.myapp.repository.ProductRepository;
import com.mycompany.myapp.repository.search.ProductSearchRepository;
import com.mycompany.myapp.service.ProductService;
import com.mycompany.myapp.service.dto.ProductDTO;
import com.mycompany.myapp.service.mapper.ProductMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;
import com.mycompany.myapp.service.dto.ProductCriteria;
import com.mycompany.myapp.service.ProductQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class ProductResourceIT {

    private static final String DEFAULT_PRO_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PRO_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_PRO_PRICE = 1D;
    private static final Double UPDATED_PRO_PRICE = 2D;
    private static final Double SMALLER_PRO_PRICE = 1D - 1D;

    private static final Integer DEFAULT_STOCK = 1;
    private static final Integer UPDATED_STOCK = 2;
    private static final Integer SMALLER_STOCK = 1 - 1;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_PICTURE = "AAAAAAAAAA";
    private static final String UPDATED_PICTURE = "BBBBBBBBBB";

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductService productService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.ProductSearchRepositoryMockConfiguration
     */
    @Autowired
    private ProductSearchRepository mockProductSearchRepository;

    @Autowired
    private ProductQueryService productQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProductMockMvc;

    private Product product;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductResource productResource = new ProductResource(productService, productQueryService);
        this.restProductMockMvc = MockMvcBuilders.standaloneSetup(productResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Product createEntity(EntityManager em) {
        Product product = new Product()
            .proName(DEFAULT_PRO_NAME)
            .proPrice(DEFAULT_PRO_PRICE)
            .stock(DEFAULT_STOCK)
            .description(DEFAULT_DESCRIPTION)
            .picture(DEFAULT_PICTURE);
        return product;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Product createUpdatedEntity(EntityManager em) {
        Product product = new Product()
            .proName(UPDATED_PRO_NAME)
            .proPrice(UPDATED_PRO_PRICE)
            .stock(UPDATED_STOCK)
            .description(UPDATED_DESCRIPTION)
            .picture(UPDATED_PICTURE);
        return product;
    }

    @BeforeEach
    public void initTest() {
        product = createEntity(em);
    }

    @Test
    @Transactional
    public void createProduct() throws Exception {
        int databaseSizeBeforeCreate = productRepository.findAll().size();

        // Create the Product
        ProductDTO productDTO = productMapper.toDto(product);
        restProductMockMvc.perform(post("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isCreated());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeCreate + 1);
        Product testProduct = productList.get(productList.size() - 1);
        assertThat(testProduct.getProName()).isEqualTo(DEFAULT_PRO_NAME);
        assertThat(testProduct.getProPrice()).isEqualTo(DEFAULT_PRO_PRICE);
        assertThat(testProduct.getStock()).isEqualTo(DEFAULT_STOCK);
        assertThat(testProduct.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProduct.getPicture()).isEqualTo(DEFAULT_PICTURE);

        // Validate the Product in Elasticsearch
        verify(mockProductSearchRepository, times(1)).save(testProduct);
    }

    @Test
    @Transactional
    public void createProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productRepository.findAll().size();

        // Create the Product with an existing ID
        product.setId(1L);
        ProductDTO productDTO = productMapper.toDto(product);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductMockMvc.perform(post("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeCreate);

        // Validate the Product in Elasticsearch
        verify(mockProductSearchRepository, times(0)).save(product);
    }


    @Test
    @Transactional
    public void getAllProducts() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].proName").value(hasItem(DEFAULT_PRO_NAME.toString())))
            .andExpect(jsonPath("$.[*].proPrice").value(hasItem(DEFAULT_PRO_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].stock").value(hasItem(DEFAULT_STOCK)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(DEFAULT_PICTURE.toString())));
    }
    
    @Test
    @Transactional
    public void getProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get the product
        restProductMockMvc.perform(get("/api/products/{id}", product.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(product.getId().intValue()))
            .andExpect(jsonPath("$.proName").value(DEFAULT_PRO_NAME.toString()))
            .andExpect(jsonPath("$.proPrice").value(DEFAULT_PRO_PRICE.doubleValue()))
            .andExpect(jsonPath("$.stock").value(DEFAULT_STOCK))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.picture").value(DEFAULT_PICTURE.toString()));
    }

    @Test
    @Transactional
    public void getAllProductsByProNameIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proName equals to DEFAULT_PRO_NAME
        defaultProductShouldBeFound("proName.equals=" + DEFAULT_PRO_NAME);

        // Get all the productList where proName equals to UPDATED_PRO_NAME
        defaultProductShouldNotBeFound("proName.equals=" + UPDATED_PRO_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByProNameIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proName in DEFAULT_PRO_NAME or UPDATED_PRO_NAME
        defaultProductShouldBeFound("proName.in=" + DEFAULT_PRO_NAME + "," + UPDATED_PRO_NAME);

        // Get all the productList where proName equals to UPDATED_PRO_NAME
        defaultProductShouldNotBeFound("proName.in=" + UPDATED_PRO_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByProNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proName is not null
        defaultProductShouldBeFound("proName.specified=true");

        // Get all the productList where proName is null
        defaultProductShouldNotBeFound("proName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByProPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proPrice equals to DEFAULT_PRO_PRICE
        defaultProductShouldBeFound("proPrice.equals=" + DEFAULT_PRO_PRICE);

        // Get all the productList where proPrice equals to UPDATED_PRO_PRICE
        defaultProductShouldNotBeFound("proPrice.equals=" + UPDATED_PRO_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByProPriceIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proPrice in DEFAULT_PRO_PRICE or UPDATED_PRO_PRICE
        defaultProductShouldBeFound("proPrice.in=" + DEFAULT_PRO_PRICE + "," + UPDATED_PRO_PRICE);

        // Get all the productList where proPrice equals to UPDATED_PRO_PRICE
        defaultProductShouldNotBeFound("proPrice.in=" + UPDATED_PRO_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByProPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proPrice is not null
        defaultProductShouldBeFound("proPrice.specified=true");

        // Get all the productList where proPrice is null
        defaultProductShouldNotBeFound("proPrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByProPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proPrice is greater than or equal to DEFAULT_PRO_PRICE
        defaultProductShouldBeFound("proPrice.greaterThanOrEqual=" + DEFAULT_PRO_PRICE);

        // Get all the productList where proPrice is greater than or equal to UPDATED_PRO_PRICE
        defaultProductShouldNotBeFound("proPrice.greaterThanOrEqual=" + UPDATED_PRO_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByProPriceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proPrice is less than or equal to DEFAULT_PRO_PRICE
        defaultProductShouldBeFound("proPrice.lessThanOrEqual=" + DEFAULT_PRO_PRICE);

        // Get all the productList where proPrice is less than or equal to SMALLER_PRO_PRICE
        defaultProductShouldNotBeFound("proPrice.lessThanOrEqual=" + SMALLER_PRO_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByProPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proPrice is less than DEFAULT_PRO_PRICE
        defaultProductShouldNotBeFound("proPrice.lessThan=" + DEFAULT_PRO_PRICE);

        // Get all the productList where proPrice is less than UPDATED_PRO_PRICE
        defaultProductShouldBeFound("proPrice.lessThan=" + UPDATED_PRO_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByProPriceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where proPrice is greater than DEFAULT_PRO_PRICE
        defaultProductShouldNotBeFound("proPrice.greaterThan=" + DEFAULT_PRO_PRICE);

        // Get all the productList where proPrice is greater than SMALLER_PRO_PRICE
        defaultProductShouldBeFound("proPrice.greaterThan=" + SMALLER_PRO_PRICE);
    }


    @Test
    @Transactional
    public void getAllProductsByStockIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock equals to DEFAULT_STOCK
        defaultProductShouldBeFound("stock.equals=" + DEFAULT_STOCK);

        // Get all the productList where stock equals to UPDATED_STOCK
        defaultProductShouldNotBeFound("stock.equals=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock in DEFAULT_STOCK or UPDATED_STOCK
        defaultProductShouldBeFound("stock.in=" + DEFAULT_STOCK + "," + UPDATED_STOCK);

        // Get all the productList where stock equals to UPDATED_STOCK
        defaultProductShouldNotBeFound("stock.in=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is not null
        defaultProductShouldBeFound("stock.specified=true");

        // Get all the productList where stock is null
        defaultProductShouldNotBeFound("stock.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is greater than or equal to DEFAULT_STOCK
        defaultProductShouldBeFound("stock.greaterThanOrEqual=" + DEFAULT_STOCK);

        // Get all the productList where stock is greater than or equal to UPDATED_STOCK
        defaultProductShouldNotBeFound("stock.greaterThanOrEqual=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is less than or equal to DEFAULT_STOCK
        defaultProductShouldBeFound("stock.lessThanOrEqual=" + DEFAULT_STOCK);

        // Get all the productList where stock is less than or equal to SMALLER_STOCK
        defaultProductShouldNotBeFound("stock.lessThanOrEqual=" + SMALLER_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is less than DEFAULT_STOCK
        defaultProductShouldNotBeFound("stock.lessThan=" + DEFAULT_STOCK);

        // Get all the productList where stock is less than UPDATED_STOCK
        defaultProductShouldBeFound("stock.lessThan=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsGreaterThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is greater than DEFAULT_STOCK
        defaultProductShouldNotBeFound("stock.greaterThan=" + DEFAULT_STOCK);

        // Get all the productList where stock is greater than SMALLER_STOCK
        defaultProductShouldBeFound("stock.greaterThan=" + SMALLER_STOCK);
    }


    @Test
    @Transactional
    public void getAllProductsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description equals to DEFAULT_DESCRIPTION
        defaultProductShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the productList where description equals to UPDATED_DESCRIPTION
        defaultProductShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultProductShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the productList where description equals to UPDATED_DESCRIPTION
        defaultProductShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description is not null
        defaultProductShouldBeFound("description.specified=true");

        // Get all the productList where description is null
        defaultProductShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByPictureIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where picture equals to DEFAULT_PICTURE
        defaultProductShouldBeFound("picture.equals=" + DEFAULT_PICTURE);

        // Get all the productList where picture equals to UPDATED_PICTURE
        defaultProductShouldNotBeFound("picture.equals=" + UPDATED_PICTURE);
    }

    @Test
    @Transactional
    public void getAllProductsByPictureIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where picture in DEFAULT_PICTURE or UPDATED_PICTURE
        defaultProductShouldBeFound("picture.in=" + DEFAULT_PICTURE + "," + UPDATED_PICTURE);

        // Get all the productList where picture equals to UPDATED_PICTURE
        defaultProductShouldNotBeFound("picture.in=" + UPDATED_PICTURE);
    }

    @Test
    @Transactional
    public void getAllProductsByPictureIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where picture is not null
        defaultProductShouldBeFound("picture.specified=true");

        // Get all the productList where picture is null
        defaultProductShouldNotBeFound("picture.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByBrandIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        Brand brand = BrandResourceIT.createEntity(em);
        em.persist(brand);
        em.flush();
        product.setBrand(brand);
        productRepository.saveAndFlush(product);
        Long brandId = brand.getId();

        // Get all the productList where brand equals to brandId
        defaultProductShouldBeFound("brandId.equals=" + brandId);

        // Get all the productList where brand equals to brandId + 1
        defaultProductShouldNotBeFound("brandId.equals=" + (brandId + 1));
    }


    @Test
    @Transactional
    public void getAllProductsByCommandEntryIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        CommandProduct commandEntry = CommandProductResourceIT.createEntity(em);
        em.persist(commandEntry);
        em.flush();
        product.addCommandEntry(commandEntry);
        productRepository.saveAndFlush(product);
        Long commandEntryId = commandEntry.getId();

        // Get all the productList where commandEntry equals to commandEntryId
        defaultProductShouldBeFound("commandEntryId.equals=" + commandEntryId);

        // Get all the productList where commandEntry equals to commandEntryId + 1
        defaultProductShouldNotBeFound("commandEntryId.equals=" + (commandEntryId + 1));
    }


    @Test
    @Transactional
    public void getAllProductsByShopcartIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        ShopCart shopcart = ShopCartResourceIT.createEntity(em);
        em.persist(shopcart);
        em.flush();
        product.addShopcart(shopcart);
        productRepository.saveAndFlush(product);
        Long shopcartId = shopcart.getId();

        // Get all the productList where shopcart equals to shopcartId
        defaultProductShouldBeFound("shopcartId.equals=" + shopcartId);

        // Get all the productList where shopcart equals to shopcartId + 1
        defaultProductShouldNotBeFound("shopcartId.equals=" + (shopcartId + 1));
    }


    @Test
    @Transactional
    public void getAllProductsByWishlistIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        WishList wishlist = WishListResourceIT.createEntity(em);
        em.persist(wishlist);
        em.flush();
        product.addWishlist(wishlist);
        productRepository.saveAndFlush(product);
        Long wishlistId = wishlist.getId();

        // Get all the productList where wishlist equals to wishlistId
        defaultProductShouldBeFound("wishlistId.equals=" + wishlistId);

        // Get all the productList where wishlist equals to wishlistId + 1
        defaultProductShouldNotBeFound("wishlistId.equals=" + (wishlistId + 1));
    }


    @Test
    @Transactional
    public void getAllProductsByCpuIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        Cpu cpu = CpuResourceIT.createEntity(em);
        em.persist(cpu);
        em.flush();
        product.setCpu(cpu);
        cpu.addProduct(product);
        productRepository.saveAndFlush(product);
        Long cpuId = cpu.getId();

        // Get all the productList where cpu equals to cpuId
        defaultProductShouldBeFound("cpuId.equals=" + cpuId);

        // Get all the productList where cpu equals to cpuId + 1
        defaultProductShouldNotBeFound("cpuId.equals=" + (cpuId + 1));
    }


    @Test
    @Transactional
    public void getAllProductsByHdIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        Harddrive hd = HarddriveResourceIT.createEntity(em);
        em.persist(hd);
        em.flush();
        product.addHd(hd);
        productRepository.saveAndFlush(product);
        Long hdId = hd.getId();

        // Get all the productList where hd equals to hdId
        defaultProductShouldBeFound("hdId.equals=" + hdId);

        // Get all the productList where hd equals to hdId + 1
        defaultProductShouldNotBeFound("hdId.equals=" + (hdId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProductShouldBeFound(String filter) throws Exception {
        restProductMockMvc.perform(get("/api/products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].proName").value(hasItem(DEFAULT_PRO_NAME)))
            .andExpect(jsonPath("$.[*].proPrice").value(hasItem(DEFAULT_PRO_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].stock").value(hasItem(DEFAULT_STOCK)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(DEFAULT_PICTURE)));

        // Check, that the count call also returns 1
        restProductMockMvc.perform(get("/api/products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProductShouldNotBeFound(String filter) throws Exception {
        restProductMockMvc.perform(get("/api/products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProductMockMvc.perform(get("/api/products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProduct() throws Exception {
        // Get the product
        restProductMockMvc.perform(get("/api/products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        int databaseSizeBeforeUpdate = productRepository.findAll().size();

        // Update the product
        Product updatedProduct = productRepository.findById(product.getId()).get();
        // Disconnect from session so that the updates on updatedProduct are not directly saved in db
        em.detach(updatedProduct);
        updatedProduct
            .proName(UPDATED_PRO_NAME)
            .proPrice(UPDATED_PRO_PRICE)
            .stock(UPDATED_STOCK)
            .description(UPDATED_DESCRIPTION)
            .picture(UPDATED_PICTURE);
        ProductDTO productDTO = productMapper.toDto(updatedProduct);

        restProductMockMvc.perform(put("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isOk());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeUpdate);
        Product testProduct = productList.get(productList.size() - 1);
        assertThat(testProduct.getProName()).isEqualTo(UPDATED_PRO_NAME);
        assertThat(testProduct.getProPrice()).isEqualTo(UPDATED_PRO_PRICE);
        assertThat(testProduct.getStock()).isEqualTo(UPDATED_STOCK);
        assertThat(testProduct.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProduct.getPicture()).isEqualTo(UPDATED_PICTURE);

        // Validate the Product in Elasticsearch
        verify(mockProductSearchRepository, times(1)).save(testProduct);
    }

    @Test
    @Transactional
    public void updateNonExistingProduct() throws Exception {
        int databaseSizeBeforeUpdate = productRepository.findAll().size();

        // Create the Product
        ProductDTO productDTO = productMapper.toDto(product);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductMockMvc.perform(put("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Product in Elasticsearch
        verify(mockProductSearchRepository, times(0)).save(product);
    }

    @Test
    @Transactional
    public void deleteProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        int databaseSizeBeforeDelete = productRepository.findAll().size();

        // Delete the product
        restProductMockMvc.perform(delete("/api/products/{id}", product.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Product in Elasticsearch
        verify(mockProductSearchRepository, times(1)).deleteById(product.getId());
    }

    @Test
    @Transactional
    public void searchProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        when(mockProductSearchRepository.search(queryStringQuery("id:" + product.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(product), PageRequest.of(0, 1), 1));
        // Search the product
        restProductMockMvc.perform(get("/api/_search/products?query=id:" + product.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].proName").value(hasItem(DEFAULT_PRO_NAME)))
            .andExpect(jsonPath("$.[*].proPrice").value(hasItem(DEFAULT_PRO_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].stock").value(hasItem(DEFAULT_STOCK)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(DEFAULT_PICTURE)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Product.class);
        Product product1 = new Product();
        product1.setId(1L);
        Product product2 = new Product();
        product2.setId(product1.getId());
        assertThat(product1).isEqualTo(product2);
        product2.setId(2L);
        assertThat(product1).isNotEqualTo(product2);
        product1.setId(null);
        assertThat(product1).isNotEqualTo(product2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductDTO.class);
        ProductDTO productDTO1 = new ProductDTO();
        productDTO1.setId(1L);
        ProductDTO productDTO2 = new ProductDTO();
        assertThat(productDTO1).isNotEqualTo(productDTO2);
        productDTO2.setId(productDTO1.getId());
        assertThat(productDTO1).isEqualTo(productDTO2);
        productDTO2.setId(2L);
        assertThat(productDTO1).isNotEqualTo(productDTO2);
        productDTO1.setId(null);
        assertThat(productDTO1).isNotEqualTo(productDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(productMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(productMapper.fromId(null)).isNull();
    }
}

package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.repository.ShopCartRepository;
import com.mycompany.myapp.repository.search.ShopCartSearchRepository;
import com.mycompany.myapp.service.ShopCartService;
import com.mycompany.myapp.service.UserService;
import com.mycompany.myapp.service.dto.ShopCartDTO;
import com.mycompany.myapp.service.mapper.ShopCartMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;
import com.mycompany.myapp.service.dto.ShopCartCriteria;
import com.mycompany.myapp.service.ProductService;
import com.mycompany.myapp.service.ShopCartQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShopCartResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class ShopCartResourceIT {

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;
    private static final Integer SMALLER_QUANTITY = 1 - 1;

    private static final LocalDate DEFAULT_ADD_TIME = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ADD_TIME = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_ADD_TIME = LocalDate.ofEpochDay(-1L);

    @Autowired
    private ShopCartRepository shopCartRepository;

    @Autowired
    private ShopCartMapper shopCartMapper;

    @Autowired
    private ShopCartService shopCartService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.ShopCartSearchRepositoryMockConfiguration
     */
    @Autowired
    private ShopCartSearchRepository mockShopCartSearchRepository;

    @Autowired
    private ShopCartQueryService shopCartQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;
    
    @Mock
    private UserService mockUserService;
    
    @Mock
    private ShopCartRepository mockShopCartRepository;

    private MockMvc restShopCartMockMvc;

    private ShopCart shopCart;
    
    @Mock
    private ProductResource mockProductResource;
    
    @Mock
    private ProductService mockProductService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShopCartResource shopCartResource = new ShopCartResource(shopCartService, shopCartQueryService, mockUserService, mockShopCartRepository, mockProductResource, mockProductService);
        this.restShopCartMockMvc = MockMvcBuilders.standaloneSetup(shopCartResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopCart createEntity(EntityManager em) {
        ShopCart shopCart = new ShopCart()
            .quantity(DEFAULT_QUANTITY)
            .addTime(DEFAULT_ADD_TIME);
        return shopCart;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopCart createUpdatedEntity(EntityManager em) {
        ShopCart shopCart = new ShopCart()
            .quantity(UPDATED_QUANTITY)
            .addTime(UPDATED_ADD_TIME);
        return shopCart;
    }

    @BeforeEach
    public void initTest() {
        shopCart = createEntity(em);
    }

    @Test
    @Transactional
    public void createShopCart() throws Exception {
        int databaseSizeBeforeCreate = shopCartRepository.findAll().size();

        // Create the ShopCart
        ShopCartDTO shopCartDTO = shopCartMapper.toDto(shopCart);
        restShopCartMockMvc.perform(post("/api/shop-carts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopCartDTO)))
            .andExpect(status().isCreated());

        // Validate the ShopCart in the database
        List<ShopCart> shopCartList = shopCartRepository.findAll();
        assertThat(shopCartList).hasSize(databaseSizeBeforeCreate + 1);
        ShopCart testShopCart = shopCartList.get(shopCartList.size() - 1);
        assertThat(testShopCart.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testShopCart.getAddTime()).isEqualTo(DEFAULT_ADD_TIME);

        // Validate the ShopCart in Elasticsearch
        verify(mockShopCartSearchRepository, times(1)).save(testShopCart);
    }

    @Test
    @Transactional
    public void createShopCartWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shopCartRepository.findAll().size();

        // Create the ShopCart with an existing ID
        shopCart.setId(1L);
        ShopCartDTO shopCartDTO = shopCartMapper.toDto(shopCart);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShopCartMockMvc.perform(post("/api/shop-carts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopCartDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShopCart in the database
        List<ShopCart> shopCartList = shopCartRepository.findAll();
        assertThat(shopCartList).hasSize(databaseSizeBeforeCreate);

        // Validate the ShopCart in Elasticsearch
        verify(mockShopCartSearchRepository, times(0)).save(shopCart);
    }


    @Test
    @Transactional
    public void getAllShopCarts() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList
        restShopCartMockMvc.perform(get("/api/shop-carts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopCart.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].addTime").value(hasItem(DEFAULT_ADD_TIME.toString())));
    }
    
    @Test
    @Transactional
    public void getShopCart() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get the shopCart
        restShopCartMockMvc.perform(get("/api/shop-carts/{id}", shopCart.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shopCart.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.addTime").value(DEFAULT_ADD_TIME.toString()));
    }

    @Test
    @Transactional
    public void getAllShopCartsByQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where quantity equals to DEFAULT_QUANTITY
        defaultShopCartShouldBeFound("quantity.equals=" + DEFAULT_QUANTITY);

        // Get all the shopCartList where quantity equals to UPDATED_QUANTITY
        defaultShopCartShouldNotBeFound("quantity.equals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShopCartsByQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where quantity in DEFAULT_QUANTITY or UPDATED_QUANTITY
        defaultShopCartShouldBeFound("quantity.in=" + DEFAULT_QUANTITY + "," + UPDATED_QUANTITY);

        // Get all the shopCartList where quantity equals to UPDATED_QUANTITY
        defaultShopCartShouldNotBeFound("quantity.in=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShopCartsByQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where quantity is not null
        defaultShopCartShouldBeFound("quantity.specified=true");

        // Get all the shopCartList where quantity is null
        defaultShopCartShouldNotBeFound("quantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopCartsByQuantityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where quantity is greater than or equal to DEFAULT_QUANTITY
        defaultShopCartShouldBeFound("quantity.greaterThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the shopCartList where quantity is greater than or equal to UPDATED_QUANTITY
        defaultShopCartShouldNotBeFound("quantity.greaterThanOrEqual=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShopCartsByQuantityIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where quantity is less than or equal to DEFAULT_QUANTITY
        defaultShopCartShouldBeFound("quantity.lessThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the shopCartList where quantity is less than or equal to SMALLER_QUANTITY
        defaultShopCartShouldNotBeFound("quantity.lessThanOrEqual=" + SMALLER_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShopCartsByQuantityIsLessThanSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where quantity is less than DEFAULT_QUANTITY
        defaultShopCartShouldNotBeFound("quantity.lessThan=" + DEFAULT_QUANTITY);

        // Get all the shopCartList where quantity is less than UPDATED_QUANTITY
        defaultShopCartShouldBeFound("quantity.lessThan=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShopCartsByQuantityIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where quantity is greater than DEFAULT_QUANTITY
        defaultShopCartShouldNotBeFound("quantity.greaterThan=" + DEFAULT_QUANTITY);

        // Get all the shopCartList where quantity is greater than SMALLER_QUANTITY
        defaultShopCartShouldBeFound("quantity.greaterThan=" + SMALLER_QUANTITY);
    }


    @Test
    @Transactional
    public void getAllShopCartsByAddTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where addTime equals to DEFAULT_ADD_TIME
        defaultShopCartShouldBeFound("addTime.equals=" + DEFAULT_ADD_TIME);

        // Get all the shopCartList where addTime equals to UPDATED_ADD_TIME
        defaultShopCartShouldNotBeFound("addTime.equals=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllShopCartsByAddTimeIsInShouldWork() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where addTime in DEFAULT_ADD_TIME or UPDATED_ADD_TIME
        defaultShopCartShouldBeFound("addTime.in=" + DEFAULT_ADD_TIME + "," + UPDATED_ADD_TIME);

        // Get all the shopCartList where addTime equals to UPDATED_ADD_TIME
        defaultShopCartShouldNotBeFound("addTime.in=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllShopCartsByAddTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where addTime is not null
        defaultShopCartShouldBeFound("addTime.specified=true");

        // Get all the shopCartList where addTime is null
        defaultShopCartShouldNotBeFound("addTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopCartsByAddTimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where addTime is greater than or equal to DEFAULT_ADD_TIME
        defaultShopCartShouldBeFound("addTime.greaterThanOrEqual=" + DEFAULT_ADD_TIME);

        // Get all the shopCartList where addTime is greater than or equal to UPDATED_ADD_TIME
        defaultShopCartShouldNotBeFound("addTime.greaterThanOrEqual=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllShopCartsByAddTimeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where addTime is less than or equal to DEFAULT_ADD_TIME
        defaultShopCartShouldBeFound("addTime.lessThanOrEqual=" + DEFAULT_ADD_TIME);

        // Get all the shopCartList where addTime is less than or equal to SMALLER_ADD_TIME
        defaultShopCartShouldNotBeFound("addTime.lessThanOrEqual=" + SMALLER_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllShopCartsByAddTimeIsLessThanSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where addTime is less than DEFAULT_ADD_TIME
        defaultShopCartShouldNotBeFound("addTime.lessThan=" + DEFAULT_ADD_TIME);

        // Get all the shopCartList where addTime is less than UPDATED_ADD_TIME
        defaultShopCartShouldBeFound("addTime.lessThan=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllShopCartsByAddTimeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        // Get all the shopCartList where addTime is greater than DEFAULT_ADD_TIME
        defaultShopCartShouldNotBeFound("addTime.greaterThan=" + DEFAULT_ADD_TIME);

        // Get all the shopCartList where addTime is greater than SMALLER_ADD_TIME
        defaultShopCartShouldBeFound("addTime.greaterThan=" + SMALLER_ADD_TIME);
    }


    @Test
    @Transactional
    public void getAllShopCartsByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);
        UserInfo user = UserInfoResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        shopCart.setUser(user);
        shopCartRepository.saveAndFlush(shopCart);
        Long userId = user.getId();

        // Get all the shopCartList where user equals to userId
        defaultShopCartShouldBeFound("userId.equals=" + userId);

        // Get all the shopCartList where user equals to userId + 1
        defaultShopCartShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllShopCartsByProductIsEqualToSomething() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);
        Product product = ProductResourceIT.createEntity(em);
        em.persist(product);
        em.flush();
        shopCart.setProduct(product);
        shopCartRepository.saveAndFlush(shopCart);
        Long productId = product.getId();

        // Get all the shopCartList where product equals to productId
        defaultShopCartShouldBeFound("productId.equals=" + productId);

        // Get all the shopCartList where product equals to productId + 1
        defaultShopCartShouldNotBeFound("productId.equals=" + (productId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShopCartShouldBeFound(String filter) throws Exception {
        restShopCartMockMvc.perform(get("/api/shop-carts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopCart.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].addTime").value(hasItem(DEFAULT_ADD_TIME.toString())));

        // Check, that the count call also returns 1
        restShopCartMockMvc.perform(get("/api/shop-carts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShopCartShouldNotBeFound(String filter) throws Exception {
        restShopCartMockMvc.perform(get("/api/shop-carts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShopCartMockMvc.perform(get("/api/shop-carts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShopCart() throws Exception {
        // Get the shopCart
        restShopCartMockMvc.perform(get("/api/shop-carts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShopCart() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        int databaseSizeBeforeUpdate = shopCartRepository.findAll().size();

        // Update the shopCart
        ShopCart updatedShopCart = shopCartRepository.findById(shopCart.getId()).get();
        // Disconnect from session so that the updates on updatedShopCart are not directly saved in db
        em.detach(updatedShopCart);
        updatedShopCart
            .quantity(UPDATED_QUANTITY)
            .addTime(UPDATED_ADD_TIME);
        ShopCartDTO shopCartDTO = shopCartMapper.toDto(updatedShopCart);

        restShopCartMockMvc.perform(put("/api/shop-carts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopCartDTO)))
            .andExpect(status().isOk());

        // Validate the ShopCart in the database
        List<ShopCart> shopCartList = shopCartRepository.findAll();
        assertThat(shopCartList).hasSize(databaseSizeBeforeUpdate);
        ShopCart testShopCart = shopCartList.get(shopCartList.size() - 1);
        assertThat(testShopCart.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testShopCart.getAddTime()).isEqualTo(UPDATED_ADD_TIME);

        // Validate the ShopCart in Elasticsearch
        verify(mockShopCartSearchRepository, times(1)).save(testShopCart);
    }

    @Test
    @Transactional
    public void updateNonExistingShopCart() throws Exception {
        int databaseSizeBeforeUpdate = shopCartRepository.findAll().size();

        // Create the ShopCart
        ShopCartDTO shopCartDTO = shopCartMapper.toDto(shopCart);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopCartMockMvc.perform(put("/api/shop-carts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopCartDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShopCart in the database
        List<ShopCart> shopCartList = shopCartRepository.findAll();
        assertThat(shopCartList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ShopCart in Elasticsearch
        verify(mockShopCartSearchRepository, times(0)).save(shopCart);
    }

    @Test
    @Transactional
    public void deleteShopCart() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);

        int databaseSizeBeforeDelete = shopCartRepository.findAll().size();

        // Delete the shopCart
        restShopCartMockMvc.perform(delete("/api/shop-carts/{id}", shopCart.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShopCart> shopCartList = shopCartRepository.findAll();
        assertThat(shopCartList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ShopCart in Elasticsearch
        verify(mockShopCartSearchRepository, times(1)).deleteById(shopCart.getId());
    }

    @Test
    @Transactional
    public void searchShopCart() throws Exception {
        // Initialize the database
        shopCartRepository.saveAndFlush(shopCart);
        when(mockShopCartSearchRepository.search(queryStringQuery("id:" + shopCart.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(shopCart), PageRequest.of(0, 1), 1));
        // Search the shopCart
        restShopCartMockMvc.perform(get("/api/_search/shop-carts?query=id:" + shopCart.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopCart.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].addTime").value(hasItem(DEFAULT_ADD_TIME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopCart.class);
        ShopCart shopCart1 = new ShopCart();
        shopCart1.setId(1L);
        ShopCart shopCart2 = new ShopCart();
        shopCart2.setId(shopCart1.getId());
        assertThat(shopCart1).isEqualTo(shopCart2);
        shopCart2.setId(2L);
        assertThat(shopCart1).isNotEqualTo(shopCart2);
        shopCart1.setId(null);
        assertThat(shopCart1).isNotEqualTo(shopCart2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopCartDTO.class);
        ShopCartDTO shopCartDTO1 = new ShopCartDTO();
        shopCartDTO1.setId(1L);
        ShopCartDTO shopCartDTO2 = new ShopCartDTO();
        assertThat(shopCartDTO1).isNotEqualTo(shopCartDTO2);
        shopCartDTO2.setId(shopCartDTO1.getId());
        assertThat(shopCartDTO1).isEqualTo(shopCartDTO2);
        shopCartDTO2.setId(2L);
        assertThat(shopCartDTO1).isNotEqualTo(shopCartDTO2);
        shopCartDTO1.setId(null);
        assertThat(shopCartDTO1).isNotEqualTo(shopCartDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shopCartMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shopCartMapper.fromId(null)).isNull();
    }
}

package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.CommandProduct;
import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.domain.Command;
import com.mycompany.myapp.repository.CommandProductRepository;
import com.mycompany.myapp.repository.search.CommandProductSearchRepository;
import com.mycompany.myapp.service.CommandProductService;
import com.mycompany.myapp.service.dto.CommandProductDTO;
import com.mycompany.myapp.service.mapper.CommandProductMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;
import com.mycompany.myapp.service.dto.CommandProductCriteria;
import com.mycompany.myapp.service.CommandProductQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CommandProductResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class CommandProductResourceIT {

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;
    private static final Integer SMALLER_QUANTITY = 1 - 1;

    private static final LocalDate DEFAULT_ADD_TIME = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ADD_TIME = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_ADD_TIME = LocalDate.ofEpochDay(-1L);

    @Autowired
    private CommandProductRepository commandProductRepository;

    @Autowired
    private CommandProductMapper commandProductMapper;

    @Autowired
    private CommandProductService commandProductService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.CommandProductSearchRepositoryMockConfiguration
     */
    @Autowired
    private CommandProductSearchRepository mockCommandProductSearchRepository;

    @Autowired
    private CommandProductQueryService commandProductQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCommandProductMockMvc;

    private CommandProduct commandProduct;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CommandProductResource commandProductResource = new CommandProductResource(commandProductService, commandProductQueryService);
        this.restCommandProductMockMvc = MockMvcBuilders.standaloneSetup(commandProductResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommandProduct createEntity(EntityManager em) {
        CommandProduct commandProduct = new CommandProduct()
            .quantity(DEFAULT_QUANTITY)
            .addTime(DEFAULT_ADD_TIME);
        return commandProduct;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommandProduct createUpdatedEntity(EntityManager em) {
        CommandProduct commandProduct = new CommandProduct()
            .quantity(UPDATED_QUANTITY)
            .addTime(UPDATED_ADD_TIME);
        return commandProduct;
    }

    @BeforeEach
    public void initTest() {
        commandProduct = createEntity(em);
    }

    @Test
    @Transactional
    public void createCommandProduct() throws Exception {
        int databaseSizeBeforeCreate = commandProductRepository.findAll().size();

        // Create the CommandProduct
        CommandProductDTO commandProductDTO = commandProductMapper.toDto(commandProduct);
        restCommandProductMockMvc.perform(post("/api/command-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandProductDTO)))
            .andExpect(status().isCreated());

        // Validate the CommandProduct in the database
        List<CommandProduct> commandProductList = commandProductRepository.findAll();
        assertThat(commandProductList).hasSize(databaseSizeBeforeCreate + 1);
        CommandProduct testCommandProduct = commandProductList.get(commandProductList.size() - 1);
        assertThat(testCommandProduct.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testCommandProduct.getAddTime()).isEqualTo(DEFAULT_ADD_TIME);

        // Validate the CommandProduct in Elasticsearch
        verify(mockCommandProductSearchRepository, times(1)).save(testCommandProduct);
    }

    @Test
    @Transactional
    public void createCommandProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = commandProductRepository.findAll().size();

        // Create the CommandProduct with an existing ID
        commandProduct.setId(1L);
        CommandProductDTO commandProductDTO = commandProductMapper.toDto(commandProduct);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommandProductMockMvc.perform(post("/api/command-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandProductDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CommandProduct in the database
        List<CommandProduct> commandProductList = commandProductRepository.findAll();
        assertThat(commandProductList).hasSize(databaseSizeBeforeCreate);

        // Validate the CommandProduct in Elasticsearch
        verify(mockCommandProductSearchRepository, times(0)).save(commandProduct);
    }


    @Test
    @Transactional
    public void getAllCommandProducts() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList
        restCommandProductMockMvc.perform(get("/api/command-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commandProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].addTime").value(hasItem(DEFAULT_ADD_TIME.toString())));
    }
    
    @Test
    @Transactional
    public void getCommandProduct() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get the commandProduct
        restCommandProductMockMvc.perform(get("/api/command-products/{id}", commandProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(commandProduct.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.addTime").value(DEFAULT_ADD_TIME.toString()));
    }

    @Test
    @Transactional
    public void getAllCommandProductsByQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where quantity equals to DEFAULT_QUANTITY
        defaultCommandProductShouldBeFound("quantity.equals=" + DEFAULT_QUANTITY);

        // Get all the commandProductList where quantity equals to UPDATED_QUANTITY
        defaultCommandProductShouldNotBeFound("quantity.equals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where quantity in DEFAULT_QUANTITY or UPDATED_QUANTITY
        defaultCommandProductShouldBeFound("quantity.in=" + DEFAULT_QUANTITY + "," + UPDATED_QUANTITY);

        // Get all the commandProductList where quantity equals to UPDATED_QUANTITY
        defaultCommandProductShouldNotBeFound("quantity.in=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where quantity is not null
        defaultCommandProductShouldBeFound("quantity.specified=true");

        // Get all the commandProductList where quantity is null
        defaultCommandProductShouldNotBeFound("quantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllCommandProductsByQuantityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where quantity is greater than or equal to DEFAULT_QUANTITY
        defaultCommandProductShouldBeFound("quantity.greaterThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the commandProductList where quantity is greater than or equal to UPDATED_QUANTITY
        defaultCommandProductShouldNotBeFound("quantity.greaterThanOrEqual=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByQuantityIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where quantity is less than or equal to DEFAULT_QUANTITY
        defaultCommandProductShouldBeFound("quantity.lessThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the commandProductList where quantity is less than or equal to SMALLER_QUANTITY
        defaultCommandProductShouldNotBeFound("quantity.lessThanOrEqual=" + SMALLER_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByQuantityIsLessThanSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where quantity is less than DEFAULT_QUANTITY
        defaultCommandProductShouldNotBeFound("quantity.lessThan=" + DEFAULT_QUANTITY);

        // Get all the commandProductList where quantity is less than UPDATED_QUANTITY
        defaultCommandProductShouldBeFound("quantity.lessThan=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByQuantityIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where quantity is greater than DEFAULT_QUANTITY
        defaultCommandProductShouldNotBeFound("quantity.greaterThan=" + DEFAULT_QUANTITY);

        // Get all the commandProductList where quantity is greater than SMALLER_QUANTITY
        defaultCommandProductShouldBeFound("quantity.greaterThan=" + SMALLER_QUANTITY);
    }


    @Test
    @Transactional
    public void getAllCommandProductsByAddTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where addTime equals to DEFAULT_ADD_TIME
        defaultCommandProductShouldBeFound("addTime.equals=" + DEFAULT_ADD_TIME);

        // Get all the commandProductList where addTime equals to UPDATED_ADD_TIME
        defaultCommandProductShouldNotBeFound("addTime.equals=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByAddTimeIsInShouldWork() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where addTime in DEFAULT_ADD_TIME or UPDATED_ADD_TIME
        defaultCommandProductShouldBeFound("addTime.in=" + DEFAULT_ADD_TIME + "," + UPDATED_ADD_TIME);

        // Get all the commandProductList where addTime equals to UPDATED_ADD_TIME
        defaultCommandProductShouldNotBeFound("addTime.in=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByAddTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where addTime is not null
        defaultCommandProductShouldBeFound("addTime.specified=true");

        // Get all the commandProductList where addTime is null
        defaultCommandProductShouldNotBeFound("addTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllCommandProductsByAddTimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where addTime is greater than or equal to DEFAULT_ADD_TIME
        defaultCommandProductShouldBeFound("addTime.greaterThanOrEqual=" + DEFAULT_ADD_TIME);

        // Get all the commandProductList where addTime is greater than or equal to UPDATED_ADD_TIME
        defaultCommandProductShouldNotBeFound("addTime.greaterThanOrEqual=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByAddTimeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where addTime is less than or equal to DEFAULT_ADD_TIME
        defaultCommandProductShouldBeFound("addTime.lessThanOrEqual=" + DEFAULT_ADD_TIME);

        // Get all the commandProductList where addTime is less than or equal to SMALLER_ADD_TIME
        defaultCommandProductShouldNotBeFound("addTime.lessThanOrEqual=" + SMALLER_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByAddTimeIsLessThanSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where addTime is less than DEFAULT_ADD_TIME
        defaultCommandProductShouldNotBeFound("addTime.lessThan=" + DEFAULT_ADD_TIME);

        // Get all the commandProductList where addTime is less than UPDATED_ADD_TIME
        defaultCommandProductShouldBeFound("addTime.lessThan=" + UPDATED_ADD_TIME);
    }

    @Test
    @Transactional
    public void getAllCommandProductsByAddTimeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        // Get all the commandProductList where addTime is greater than DEFAULT_ADD_TIME
        defaultCommandProductShouldNotBeFound("addTime.greaterThan=" + DEFAULT_ADD_TIME);

        // Get all the commandProductList where addTime is greater than SMALLER_ADD_TIME
        defaultCommandProductShouldBeFound("addTime.greaterThan=" + SMALLER_ADD_TIME);
    }


    @Test
    @Transactional
    public void getAllCommandProductsByProductIsEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);
        Product product = ProductResourceIT.createEntity(em);
        em.persist(product);
        em.flush();
        commandProduct.setProduct(product);
        commandProductRepository.saveAndFlush(commandProduct);
        Long productId = product.getId();

        // Get all the commandProductList where product equals to productId
        defaultCommandProductShouldBeFound("productId.equals=" + productId);

        // Get all the commandProductList where product equals to productId + 1
        defaultCommandProductShouldNotBeFound("productId.equals=" + (productId + 1));
    }


    @Test
    @Transactional
    public void getAllCommandProductsByCommandIsEqualToSomething() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);
        Command command = CommandResourceIT.createEntity(em);
        em.persist(command);
        em.flush();
        commandProduct.setCommand(command);
        commandProductRepository.saveAndFlush(commandProduct);
        Long commandId = command.getId();

        // Get all the commandProductList where command equals to commandId
        defaultCommandProductShouldBeFound("commandId.equals=" + commandId);

        // Get all the commandProductList where command equals to commandId + 1
        defaultCommandProductShouldNotBeFound("commandId.equals=" + (commandId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommandProductShouldBeFound(String filter) throws Exception {
        restCommandProductMockMvc.perform(get("/api/command-products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commandProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].addTime").value(hasItem(DEFAULT_ADD_TIME.toString())));

        // Check, that the count call also returns 1
        restCommandProductMockMvc.perform(get("/api/command-products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommandProductShouldNotBeFound(String filter) throws Exception {
        restCommandProductMockMvc.perform(get("/api/command-products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommandProductMockMvc.perform(get("/api/command-products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCommandProduct() throws Exception {
        // Get the commandProduct
        restCommandProductMockMvc.perform(get("/api/command-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCommandProduct() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        int databaseSizeBeforeUpdate = commandProductRepository.findAll().size();

        // Update the commandProduct
        CommandProduct updatedCommandProduct = commandProductRepository.findById(commandProduct.getId()).get();
        // Disconnect from session so that the updates on updatedCommandProduct are not directly saved in db
        em.detach(updatedCommandProduct);
        updatedCommandProduct
            .quantity(UPDATED_QUANTITY)
            .addTime(UPDATED_ADD_TIME);
        CommandProductDTO commandProductDTO = commandProductMapper.toDto(updatedCommandProduct);

        restCommandProductMockMvc.perform(put("/api/command-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandProductDTO)))
            .andExpect(status().isOk());

        // Validate the CommandProduct in the database
        List<CommandProduct> commandProductList = commandProductRepository.findAll();
        assertThat(commandProductList).hasSize(databaseSizeBeforeUpdate);
        CommandProduct testCommandProduct = commandProductList.get(commandProductList.size() - 1);
        assertThat(testCommandProduct.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testCommandProduct.getAddTime()).isEqualTo(UPDATED_ADD_TIME);

        // Validate the CommandProduct in Elasticsearch
        verify(mockCommandProductSearchRepository, times(1)).save(testCommandProduct);
    }

    @Test
    @Transactional
    public void updateNonExistingCommandProduct() throws Exception {
        int databaseSizeBeforeUpdate = commandProductRepository.findAll().size();

        // Create the CommandProduct
        CommandProductDTO commandProductDTO = commandProductMapper.toDto(commandProduct);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommandProductMockMvc.perform(put("/api/command-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commandProductDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CommandProduct in the database
        List<CommandProduct> commandProductList = commandProductRepository.findAll();
        assertThat(commandProductList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CommandProduct in Elasticsearch
        verify(mockCommandProductSearchRepository, times(0)).save(commandProduct);
    }

    @Test
    @Transactional
    public void deleteCommandProduct() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);

        int databaseSizeBeforeDelete = commandProductRepository.findAll().size();

        // Delete the commandProduct
        restCommandProductMockMvc.perform(delete("/api/command-products/{id}", commandProduct.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommandProduct> commandProductList = commandProductRepository.findAll();
        assertThat(commandProductList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CommandProduct in Elasticsearch
        verify(mockCommandProductSearchRepository, times(1)).deleteById(commandProduct.getId());
    }

    @Test
    @Transactional
    public void searchCommandProduct() throws Exception {
        // Initialize the database
        commandProductRepository.saveAndFlush(commandProduct);
        when(mockCommandProductSearchRepository.search(queryStringQuery("id:" + commandProduct.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(commandProduct), PageRequest.of(0, 1), 1));
        // Search the commandProduct
        restCommandProductMockMvc.perform(get("/api/_search/command-products?query=id:" + commandProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commandProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].addTime").value(hasItem(DEFAULT_ADD_TIME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommandProduct.class);
        CommandProduct commandProduct1 = new CommandProduct();
        commandProduct1.setId(1L);
        CommandProduct commandProduct2 = new CommandProduct();
        commandProduct2.setId(commandProduct1.getId());
        assertThat(commandProduct1).isEqualTo(commandProduct2);
        commandProduct2.setId(2L);
        assertThat(commandProduct1).isNotEqualTo(commandProduct2);
        commandProduct1.setId(null);
        assertThat(commandProduct1).isNotEqualTo(commandProduct2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommandProductDTO.class);
        CommandProductDTO commandProductDTO1 = new CommandProductDTO();
        commandProductDTO1.setId(1L);
        CommandProductDTO commandProductDTO2 = new CommandProductDTO();
        assertThat(commandProductDTO1).isNotEqualTo(commandProductDTO2);
        commandProductDTO2.setId(commandProductDTO1.getId());
        assertThat(commandProductDTO1).isEqualTo(commandProductDTO2);
        commandProductDTO2.setId(2L);
        assertThat(commandProductDTO1).isNotEqualTo(commandProductDTO2);
        commandProductDTO1.setId(null);
        assertThat(commandProductDTO1).isNotEqualTo(commandProductDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(commandProductMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(commandProductMapper.fromId(null)).isNull();
    }
}

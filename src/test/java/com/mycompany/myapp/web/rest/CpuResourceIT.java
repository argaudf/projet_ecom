package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.Cpu;
import com.mycompany.myapp.repository.CpuRepository;
import com.mycompany.myapp.repository.search.CpuSearchRepository;
import com.mycompany.myapp.service.CpuService;
import com.mycompany.myapp.service.dto.CpuDTO;
import com.mycompany.myapp.service.mapper.CpuMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CpuResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class CpuResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_FREQUENCY = 1D;
    private static final Double UPDATED_FREQUENCY = 2D;
    private static final Double SMALLER_FREQUENCY = 1D - 1D;

    @Autowired
    private CpuRepository cpuRepository;

    @Autowired
    private CpuMapper cpuMapper;

    @Autowired
    private CpuService cpuService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.CpuSearchRepositoryMockConfiguration
     */
    @Autowired
    private CpuSearchRepository mockCpuSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCpuMockMvc;

    private Cpu cpu;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CpuResource cpuResource = new CpuResource(cpuService);
        this.restCpuMockMvc = MockMvcBuilders.standaloneSetup(cpuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cpu createEntity(EntityManager em) {
        Cpu cpu = new Cpu()
            .name(DEFAULT_NAME)
            .frequency(DEFAULT_FREQUENCY);
        return cpu;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cpu createUpdatedEntity(EntityManager em) {
        Cpu cpu = new Cpu()
            .name(UPDATED_NAME)
            .frequency(UPDATED_FREQUENCY);
        return cpu;
    }

    @BeforeEach
    public void initTest() {
        cpu = createEntity(em);
    }

    @Test
    @Transactional
    public void createCpu() throws Exception {
        int databaseSizeBeforeCreate = cpuRepository.findAll().size();

        // Create the Cpu
        CpuDTO cpuDTO = cpuMapper.toDto(cpu);
        restCpuMockMvc.perform(post("/api/cpus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cpuDTO)))
            .andExpect(status().isCreated());

        // Validate the Cpu in the database
        List<Cpu> cpuList = cpuRepository.findAll();
        assertThat(cpuList).hasSize(databaseSizeBeforeCreate + 1);
        Cpu testCpu = cpuList.get(cpuList.size() - 1);
        assertThat(testCpu.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCpu.getFrequency()).isEqualTo(DEFAULT_FREQUENCY);

        // Validate the Cpu in Elasticsearch
        verify(mockCpuSearchRepository, times(1)).save(testCpu);
    }

    @Test
    @Transactional
    public void createCpuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cpuRepository.findAll().size();

        // Create the Cpu with an existing ID
        cpu.setId(1L);
        CpuDTO cpuDTO = cpuMapper.toDto(cpu);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCpuMockMvc.perform(post("/api/cpus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cpuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cpu in the database
        List<Cpu> cpuList = cpuRepository.findAll();
        assertThat(cpuList).hasSize(databaseSizeBeforeCreate);

        // Validate the Cpu in Elasticsearch
        verify(mockCpuSearchRepository, times(0)).save(cpu);
    }


    @Test
    @Transactional
    public void getAllCpus() throws Exception {
        // Initialize the database
        cpuRepository.saveAndFlush(cpu);

        // Get all the cpuList
        restCpuMockMvc.perform(get("/api/cpus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cpu.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].frequency").value(hasItem(DEFAULT_FREQUENCY.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getCpu() throws Exception {
        // Initialize the database
        cpuRepository.saveAndFlush(cpu);

        // Get the cpu
        restCpuMockMvc.perform(get("/api/cpus/{id}", cpu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cpu.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.frequency").value(DEFAULT_FREQUENCY.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCpu() throws Exception {
        // Get the cpu
        restCpuMockMvc.perform(get("/api/cpus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCpu() throws Exception {
        // Initialize the database
        cpuRepository.saveAndFlush(cpu);

        int databaseSizeBeforeUpdate = cpuRepository.findAll().size();

        // Update the cpu
        Cpu updatedCpu = cpuRepository.findById(cpu.getId()).get();
        // Disconnect from session so that the updates on updatedCpu are not directly saved in db
        em.detach(updatedCpu);
        updatedCpu
            .name(UPDATED_NAME)
            .frequency(UPDATED_FREQUENCY);
        CpuDTO cpuDTO = cpuMapper.toDto(updatedCpu);

        restCpuMockMvc.perform(put("/api/cpus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cpuDTO)))
            .andExpect(status().isOk());

        // Validate the Cpu in the database
        List<Cpu> cpuList = cpuRepository.findAll();
        assertThat(cpuList).hasSize(databaseSizeBeforeUpdate);
        Cpu testCpu = cpuList.get(cpuList.size() - 1);
        assertThat(testCpu.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCpu.getFrequency()).isEqualTo(UPDATED_FREQUENCY);

        // Validate the Cpu in Elasticsearch
        verify(mockCpuSearchRepository, times(1)).save(testCpu);
    }

    @Test
    @Transactional
    public void updateNonExistingCpu() throws Exception {
        int databaseSizeBeforeUpdate = cpuRepository.findAll().size();

        // Create the Cpu
        CpuDTO cpuDTO = cpuMapper.toDto(cpu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCpuMockMvc.perform(put("/api/cpus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cpuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cpu in the database
        List<Cpu> cpuList = cpuRepository.findAll();
        assertThat(cpuList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Cpu in Elasticsearch
        verify(mockCpuSearchRepository, times(0)).save(cpu);
    }

    @Test
    @Transactional
    public void deleteCpu() throws Exception {
        // Initialize the database
        cpuRepository.saveAndFlush(cpu);

        int databaseSizeBeforeDelete = cpuRepository.findAll().size();

        // Delete the cpu
        restCpuMockMvc.perform(delete("/api/cpus/{id}", cpu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cpu> cpuList = cpuRepository.findAll();
        assertThat(cpuList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Cpu in Elasticsearch
        verify(mockCpuSearchRepository, times(1)).deleteById(cpu.getId());
    }

    @Test
    @Transactional
    public void searchCpu() throws Exception {
        // Initialize the database
        cpuRepository.saveAndFlush(cpu);
        when(mockCpuSearchRepository.search(queryStringQuery("id:" + cpu.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(cpu), PageRequest.of(0, 1), 1));
        // Search the cpu
        restCpuMockMvc.perform(get("/api/_search/cpus?query=id:" + cpu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cpu.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].frequency").value(hasItem(DEFAULT_FREQUENCY.doubleValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cpu.class);
        Cpu cpu1 = new Cpu();
        cpu1.setId(1L);
        Cpu cpu2 = new Cpu();
        cpu2.setId(cpu1.getId());
        assertThat(cpu1).isEqualTo(cpu2);
        cpu2.setId(2L);
        assertThat(cpu1).isNotEqualTo(cpu2);
        cpu1.setId(null);
        assertThat(cpu1).isNotEqualTo(cpu2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CpuDTO.class);
        CpuDTO cpuDTO1 = new CpuDTO();
        cpuDTO1.setId(1L);
        CpuDTO cpuDTO2 = new CpuDTO();
        assertThat(cpuDTO1).isNotEqualTo(cpuDTO2);
        cpuDTO2.setId(cpuDTO1.getId());
        assertThat(cpuDTO1).isEqualTo(cpuDTO2);
        cpuDTO2.setId(2L);
        assertThat(cpuDTO1).isNotEqualTo(cpuDTO2);
        cpuDTO1.setId(null);
        assertThat(cpuDTO1).isNotEqualTo(cpuDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(cpuMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(cpuMapper.fromId(null)).isNull();
    }
}

package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.domain.Command;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.Address;
import com.mycompany.myapp.repository.UserInfoRepository;
import com.mycompany.myapp.repository.search.UserInfoSearchRepository;
import com.mycompany.myapp.service.UserInfoService;
import com.mycompany.myapp.service.dto.UserInfoDTO;
import com.mycompany.myapp.service.mapper.UserInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;
import com.mycompany.myapp.service.dto.UserInfoCriteria;
import com.mycompany.myapp.service.UserInfoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserInfoResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class UserInfoResourceIT {

    private static final String DEFAULT_USER_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_USER_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_TEL = "AAAAAAAAAA";
    private static final String UPDATED_TEL = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD_Q = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD_Q = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD_A = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD_A = "BBBBBBBBBB";

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private UserInfoService userInfoService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.UserInfoSearchRepositoryMockConfiguration
     */
    @Autowired
    private UserInfoSearchRepository mockUserInfoSearchRepository;

    @Autowired
    private UserInfoQueryService userInfoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserInfoMockMvc;

    private UserInfo userInfo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserInfoResource userInfoResource = new UserInfoResource(userInfoService, userInfoQueryService);
        this.restUserInfoMockMvc = MockMvcBuilders.standaloneSetup(userInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserInfo createEntity(EntityManager em) {
        UserInfo userInfo = new UserInfo()
            .userStatus(DEFAULT_USER_STATUS)
            .tel(DEFAULT_TEL)
            .email(DEFAULT_EMAIL)
            .passwordQ(DEFAULT_PASSWORD_Q)
            .passwordA(DEFAULT_PASSWORD_A);
        return userInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserInfo createUpdatedEntity(EntityManager em) {
        UserInfo userInfo = new UserInfo()
            .userStatus(UPDATED_USER_STATUS)
            .tel(UPDATED_TEL)
            .email(UPDATED_EMAIL)
            .passwordQ(UPDATED_PASSWORD_Q)
            .passwordA(UPDATED_PASSWORD_A);
        return userInfo;
    }

    @BeforeEach
    public void initTest() {
        userInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserInfo() throws Exception {
        int databaseSizeBeforeCreate = userInfoRepository.findAll().size();

        // Create the UserInfo
        UserInfoDTO userInfoDTO = userInfoMapper.toDto(userInfo);
        restUserInfoMockMvc.perform(post("/api/user-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the UserInfo in the database
        List<UserInfo> userInfoList = userInfoRepository.findAll();
        assertThat(userInfoList).hasSize(databaseSizeBeforeCreate + 1);
        UserInfo testUserInfo = userInfoList.get(userInfoList.size() - 1);
        assertThat(testUserInfo.getUserStatus()).isEqualTo(DEFAULT_USER_STATUS);
        assertThat(testUserInfo.getTel()).isEqualTo(DEFAULT_TEL);
        assertThat(testUserInfo.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testUserInfo.getPasswordQ()).isEqualTo(DEFAULT_PASSWORD_Q);
        assertThat(testUserInfo.getPasswordA()).isEqualTo(DEFAULT_PASSWORD_A);

        // Validate the UserInfo in Elasticsearch
        verify(mockUserInfoSearchRepository, times(1)).save(testUserInfo);
    }

    @Test
    @Transactional
    public void createUserInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userInfoRepository.findAll().size();

        // Create the UserInfo with an existing ID
        userInfo.setId(1L);
        UserInfoDTO userInfoDTO = userInfoMapper.toDto(userInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserInfoMockMvc.perform(post("/api/user-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserInfo in the database
        List<UserInfo> userInfoList = userInfoRepository.findAll();
        assertThat(userInfoList).hasSize(databaseSizeBeforeCreate);

        // Validate the UserInfo in Elasticsearch
        verify(mockUserInfoSearchRepository, times(0)).save(userInfo);
    }


    @Test
    @Transactional
    public void getAllUserInfos() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList
        restUserInfoMockMvc.perform(get("/api/user-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].userStatus").value(hasItem(DEFAULT_USER_STATUS.toString())))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].passwordQ").value(hasItem(DEFAULT_PASSWORD_Q.toString())))
            .andExpect(jsonPath("$.[*].passwordA").value(hasItem(DEFAULT_PASSWORD_A.toString())));
    }
    
    @Test
    @Transactional
    public void getUserInfo() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get the userInfo
        restUserInfoMockMvc.perform(get("/api/user-infos/{id}", userInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userInfo.getId().intValue()))
            .andExpect(jsonPath("$.userStatus").value(DEFAULT_USER_STATUS.toString()))
            .andExpect(jsonPath("$.tel").value(DEFAULT_TEL.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.passwordQ").value(DEFAULT_PASSWORD_Q.toString()))
            .andExpect(jsonPath("$.passwordA").value(DEFAULT_PASSWORD_A.toString()));
    }

    @Test
    @Transactional
    public void getAllUserInfosByUserStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where userStatus equals to DEFAULT_USER_STATUS
        defaultUserInfoShouldBeFound("userStatus.equals=" + DEFAULT_USER_STATUS);

        // Get all the userInfoList where userStatus equals to UPDATED_USER_STATUS
        defaultUserInfoShouldNotBeFound("userStatus.equals=" + UPDATED_USER_STATUS);
    }

    @Test
    @Transactional
    public void getAllUserInfosByUserStatusIsInShouldWork() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where userStatus in DEFAULT_USER_STATUS or UPDATED_USER_STATUS
        defaultUserInfoShouldBeFound("userStatus.in=" + DEFAULT_USER_STATUS + "," + UPDATED_USER_STATUS);

        // Get all the userInfoList where userStatus equals to UPDATED_USER_STATUS
        defaultUserInfoShouldNotBeFound("userStatus.in=" + UPDATED_USER_STATUS);
    }

    @Test
    @Transactional
    public void getAllUserInfosByUserStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where userStatus is not null
        defaultUserInfoShouldBeFound("userStatus.specified=true");

        // Get all the userInfoList where userStatus is null
        defaultUserInfoShouldNotBeFound("userStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserInfosByTelIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where tel equals to DEFAULT_TEL
        defaultUserInfoShouldBeFound("tel.equals=" + DEFAULT_TEL);

        // Get all the userInfoList where tel equals to UPDATED_TEL
        defaultUserInfoShouldNotBeFound("tel.equals=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllUserInfosByTelIsInShouldWork() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where tel in DEFAULT_TEL or UPDATED_TEL
        defaultUserInfoShouldBeFound("tel.in=" + DEFAULT_TEL + "," + UPDATED_TEL);

        // Get all the userInfoList where tel equals to UPDATED_TEL
        defaultUserInfoShouldNotBeFound("tel.in=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllUserInfosByTelIsNullOrNotNull() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where tel is not null
        defaultUserInfoShouldBeFound("tel.specified=true");

        // Get all the userInfoList where tel is null
        defaultUserInfoShouldNotBeFound("tel.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserInfosByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where email equals to DEFAULT_EMAIL
        defaultUserInfoShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the userInfoList where email equals to UPDATED_EMAIL
        defaultUserInfoShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUserInfosByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultUserInfoShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the userInfoList where email equals to UPDATED_EMAIL
        defaultUserInfoShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUserInfosByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where email is not null
        defaultUserInfoShouldBeFound("email.specified=true");

        // Get all the userInfoList where email is null
        defaultUserInfoShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserInfosByPasswordQIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where passwordQ equals to DEFAULT_PASSWORD_Q
        defaultUserInfoShouldBeFound("passwordQ.equals=" + DEFAULT_PASSWORD_Q);

        // Get all the userInfoList where passwordQ equals to UPDATED_PASSWORD_Q
        defaultUserInfoShouldNotBeFound("passwordQ.equals=" + UPDATED_PASSWORD_Q);
    }

    @Test
    @Transactional
    public void getAllUserInfosByPasswordQIsInShouldWork() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where passwordQ in DEFAULT_PASSWORD_Q or UPDATED_PASSWORD_Q
        defaultUserInfoShouldBeFound("passwordQ.in=" + DEFAULT_PASSWORD_Q + "," + UPDATED_PASSWORD_Q);

        // Get all the userInfoList where passwordQ equals to UPDATED_PASSWORD_Q
        defaultUserInfoShouldNotBeFound("passwordQ.in=" + UPDATED_PASSWORD_Q);
    }

    @Test
    @Transactional
    public void getAllUserInfosByPasswordQIsNullOrNotNull() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where passwordQ is not null
        defaultUserInfoShouldBeFound("passwordQ.specified=true");

        // Get all the userInfoList where passwordQ is null
        defaultUserInfoShouldNotBeFound("passwordQ.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserInfosByPasswordAIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where passwordA equals to DEFAULT_PASSWORD_A
        defaultUserInfoShouldBeFound("passwordA.equals=" + DEFAULT_PASSWORD_A);

        // Get all the userInfoList where passwordA equals to UPDATED_PASSWORD_A
        defaultUserInfoShouldNotBeFound("passwordA.equals=" + UPDATED_PASSWORD_A);
    }

    @Test
    @Transactional
    public void getAllUserInfosByPasswordAIsInShouldWork() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where passwordA in DEFAULT_PASSWORD_A or UPDATED_PASSWORD_A
        defaultUserInfoShouldBeFound("passwordA.in=" + DEFAULT_PASSWORD_A + "," + UPDATED_PASSWORD_A);

        // Get all the userInfoList where passwordA equals to UPDATED_PASSWORD_A
        defaultUserInfoShouldNotBeFound("passwordA.in=" + UPDATED_PASSWORD_A);
    }

    @Test
    @Transactional
    public void getAllUserInfosByPasswordAIsNullOrNotNull() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        // Get all the userInfoList where passwordA is not null
        defaultUserInfoShouldBeFound("passwordA.specified=true");

        // Get all the userInfoList where passwordA is null
        defaultUserInfoShouldNotBeFound("passwordA.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserInfosByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        userInfo.setUser(user);
        userInfoRepository.saveAndFlush(userInfo);
        Long userId = user.getId();

        // Get all the userInfoList where user equals to userId
        defaultUserInfoShouldBeFound("userId.equals=" + userId);

        // Get all the userInfoList where user equals to userId + 1
        defaultUserInfoShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllUserInfosByCommandsIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);
        Command commands = CommandResourceIT.createEntity(em);
        em.persist(commands);
        em.flush();
        userInfo.addCommands(commands);
        userInfoRepository.saveAndFlush(userInfo);
        Long commandsId = commands.getId();

        // Get all the userInfoList where commands equals to commandsId
        defaultUserInfoShouldBeFound("commandsId.equals=" + commandsId);

        // Get all the userInfoList where commands equals to commandsId + 1
        defaultUserInfoShouldNotBeFound("commandsId.equals=" + (commandsId + 1));
    }


    @Test
    @Transactional
    public void getAllUserInfosByShopcartIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);
        ShopCart shopcart = ShopCartResourceIT.createEntity(em);
        em.persist(shopcart);
        em.flush();
        userInfo.addShopcart(shopcart);
        userInfoRepository.saveAndFlush(userInfo);
        Long shopcartId = shopcart.getId();

        // Get all the userInfoList where shopcart equals to shopcartId
        defaultUserInfoShouldBeFound("shopcartId.equals=" + shopcartId);

        // Get all the userInfoList where shopcart equals to shopcartId + 1
        defaultUserInfoShouldNotBeFound("shopcartId.equals=" + (shopcartId + 1));
    }


    @Test
    @Transactional
    public void getAllUserInfosByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);
        Address address = AddressResourceIT.createEntity(em);
        em.persist(address);
        em.flush();
        userInfo.addAddress(address);
        userInfoRepository.saveAndFlush(userInfo);
        Long addressId = address.getId();

        // Get all the userInfoList where address equals to addressId
        defaultUserInfoShouldBeFound("addressId.equals=" + addressId);

        // Get all the userInfoList where address equals to addressId + 1
        defaultUserInfoShouldNotBeFound("addressId.equals=" + (addressId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserInfoShouldBeFound(String filter) throws Exception {
        restUserInfoMockMvc.perform(get("/api/user-infos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].userStatus").value(hasItem(DEFAULT_USER_STATUS)))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].passwordQ").value(hasItem(DEFAULT_PASSWORD_Q)))
            .andExpect(jsonPath("$.[*].passwordA").value(hasItem(DEFAULT_PASSWORD_A)));

        // Check, that the count call also returns 1
        restUserInfoMockMvc.perform(get("/api/user-infos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserInfoShouldNotBeFound(String filter) throws Exception {
        restUserInfoMockMvc.perform(get("/api/user-infos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserInfoMockMvc.perform(get("/api/user-infos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserInfo() throws Exception {
        // Get the userInfo
        restUserInfoMockMvc.perform(get("/api/user-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserInfo() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        int databaseSizeBeforeUpdate = userInfoRepository.findAll().size();

        // Update the userInfo
        UserInfo updatedUserInfo = userInfoRepository.findById(userInfo.getId()).get();
        // Disconnect from session so that the updates on updatedUserInfo are not directly saved in db
        em.detach(updatedUserInfo);
        updatedUserInfo
            .userStatus(UPDATED_USER_STATUS)
            .tel(UPDATED_TEL)
            .email(UPDATED_EMAIL)
            .passwordQ(UPDATED_PASSWORD_Q)
            .passwordA(UPDATED_PASSWORD_A);
        UserInfoDTO userInfoDTO = userInfoMapper.toDto(updatedUserInfo);

        restUserInfoMockMvc.perform(put("/api/user-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userInfoDTO)))
            .andExpect(status().isOk());

        // Validate the UserInfo in the database
        List<UserInfo> userInfoList = userInfoRepository.findAll();
        assertThat(userInfoList).hasSize(databaseSizeBeforeUpdate);
        UserInfo testUserInfo = userInfoList.get(userInfoList.size() - 1);
        assertThat(testUserInfo.getUserStatus()).isEqualTo(UPDATED_USER_STATUS);
        assertThat(testUserInfo.getTel()).isEqualTo(UPDATED_TEL);
        assertThat(testUserInfo.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserInfo.getPasswordQ()).isEqualTo(UPDATED_PASSWORD_Q);
        assertThat(testUserInfo.getPasswordA()).isEqualTo(UPDATED_PASSWORD_A);

        // Validate the UserInfo in Elasticsearch
        verify(mockUserInfoSearchRepository, times(1)).save(testUserInfo);
    }

    @Test
    @Transactional
    public void updateNonExistingUserInfo() throws Exception {
        int databaseSizeBeforeUpdate = userInfoRepository.findAll().size();

        // Create the UserInfo
        UserInfoDTO userInfoDTO = userInfoMapper.toDto(userInfo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserInfoMockMvc.perform(put("/api/user-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserInfo in the database
        List<UserInfo> userInfoList = userInfoRepository.findAll();
        assertThat(userInfoList).hasSize(databaseSizeBeforeUpdate);

        // Validate the UserInfo in Elasticsearch
        verify(mockUserInfoSearchRepository, times(0)).save(userInfo);
    }

    @Test
    @Transactional
    public void deleteUserInfo() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);

        int databaseSizeBeforeDelete = userInfoRepository.findAll().size();

        // Delete the userInfo
        restUserInfoMockMvc.perform(delete("/api/user-infos/{id}", userInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserInfo> userInfoList = userInfoRepository.findAll();
        assertThat(userInfoList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the UserInfo in Elasticsearch
        verify(mockUserInfoSearchRepository, times(1)).deleteById(userInfo.getId());
    }

    @Test
    @Transactional
    public void searchUserInfo() throws Exception {
        // Initialize the database
        userInfoRepository.saveAndFlush(userInfo);
        when(mockUserInfoSearchRepository.search(queryStringQuery("id:" + userInfo.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(userInfo), PageRequest.of(0, 1), 1));
        // Search the userInfo
        restUserInfoMockMvc.perform(get("/api/_search/user-infos?query=id:" + userInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].userStatus").value(hasItem(DEFAULT_USER_STATUS)))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].passwordQ").value(hasItem(DEFAULT_PASSWORD_Q)))
            .andExpect(jsonPath("$.[*].passwordA").value(hasItem(DEFAULT_PASSWORD_A)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserInfo.class);
        UserInfo userInfo1 = new UserInfo();
        userInfo1.setId(1L);
        UserInfo userInfo2 = new UserInfo();
        userInfo2.setId(userInfo1.getId());
        assertThat(userInfo1).isEqualTo(userInfo2);
        userInfo2.setId(2L);
        assertThat(userInfo1).isNotEqualTo(userInfo2);
        userInfo1.setId(null);
        assertThat(userInfo1).isNotEqualTo(userInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserInfoDTO.class);
        UserInfoDTO userInfoDTO1 = new UserInfoDTO();
        userInfoDTO1.setId(1L);
        UserInfoDTO userInfoDTO2 = new UserInfoDTO();
        assertThat(userInfoDTO1).isNotEqualTo(userInfoDTO2);
        userInfoDTO2.setId(userInfoDTO1.getId());
        assertThat(userInfoDTO1).isEqualTo(userInfoDTO2);
        userInfoDTO2.setId(2L);
        assertThat(userInfoDTO1).isNotEqualTo(userInfoDTO2);
        userInfoDTO1.setId(null);
        assertThat(userInfoDTO1).isNotEqualTo(userInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userInfoMapper.fromId(null)).isNull();
    }
}

package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.Harddrive;
import com.mycompany.myapp.repository.HarddriveRepository;
import com.mycompany.myapp.repository.search.HarddriveSearchRepository;
import com.mycompany.myapp.service.HarddriveService;
import com.mycompany.myapp.service.dto.HarddriveDTO;
import com.mycompany.myapp.service.mapper.HarddriveMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HarddriveResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class HarddriveResourceIT {

    private static final Integer DEFAULT_SIZE = 1;
    private static final Integer UPDATED_SIZE = 2;
    private static final Integer SMALLER_SIZE = 1 - 1;

    @Autowired
    private HarddriveRepository harddriveRepository;

    @Mock
    private HarddriveRepository harddriveRepositoryMock;

    @Autowired
    private HarddriveMapper harddriveMapper;

    @Mock
    private HarddriveService harddriveServiceMock;

    @Autowired
    private HarddriveService harddriveService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.HarddriveSearchRepositoryMockConfiguration
     */
    @Autowired
    private HarddriveSearchRepository mockHarddriveSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restHarddriveMockMvc;

    private Harddrive harddrive;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HarddriveResource harddriveResource = new HarddriveResource(harddriveService);
        this.restHarddriveMockMvc = MockMvcBuilders.standaloneSetup(harddriveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Harddrive createEntity(EntityManager em) {
        Harddrive harddrive = new Harddrive()
            .size(DEFAULT_SIZE);
        return harddrive;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Harddrive createUpdatedEntity(EntityManager em) {
        Harddrive harddrive = new Harddrive()
            .size(UPDATED_SIZE);
        return harddrive;
    }

    @BeforeEach
    public void initTest() {
        harddrive = createEntity(em);
    }

    @Test
    @Transactional
    public void createHarddrive() throws Exception {
        int databaseSizeBeforeCreate = harddriveRepository.findAll().size();

        // Create the Harddrive
        HarddriveDTO harddriveDTO = harddriveMapper.toDto(harddrive);
        restHarddriveMockMvc.perform(post("/api/harddrives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(harddriveDTO)))
            .andExpect(status().isCreated());

        // Validate the Harddrive in the database
        List<Harddrive> harddriveList = harddriveRepository.findAll();
        assertThat(harddriveList).hasSize(databaseSizeBeforeCreate + 1);
        Harddrive testHarddrive = harddriveList.get(harddriveList.size() - 1);
        assertThat(testHarddrive.getSize()).isEqualTo(DEFAULT_SIZE);

        // Validate the Harddrive in Elasticsearch
        verify(mockHarddriveSearchRepository, times(1)).save(testHarddrive);
    }

    @Test
    @Transactional
    public void createHarddriveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = harddriveRepository.findAll().size();

        // Create the Harddrive with an existing ID
        harddrive.setId(1L);
        HarddriveDTO harddriveDTO = harddriveMapper.toDto(harddrive);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHarddriveMockMvc.perform(post("/api/harddrives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(harddriveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Harddrive in the database
        List<Harddrive> harddriveList = harddriveRepository.findAll();
        assertThat(harddriveList).hasSize(databaseSizeBeforeCreate);

        // Validate the Harddrive in Elasticsearch
        verify(mockHarddriveSearchRepository, times(0)).save(harddrive);
    }


    @Test
    @Transactional
    public void getAllHarddrives() throws Exception {
        // Initialize the database
        harddriveRepository.saveAndFlush(harddrive);

        // Get all the harddriveList
        restHarddriveMockMvc.perform(get("/api/harddrives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(harddrive.getId().intValue())))
            .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllHarddrivesWithEagerRelationshipsIsEnabled() throws Exception {
        HarddriveResource harddriveResource = new HarddriveResource(harddriveServiceMock);
        when(harddriveServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restHarddriveMockMvc = MockMvcBuilders.standaloneSetup(harddriveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restHarddriveMockMvc.perform(get("/api/harddrives?eagerload=true"))
        .andExpect(status().isOk());

        verify(harddriveServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllHarddrivesWithEagerRelationshipsIsNotEnabled() throws Exception {
        HarddriveResource harddriveResource = new HarddriveResource(harddriveServiceMock);
            when(harddriveServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restHarddriveMockMvc = MockMvcBuilders.standaloneSetup(harddriveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restHarddriveMockMvc.perform(get("/api/harddrives?eagerload=true"))
        .andExpect(status().isOk());

            verify(harddriveServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getHarddrive() throws Exception {
        // Initialize the database
        harddriveRepository.saveAndFlush(harddrive);

        // Get the harddrive
        restHarddriveMockMvc.perform(get("/api/harddrives/{id}", harddrive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(harddrive.getId().intValue()))
            .andExpect(jsonPath("$.size").value(DEFAULT_SIZE));
    }

    @Test
    @Transactional
    public void getNonExistingHarddrive() throws Exception {
        // Get the harddrive
        restHarddriveMockMvc.perform(get("/api/harddrives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHarddrive() throws Exception {
        // Initialize the database
        harddriveRepository.saveAndFlush(harddrive);

        int databaseSizeBeforeUpdate = harddriveRepository.findAll().size();

        // Update the harddrive
        Harddrive updatedHarddrive = harddriveRepository.findById(harddrive.getId()).get();
        // Disconnect from session so that the updates on updatedHarddrive are not directly saved in db
        em.detach(updatedHarddrive);
        updatedHarddrive
            .size(UPDATED_SIZE);
        HarddriveDTO harddriveDTO = harddriveMapper.toDto(updatedHarddrive);

        restHarddriveMockMvc.perform(put("/api/harddrives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(harddriveDTO)))
            .andExpect(status().isOk());

        // Validate the Harddrive in the database
        List<Harddrive> harddriveList = harddriveRepository.findAll();
        assertThat(harddriveList).hasSize(databaseSizeBeforeUpdate);
        Harddrive testHarddrive = harddriveList.get(harddriveList.size() - 1);
        assertThat(testHarddrive.getSize()).isEqualTo(UPDATED_SIZE);

        // Validate the Harddrive in Elasticsearch
        verify(mockHarddriveSearchRepository, times(1)).save(testHarddrive);
    }

    @Test
    @Transactional
    public void updateNonExistingHarddrive() throws Exception {
        int databaseSizeBeforeUpdate = harddriveRepository.findAll().size();

        // Create the Harddrive
        HarddriveDTO harddriveDTO = harddriveMapper.toDto(harddrive);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHarddriveMockMvc.perform(put("/api/harddrives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(harddriveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Harddrive in the database
        List<Harddrive> harddriveList = harddriveRepository.findAll();
        assertThat(harddriveList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Harddrive in Elasticsearch
        verify(mockHarddriveSearchRepository, times(0)).save(harddrive);
    }

    @Test
    @Transactional
    public void deleteHarddrive() throws Exception {
        // Initialize the database
        harddriveRepository.saveAndFlush(harddrive);

        int databaseSizeBeforeDelete = harddriveRepository.findAll().size();

        // Delete the harddrive
        restHarddriveMockMvc.perform(delete("/api/harddrives/{id}", harddrive.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Harddrive> harddriveList = harddriveRepository.findAll();
        assertThat(harddriveList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Harddrive in Elasticsearch
        verify(mockHarddriveSearchRepository, times(1)).deleteById(harddrive.getId());
    }

    @Test
    @Transactional
    public void searchHarddrive() throws Exception {
        // Initialize the database
        harddriveRepository.saveAndFlush(harddrive);
        when(mockHarddriveSearchRepository.search(queryStringQuery("id:" + harddrive.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(harddrive), PageRequest.of(0, 1), 1));
        // Search the harddrive
        restHarddriveMockMvc.perform(get("/api/_search/harddrives?query=id:" + harddrive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(harddrive.getId().intValue())))
            .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Harddrive.class);
        Harddrive harddrive1 = new Harddrive();
        harddrive1.setId(1L);
        Harddrive harddrive2 = new Harddrive();
        harddrive2.setId(harddrive1.getId());
        assertThat(harddrive1).isEqualTo(harddrive2);
        harddrive2.setId(2L);
        assertThat(harddrive1).isNotEqualTo(harddrive2);
        harddrive1.setId(null);
        assertThat(harddrive1).isNotEqualTo(harddrive2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HarddriveDTO.class);
        HarddriveDTO harddriveDTO1 = new HarddriveDTO();
        harddriveDTO1.setId(1L);
        HarddriveDTO harddriveDTO2 = new HarddriveDTO();
        assertThat(harddriveDTO1).isNotEqualTo(harddriveDTO2);
        harddriveDTO2.setId(harddriveDTO1.getId());
        assertThat(harddriveDTO1).isEqualTo(harddriveDTO2);
        harddriveDTO2.setId(2L);
        assertThat(harddriveDTO1).isNotEqualTo(harddriveDTO2);
        harddriveDTO1.setId(null);
        assertThat(harddriveDTO1).isNotEqualTo(harddriveDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(harddriveMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(harddriveMapper.fromId(null)).isNull();
    }
}

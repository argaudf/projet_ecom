package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ProjetEcomApp;
import com.mycompany.myapp.domain.Address;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.domain.Command;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.search.AddressSearchRepository;
import com.mycompany.myapp.service.AddressService;
import com.mycompany.myapp.service.dto.AddressDTO;
import com.mycompany.myapp.service.mapper.AddressMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;
import com.mycompany.myapp.service.dto.AddressCriteria;
import com.mycompany.myapp.service.AddressQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AddressResource} REST controller.
 */
@SpringBootTest(classes = ProjetEcomApp.class)
public class AddressResourceIT {

    private static final String DEFAULT_ADDRESS_COMPLETE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_COMPLETE = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVE_TEL = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVE_TEL = "BBBBBBBBBB";

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AddressService addressService;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.AddressSearchRepositoryMockConfiguration
     */
    @Autowired
    private AddressSearchRepository mockAddressSearchRepository;

    @Autowired
    private AddressQueryService addressQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAddressMockMvc;

    private Address address;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AddressResource addressResource = new AddressResource(addressService, addressQueryService);
        this.restAddressMockMvc = MockMvcBuilders.standaloneSetup(addressResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Address createEntity(EntityManager em) {
        Address address = new Address()
            .addressComplete(DEFAULT_ADDRESS_COMPLETE)
            .receiveName(DEFAULT_RECEIVE_NAME)
            .receiveTel(DEFAULT_RECEIVE_TEL);
        return address;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Address createUpdatedEntity(EntityManager em) {
        Address address = new Address()
            .addressComplete(UPDATED_ADDRESS_COMPLETE)
            .receiveName(UPDATED_RECEIVE_NAME)
            .receiveTel(UPDATED_RECEIVE_TEL);
        return address;
    }

    @BeforeEach
    public void initTest() {
        address = createEntity(em);
    }

    @Test
    @Transactional
    public void createAddress() throws Exception {
        int databaseSizeBeforeCreate = addressRepository.findAll().size();

        // Create the Address
        AddressDTO addressDTO = addressMapper.toDto(address);
        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isCreated());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeCreate + 1);
        Address testAddress = addressList.get(addressList.size() - 1);
        assertThat(testAddress.getAddressComplete()).isEqualTo(DEFAULT_ADDRESS_COMPLETE);
        assertThat(testAddress.getReceiveName()).isEqualTo(DEFAULT_RECEIVE_NAME);
        assertThat(testAddress.getReceiveTel()).isEqualTo(DEFAULT_RECEIVE_TEL);

        // Validate the Address in Elasticsearch
        verify(mockAddressSearchRepository, times(1)).save(testAddress);
    }

    @Test
    @Transactional
    public void createAddressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = addressRepository.findAll().size();

        // Create the Address with an existing ID
        address.setId(1L);
        AddressDTO addressDTO = addressMapper.toDto(address);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeCreate);

        // Validate the Address in Elasticsearch
        verify(mockAddressSearchRepository, times(0)).save(address);
    }


    @Test
    @Transactional
    public void getAllAddresses() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList
        restAddressMockMvc.perform(get("/api/addresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.getId().intValue())))
            .andExpect(jsonPath("$.[*].addressComplete").value(hasItem(DEFAULT_ADDRESS_COMPLETE.toString())))
            .andExpect(jsonPath("$.[*].receiveName").value(hasItem(DEFAULT_RECEIVE_NAME.toString())))
            .andExpect(jsonPath("$.[*].receiveTel").value(hasItem(DEFAULT_RECEIVE_TEL.toString())));
    }
    
    @Test
    @Transactional
    public void getAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get the address
        restAddressMockMvc.perform(get("/api/addresses/{id}", address.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(address.getId().intValue()))
            .andExpect(jsonPath("$.addressComplete").value(DEFAULT_ADDRESS_COMPLETE.toString()))
            .andExpect(jsonPath("$.receiveName").value(DEFAULT_RECEIVE_NAME.toString()))
            .andExpect(jsonPath("$.receiveTel").value(DEFAULT_RECEIVE_TEL.toString()));
    }

    @Test
    @Transactional
    public void getAllAddressesByAddressCompleteIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where addressComplete equals to DEFAULT_ADDRESS_COMPLETE
        defaultAddressShouldBeFound("addressComplete.equals=" + DEFAULT_ADDRESS_COMPLETE);

        // Get all the addressList where addressComplete equals to UPDATED_ADDRESS_COMPLETE
        defaultAddressShouldNotBeFound("addressComplete.equals=" + UPDATED_ADDRESS_COMPLETE);
    }

    @Test
    @Transactional
    public void getAllAddressesByAddressCompleteIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where addressComplete in DEFAULT_ADDRESS_COMPLETE or UPDATED_ADDRESS_COMPLETE
        defaultAddressShouldBeFound("addressComplete.in=" + DEFAULT_ADDRESS_COMPLETE + "," + UPDATED_ADDRESS_COMPLETE);

        // Get all the addressList where addressComplete equals to UPDATED_ADDRESS_COMPLETE
        defaultAddressShouldNotBeFound("addressComplete.in=" + UPDATED_ADDRESS_COMPLETE);
    }

    @Test
    @Transactional
    public void getAllAddressesByAddressCompleteIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where addressComplete is not null
        defaultAddressShouldBeFound("addressComplete.specified=true");

        // Get all the addressList where addressComplete is null
        defaultAddressShouldNotBeFound("addressComplete.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByReceiveNameIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where receiveName equals to DEFAULT_RECEIVE_NAME
        defaultAddressShouldBeFound("receiveName.equals=" + DEFAULT_RECEIVE_NAME);

        // Get all the addressList where receiveName equals to UPDATED_RECEIVE_NAME
        defaultAddressShouldNotBeFound("receiveName.equals=" + UPDATED_RECEIVE_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByReceiveNameIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where receiveName in DEFAULT_RECEIVE_NAME or UPDATED_RECEIVE_NAME
        defaultAddressShouldBeFound("receiveName.in=" + DEFAULT_RECEIVE_NAME + "," + UPDATED_RECEIVE_NAME);

        // Get all the addressList where receiveName equals to UPDATED_RECEIVE_NAME
        defaultAddressShouldNotBeFound("receiveName.in=" + UPDATED_RECEIVE_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByReceiveNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where receiveName is not null
        defaultAddressShouldBeFound("receiveName.specified=true");

        // Get all the addressList where receiveName is null
        defaultAddressShouldNotBeFound("receiveName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByReceiveTelIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where receiveTel equals to DEFAULT_RECEIVE_TEL
        defaultAddressShouldBeFound("receiveTel.equals=" + DEFAULT_RECEIVE_TEL);

        // Get all the addressList where receiveTel equals to UPDATED_RECEIVE_TEL
        defaultAddressShouldNotBeFound("receiveTel.equals=" + UPDATED_RECEIVE_TEL);
    }

    @Test
    @Transactional
    public void getAllAddressesByReceiveTelIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where receiveTel in DEFAULT_RECEIVE_TEL or UPDATED_RECEIVE_TEL
        defaultAddressShouldBeFound("receiveTel.in=" + DEFAULT_RECEIVE_TEL + "," + UPDATED_RECEIVE_TEL);

        // Get all the addressList where receiveTel equals to UPDATED_RECEIVE_TEL
        defaultAddressShouldNotBeFound("receiveTel.in=" + UPDATED_RECEIVE_TEL);
    }

    @Test
    @Transactional
    public void getAllAddressesByReceiveTelIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where receiveTel is not null
        defaultAddressShouldBeFound("receiveTel.specified=true");

        // Get all the addressList where receiveTel is null
        defaultAddressShouldNotBeFound("receiveTel.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);
        UserInfo user = UserInfoResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        address.setUser(user);
        addressRepository.saveAndFlush(address);
        Long userId = user.getId();

        // Get all the addressList where user equals to userId
        defaultAddressShouldBeFound("userId.equals=" + userId);

        // Get all the addressList where user equals to userId + 1
        defaultAddressShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllAddressesByCommandIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);
        Command command = CommandResourceIT.createEntity(em);
        em.persist(command);
        em.flush();
        address.addCommand(command);
        addressRepository.saveAndFlush(address);
        Long commandId = command.getId();

        // Get all the addressList where command equals to commandId
        defaultAddressShouldBeFound("commandId.equals=" + commandId);

        // Get all the addressList where command equals to commandId + 1
        defaultAddressShouldNotBeFound("commandId.equals=" + (commandId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAddressShouldBeFound(String filter) throws Exception {
        restAddressMockMvc.perform(get("/api/addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.getId().intValue())))
            .andExpect(jsonPath("$.[*].addressComplete").value(hasItem(DEFAULT_ADDRESS_COMPLETE)))
            .andExpect(jsonPath("$.[*].receiveName").value(hasItem(DEFAULT_RECEIVE_NAME)))
            .andExpect(jsonPath("$.[*].receiveTel").value(hasItem(DEFAULT_RECEIVE_TEL)));

        // Check, that the count call also returns 1
        restAddressMockMvc.perform(get("/api/addresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAddressShouldNotBeFound(String filter) throws Exception {
        restAddressMockMvc.perform(get("/api/addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAddressMockMvc.perform(get("/api/addresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAddress() throws Exception {
        // Get the address
        restAddressMockMvc.perform(get("/api/addresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        int databaseSizeBeforeUpdate = addressRepository.findAll().size();

        // Update the address
        Address updatedAddress = addressRepository.findById(address.getId()).get();
        // Disconnect from session so that the updates on updatedAddress are not directly saved in db
        em.detach(updatedAddress);
        updatedAddress
            .addressComplete(UPDATED_ADDRESS_COMPLETE)
            .receiveName(UPDATED_RECEIVE_NAME)
            .receiveTel(UPDATED_RECEIVE_TEL);
        AddressDTO addressDTO = addressMapper.toDto(updatedAddress);

        restAddressMockMvc.perform(put("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isOk());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate);
        Address testAddress = addressList.get(addressList.size() - 1);
        assertThat(testAddress.getAddressComplete()).isEqualTo(UPDATED_ADDRESS_COMPLETE);
        assertThat(testAddress.getReceiveName()).isEqualTo(UPDATED_RECEIVE_NAME);
        assertThat(testAddress.getReceiveTel()).isEqualTo(UPDATED_RECEIVE_TEL);

        // Validate the Address in Elasticsearch
        verify(mockAddressSearchRepository, times(1)).save(testAddress);
    }

    @Test
    @Transactional
    public void updateNonExistingAddress() throws Exception {
        int databaseSizeBeforeUpdate = addressRepository.findAll().size();

        // Create the Address
        AddressDTO addressDTO = addressMapper.toDto(address);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAddressMockMvc.perform(put("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Address in Elasticsearch
        verify(mockAddressSearchRepository, times(0)).save(address);
    }

    @Test
    @Transactional
    public void deleteAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        int databaseSizeBeforeDelete = addressRepository.findAll().size();

        // Delete the address
        restAddressMockMvc.perform(delete("/api/addresses/{id}", address.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Address in Elasticsearch
        verify(mockAddressSearchRepository, times(1)).deleteById(address.getId());
    }

    @Test
    @Transactional
    public void searchAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);
        when(mockAddressSearchRepository.search(queryStringQuery("id:" + address.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(address), PageRequest.of(0, 1), 1));
        // Search the address
        restAddressMockMvc.perform(get("/api/_search/addresses?query=id:" + address.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.getId().intValue())))
            .andExpect(jsonPath("$.[*].addressComplete").value(hasItem(DEFAULT_ADDRESS_COMPLETE)))
            .andExpect(jsonPath("$.[*].receiveName").value(hasItem(DEFAULT_RECEIVE_NAME)))
            .andExpect(jsonPath("$.[*].receiveTel").value(hasItem(DEFAULT_RECEIVE_TEL)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Address.class);
        Address address1 = new Address();
        address1.setId(1L);
        Address address2 = new Address();
        address2.setId(address1.getId());
        assertThat(address1).isEqualTo(address2);
        address2.setId(2L);
        assertThat(address1).isNotEqualTo(address2);
        address1.setId(null);
        assertThat(address1).isNotEqualTo(address2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AddressDTO.class);
        AddressDTO addressDTO1 = new AddressDTO();
        addressDTO1.setId(1L);
        AddressDTO addressDTO2 = new AddressDTO();
        assertThat(addressDTO1).isNotEqualTo(addressDTO2);
        addressDTO2.setId(addressDTO1.getId());
        assertThat(addressDTO1).isEqualTo(addressDTO2);
        addressDTO2.setId(2L);
        assertThat(addressDTO1).isNotEqualTo(addressDTO2);
        addressDTO1.setId(null);
        assertThat(addressDTO1).isNotEqualTo(addressDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(addressMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(addressMapper.fromId(null)).isNull();
    }
}

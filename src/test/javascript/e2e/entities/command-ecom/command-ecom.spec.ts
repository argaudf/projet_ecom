// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { CommandComponentsPage, CommandDeleteDialog, CommandUpdatePage } from './command-ecom.page-object';

const expect = chai.expect;

describe('Command e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let commandUpdatePage: CommandUpdatePage;
  let commandComponentsPage: CommandComponentsPage;
  let commandDeleteDialog: CommandDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Commands', async () => {
    await navBarPage.goToEntity('command-ecom');
    commandComponentsPage = new CommandComponentsPage();
    await browser.wait(ec.visibilityOf(commandComponentsPage.title), 5000);
    expect(await commandComponentsPage.getTitle()).to.eq('projetEcomApp.command.home.title');
  });

  it('should load create Command page', async () => {
    await commandComponentsPage.clickOnCreateButton();
    commandUpdatePage = new CommandUpdatePage();
    expect(await commandUpdatePage.getPageTitle()).to.eq('projetEcomApp.command.home.createOrEditLabel');
    await commandUpdatePage.cancel();
  });

  it('should create and save Commands', async () => {
    const nbButtonsBeforeCreate = await commandComponentsPage.countDeleteButtons();

    await commandComponentsPage.clickOnCreateButton();
    await promise.all([
      commandUpdatePage.setStatusInput('5'),
      commandUpdatePage.userSelectLastOption(),
      commandUpdatePage.addressSelectLastOption()
    ]);
    expect(await commandUpdatePage.getStatusInput()).to.eq('5', 'Expected status value to be equals to 5');
    await commandUpdatePage.save();
    expect(await commandUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await commandComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Command', async () => {
    const nbButtonsBeforeDelete = await commandComponentsPage.countDeleteButtons();
    await commandComponentsPage.clickOnLastDeleteButton();

    commandDeleteDialog = new CommandDeleteDialog();
    expect(await commandDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.command.delete.question');
    await commandDeleteDialog.clickOnConfirmButton();

    expect(await commandComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

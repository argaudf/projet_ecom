// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { CpuComponentsPage, CpuDeleteDialog, CpuUpdatePage } from './cpu-ecom.page-object';

const expect = chai.expect;

describe('Cpu e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let cpuUpdatePage: CpuUpdatePage;
  let cpuComponentsPage: CpuComponentsPage;
  let cpuDeleteDialog: CpuDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Cpus', async () => {
    await navBarPage.goToEntity('cpu-ecom');
    cpuComponentsPage = new CpuComponentsPage();
    await browser.wait(ec.visibilityOf(cpuComponentsPage.title), 5000);
    expect(await cpuComponentsPage.getTitle()).to.eq('projetEcomApp.cpu.home.title');
  });

  it('should load create Cpu page', async () => {
    await cpuComponentsPage.clickOnCreateButton();
    cpuUpdatePage = new CpuUpdatePage();
    expect(await cpuUpdatePage.getPageTitle()).to.eq('projetEcomApp.cpu.home.createOrEditLabel');
    await cpuUpdatePage.cancel();
  });

  it('should create and save Cpus', async () => {
    const nbButtonsBeforeCreate = await cpuComponentsPage.countDeleteButtons();

    await cpuComponentsPage.clickOnCreateButton();
    await promise.all([cpuUpdatePage.setNameInput('name'), cpuUpdatePage.setFrequencyInput('5')]);
    expect(await cpuUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await cpuUpdatePage.getFrequencyInput()).to.eq('5', 'Expected frequency value to be equals to 5');
    await cpuUpdatePage.save();
    expect(await cpuUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await cpuComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Cpu', async () => {
    const nbButtonsBeforeDelete = await cpuComponentsPage.countDeleteButtons();
    await cpuComponentsPage.clickOnLastDeleteButton();

    cpuDeleteDialog = new CpuDeleteDialog();
    expect(await cpuDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.cpu.delete.question');
    await cpuDeleteDialog.clickOnConfirmButton();

    expect(await cpuComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ShopCartComponentsPage, ShopCartDeleteDialog, ShopCartUpdatePage } from './shop-cart-ecom.page-object';

const expect = chai.expect;

describe('ShopCart e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shopCartUpdatePage: ShopCartUpdatePage;
  let shopCartComponentsPage: ShopCartComponentsPage;
  let shopCartDeleteDialog: ShopCartDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ShopCarts', async () => {
    await navBarPage.goToEntity('shop-cart-ecom');
    shopCartComponentsPage = new ShopCartComponentsPage();
    await browser.wait(ec.visibilityOf(shopCartComponentsPage.title), 5000);
    expect(await shopCartComponentsPage.getTitle()).to.eq('projetEcomApp.shopCart.home.title');
  });

  it('should load create ShopCart page', async () => {
    await shopCartComponentsPage.clickOnCreateButton();
    shopCartUpdatePage = new ShopCartUpdatePage();
    expect(await shopCartUpdatePage.getPageTitle()).to.eq('projetEcomApp.shopCart.home.createOrEditLabel');
    await shopCartUpdatePage.cancel();
  });

  it('should create and save ShopCarts', async () => {
    const nbButtonsBeforeCreate = await shopCartComponentsPage.countDeleteButtons();

    await shopCartComponentsPage.clickOnCreateButton();
    await promise.all([
      shopCartUpdatePage.setQuantityInput('5'),
      shopCartUpdatePage.setAddTimeInput('2000-12-31'),
      shopCartUpdatePage.userSelectLastOption(),
      shopCartUpdatePage.productSelectLastOption()
    ]);
    expect(await shopCartUpdatePage.getQuantityInput()).to.eq('5', 'Expected quantity value to be equals to 5');
    expect(await shopCartUpdatePage.getAddTimeInput()).to.eq('2000-12-31', 'Expected addTime value to be equals to 2000-12-31');
    await shopCartUpdatePage.save();
    expect(await shopCartUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await shopCartComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last ShopCart', async () => {
    const nbButtonsBeforeDelete = await shopCartComponentsPage.countDeleteButtons();
    await shopCartComponentsPage.clickOnLastDeleteButton();

    shopCartDeleteDialog = new ShopCartDeleteDialog();
    expect(await shopCartDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.shopCart.delete.question');
    await shopCartDeleteDialog.clickOnConfirmButton();

    expect(await shopCartComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

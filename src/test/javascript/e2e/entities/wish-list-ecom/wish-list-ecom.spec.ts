// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { WishListComponentsPage, WishListDeleteDialog, WishListUpdatePage } from './wish-list-ecom.page-object';

const expect = chai.expect;

describe('WishList e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let wishListUpdatePage: WishListUpdatePage;
  let wishListComponentsPage: WishListComponentsPage;
  let wishListDeleteDialog: WishListDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load WishLists', async () => {
    await navBarPage.goToEntity('wish-list-ecom');
    wishListComponentsPage = new WishListComponentsPage();
    await browser.wait(ec.visibilityOf(wishListComponentsPage.title), 5000);
    expect(await wishListComponentsPage.getTitle()).to.eq('projetEcomApp.wishList.home.title');
  });

  it('should load create WishList page', async () => {
    await wishListComponentsPage.clickOnCreateButton();
    wishListUpdatePage = new WishListUpdatePage();
    expect(await wishListUpdatePage.getPageTitle()).to.eq('projetEcomApp.wishList.home.createOrEditLabel');
    await wishListUpdatePage.cancel();
  });

  it('should create and save WishLists', async () => {
    const nbButtonsBeforeCreate = await wishListComponentsPage.countDeleteButtons();

    await wishListComponentsPage.clickOnCreateButton();
    await promise.all([
      wishListUpdatePage.userSelectLastOption()
      // wishListUpdatePage.productSelectLastOption(),
    ]);
    await wishListUpdatePage.save();
    expect(await wishListUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await wishListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last WishList', async () => {
    const nbButtonsBeforeDelete = await wishListComponentsPage.countDeleteButtons();
    await wishListComponentsPage.clickOnLastDeleteButton();

    wishListDeleteDialog = new WishListDeleteDialog();
    expect(await wishListDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.wishList.delete.question');
    await wishListDeleteDialog.clickOnConfirmButton();

    expect(await wishListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

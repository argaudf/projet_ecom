// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { CommandProductComponentsPage, CommandProductDeleteDialog, CommandProductUpdatePage } from './command-product-ecom.page-object';

const expect = chai.expect;

describe('CommandProduct e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let commandProductUpdatePage: CommandProductUpdatePage;
  let commandProductComponentsPage: CommandProductComponentsPage;
  let commandProductDeleteDialog: CommandProductDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CommandProducts', async () => {
    await navBarPage.goToEntity('command-product-ecom');
    commandProductComponentsPage = new CommandProductComponentsPage();
    await browser.wait(ec.visibilityOf(commandProductComponentsPage.title), 5000);
    expect(await commandProductComponentsPage.getTitle()).to.eq('projetEcomApp.commandProduct.home.title');
  });

  it('should load create CommandProduct page', async () => {
    await commandProductComponentsPage.clickOnCreateButton();
    commandProductUpdatePage = new CommandProductUpdatePage();
    expect(await commandProductUpdatePage.getPageTitle()).to.eq('projetEcomApp.commandProduct.home.createOrEditLabel');
    await commandProductUpdatePage.cancel();
  });

  it('should create and save CommandProducts', async () => {
    const nbButtonsBeforeCreate = await commandProductComponentsPage.countDeleteButtons();

    await commandProductComponentsPage.clickOnCreateButton();
    await promise.all([
      commandProductUpdatePage.setQuantityInput('5'),
      commandProductUpdatePage.setAddTimeInput('2000-12-31'),
      commandProductUpdatePage.productSelectLastOption(),
      commandProductUpdatePage.commandSelectLastOption()
    ]);
    expect(await commandProductUpdatePage.getQuantityInput()).to.eq('5', 'Expected quantity value to be equals to 5');
    expect(await commandProductUpdatePage.getAddTimeInput()).to.eq('2000-12-31', 'Expected addTime value to be equals to 2000-12-31');
    await commandProductUpdatePage.save();
    expect(await commandProductUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await commandProductComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last CommandProduct', async () => {
    const nbButtonsBeforeDelete = await commandProductComponentsPage.countDeleteButtons();
    await commandProductComponentsPage.clickOnLastDeleteButton();

    commandProductDeleteDialog = new CommandProductDeleteDialog();
    expect(await commandProductDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.commandProduct.delete.question');
    await commandProductDeleteDialog.clickOnConfirmButton();

    expect(await commandProductComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

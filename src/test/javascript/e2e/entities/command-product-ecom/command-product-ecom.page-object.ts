import { element, by, ElementFinder } from 'protractor';

export class CommandProductComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-command-product-ecom div table .btn-danger'));
  title = element.all(by.css('jhi-command-product-ecom div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CommandProductUpdatePage {
  pageTitle = element(by.id('jhi-command-product-ecom-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  quantityInput = element(by.id('field_quantity'));
  addTimeInput = element(by.id('field_addTime'));
  productSelect = element(by.id('field_product'));
  commandSelect = element(by.id('field_command'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setQuantityInput(quantity) {
    await this.quantityInput.sendKeys(quantity);
  }

  async getQuantityInput() {
    return await this.quantityInput.getAttribute('value');
  }

  async setAddTimeInput(addTime) {
    await this.addTimeInput.sendKeys(addTime);
  }

  async getAddTimeInput() {
    return await this.addTimeInput.getAttribute('value');
  }

  async productSelectLastOption(timeout?: number) {
    await this.productSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async productSelectOption(option) {
    await this.productSelect.sendKeys(option);
  }

  getProductSelect(): ElementFinder {
    return this.productSelect;
  }

  async getProductSelectedOption() {
    return await this.productSelect.element(by.css('option:checked')).getText();
  }

  async commandSelectLastOption(timeout?: number) {
    await this.commandSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async commandSelectOption(option) {
    await this.commandSelect.sendKeys(option);
  }

  getCommandSelect(): ElementFinder {
    return this.commandSelect;
  }

  async getCommandSelectedOption() {
    return await this.commandSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CommandProductDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-commandProduct-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-commandProduct'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}

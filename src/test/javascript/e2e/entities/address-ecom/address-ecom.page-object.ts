import { element, by, ElementFinder } from 'protractor';

export class AddressComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-address-ecom div table .btn-danger'));
  title = element.all(by.css('jhi-address-ecom div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AddressUpdatePage {
  pageTitle = element(by.id('jhi-address-ecom-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  addressCompleteInput = element(by.id('field_addressComplete'));
  receiveNameInput = element(by.id('field_receiveName'));
  receiveTelInput = element(by.id('field_receiveTel'));
  userSelect = element(by.id('field_user'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setAddressCompleteInput(addressComplete) {
    await this.addressCompleteInput.sendKeys(addressComplete);
  }

  async getAddressCompleteInput() {
    return await this.addressCompleteInput.getAttribute('value');
  }

  async setReceiveNameInput(receiveName) {
    await this.receiveNameInput.sendKeys(receiveName);
  }

  async getReceiveNameInput() {
    return await this.receiveNameInput.getAttribute('value');
  }

  async setReceiveTelInput(receiveTel) {
    await this.receiveTelInput.sendKeys(receiveTel);
  }

  async getReceiveTelInput() {
    return await this.receiveTelInput.getAttribute('value');
  }

  async userSelectLastOption(timeout?: number) {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option) {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption() {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AddressDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-address-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-address'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HarddriveComponentsPage, HarddriveDeleteDialog, HarddriveUpdatePage } from './harddrive-ecom.page-object';

const expect = chai.expect;

describe('Harddrive e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let harddriveUpdatePage: HarddriveUpdatePage;
  let harddriveComponentsPage: HarddriveComponentsPage;
  let harddriveDeleteDialog: HarddriveDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Harddrives', async () => {
    await navBarPage.goToEntity('harddrive-ecom');
    harddriveComponentsPage = new HarddriveComponentsPage();
    await browser.wait(ec.visibilityOf(harddriveComponentsPage.title), 5000);
    expect(await harddriveComponentsPage.getTitle()).to.eq('projetEcomApp.harddrive.home.title');
  });

  it('should load create Harddrive page', async () => {
    await harddriveComponentsPage.clickOnCreateButton();
    harddriveUpdatePage = new HarddriveUpdatePage();
    expect(await harddriveUpdatePage.getPageTitle()).to.eq('projetEcomApp.harddrive.home.createOrEditLabel');
    await harddriveUpdatePage.cancel();
  });

  it('should create and save Harddrives', async () => {
    const nbButtonsBeforeCreate = await harddriveComponentsPage.countDeleteButtons();

    await harddriveComponentsPage.clickOnCreateButton();
    await promise.all([
      harddriveUpdatePage.setSizeInput('5')
      // harddriveUpdatePage.productSelectLastOption(),
    ]);
    expect(await harddriveUpdatePage.getSizeInput()).to.eq('5', 'Expected size value to be equals to 5');
    await harddriveUpdatePage.save();
    expect(await harddriveUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await harddriveComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Harddrive', async () => {
    const nbButtonsBeforeDelete = await harddriveComponentsPage.countDeleteButtons();
    await harddriveComponentsPage.clickOnLastDeleteButton();

    harddriveDeleteDialog = new HarddriveDeleteDialog();
    expect(await harddriveDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.harddrive.delete.question');
    await harddriveDeleteDialog.clickOnConfirmButton();

    expect(await harddriveComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { UserInfoComponentsPage, UserInfoDeleteDialog, UserInfoUpdatePage } from './user-info-ecom.page-object';

const expect = chai.expect;

describe('UserInfo e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userInfoUpdatePage: UserInfoUpdatePage;
  let userInfoComponentsPage: UserInfoComponentsPage;
  let userInfoDeleteDialog: UserInfoDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserInfos', async () => {
    await navBarPage.goToEntity('user-info-ecom');
    userInfoComponentsPage = new UserInfoComponentsPage();
    await browser.wait(ec.visibilityOf(userInfoComponentsPage.title), 5000);
    expect(await userInfoComponentsPage.getTitle()).to.eq('projetEcomApp.userInfo.home.title');
  });

  it('should load create UserInfo page', async () => {
    await userInfoComponentsPage.clickOnCreateButton();
    userInfoUpdatePage = new UserInfoUpdatePage();
    expect(await userInfoUpdatePage.getPageTitle()).to.eq('projetEcomApp.userInfo.home.createOrEditLabel');
    await userInfoUpdatePage.cancel();
  });

  it('should create and save UserInfos', async () => {
    const nbButtonsBeforeCreate = await userInfoComponentsPage.countDeleteButtons();

    await userInfoComponentsPage.clickOnCreateButton();
    await promise.all([
      userInfoUpdatePage.setUserStatusInput('userStatus'),
      userInfoUpdatePage.setTelInput('tel'),
      userInfoUpdatePage.setEmailInput('email'),
      userInfoUpdatePage.setPasswordQInput('passwordQ'),
      userInfoUpdatePage.setPasswordAInput('passwordA'),
      userInfoUpdatePage.userSelectLastOption()
    ]);
    expect(await userInfoUpdatePage.getUserStatusInput()).to.eq('userStatus', 'Expected UserStatus value to be equals to userStatus');
    expect(await userInfoUpdatePage.getTelInput()).to.eq('tel', 'Expected Tel value to be equals to tel');
    expect(await userInfoUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
    expect(await userInfoUpdatePage.getPasswordQInput()).to.eq('passwordQ', 'Expected PasswordQ value to be equals to passwordQ');
    expect(await userInfoUpdatePage.getPasswordAInput()).to.eq('passwordA', 'Expected PasswordA value to be equals to passwordA');
    await userInfoUpdatePage.save();
    expect(await userInfoUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userInfoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last UserInfo', async () => {
    const nbButtonsBeforeDelete = await userInfoComponentsPage.countDeleteButtons();
    await userInfoComponentsPage.clickOnLastDeleteButton();

    userInfoDeleteDialog = new UserInfoDeleteDialog();
    expect(await userInfoDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.userInfo.delete.question');
    await userInfoDeleteDialog.clickOnConfirmButton();

    expect(await userInfoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

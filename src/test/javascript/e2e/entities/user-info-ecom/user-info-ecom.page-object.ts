import { element, by, ElementFinder } from 'protractor';

export class UserInfoComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-user-info-ecom div table .btn-danger'));
  title = element.all(by.css('jhi-user-info-ecom div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class UserInfoUpdatePage {
  pageTitle = element(by.id('jhi-user-info-ecom-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  userStatusInput = element(by.id('field_userStatus'));
  telInput = element(by.id('field_tel'));
  emailInput = element(by.id('field_email'));
  passwordQInput = element(by.id('field_passwordQ'));
  passwordAInput = element(by.id('field_passwordA'));
  userSelect = element(by.id('field_user'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setUserStatusInput(userStatus) {
    await this.userStatusInput.sendKeys(userStatus);
  }

  async getUserStatusInput() {
    return await this.userStatusInput.getAttribute('value');
  }

  async setTelInput(tel) {
    await this.telInput.sendKeys(tel);
  }

  async getTelInput() {
    return await this.telInput.getAttribute('value');
  }

  async setEmailInput(email) {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput() {
    return await this.emailInput.getAttribute('value');
  }

  async setPasswordQInput(passwordQ) {
    await this.passwordQInput.sendKeys(passwordQ);
  }

  async getPasswordQInput() {
    return await this.passwordQInput.getAttribute('value');
  }

  async setPasswordAInput(passwordA) {
    await this.passwordAInput.sendKeys(passwordA);
  }

  async getPasswordAInput() {
    return await this.passwordAInput.getAttribute('value');
  }

  async userSelectLastOption(timeout?: number) {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option) {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption() {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserInfoDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-userInfo-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-userInfo'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BrandComponentsPage, BrandDeleteDialog, BrandUpdatePage } from './brand-ecom.page-object';

const expect = chai.expect;

describe('Brand e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let brandUpdatePage: BrandUpdatePage;
  let brandComponentsPage: BrandComponentsPage;
  let brandDeleteDialog: BrandDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Brands', async () => {
    await navBarPage.goToEntity('brand-ecom');
    brandComponentsPage = new BrandComponentsPage();
    await browser.wait(ec.visibilityOf(brandComponentsPage.title), 5000);
    expect(await brandComponentsPage.getTitle()).to.eq('projetEcomApp.brand.home.title');
  });

  it('should load create Brand page', async () => {
    await brandComponentsPage.clickOnCreateButton();
    brandUpdatePage = new BrandUpdatePage();
    expect(await brandUpdatePage.getPageTitle()).to.eq('projetEcomApp.brand.home.createOrEditLabel');
    await brandUpdatePage.cancel();
  });

  it('should create and save Brands', async () => {
    const nbButtonsBeforeCreate = await brandComponentsPage.countDeleteButtons();

    await brandComponentsPage.clickOnCreateButton();
    await promise.all([brandUpdatePage.setBrandNameInput('brandName')]);
    expect(await brandUpdatePage.getBrandNameInput()).to.eq('brandName', 'Expected BrandName value to be equals to brandName');
    await brandUpdatePage.save();
    expect(await brandUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await brandComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Brand', async () => {
    const nbButtonsBeforeDelete = await brandComponentsPage.countDeleteButtons();
    await brandComponentsPage.clickOnLastDeleteButton();

    brandDeleteDialog = new BrandDeleteDialog();
    expect(await brandDeleteDialog.getDialogTitle()).to.eq('projetEcomApp.brand.delete.question');
    await brandDeleteDialog.clickOnConfirmButton();

    expect(await brandComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

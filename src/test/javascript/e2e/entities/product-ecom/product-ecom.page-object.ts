import { element, by, ElementFinder } from 'protractor';

export class ProductComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-product-ecom div table .btn-danger'));
  title = element.all(by.css('jhi-product-ecom div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ProductUpdatePage {
  pageTitle = element(by.id('jhi-product-ecom-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  proNameInput = element(by.id('field_proName'));
  proPriceInput = element(by.id('field_proPrice'));
  stockInput = element(by.id('field_stock'));
  descriptionInput = element(by.id('field_description'));
  pictureInput = element(by.id('field_picture'));
  brandSelect = element(by.id('field_brand'));
  cpuSelect = element(by.id('field_cpu'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setProNameInput(proName) {
    await this.proNameInput.sendKeys(proName);
  }

  async getProNameInput() {
    return await this.proNameInput.getAttribute('value');
  }

  async setProPriceInput(proPrice) {
    await this.proPriceInput.sendKeys(proPrice);
  }

  async getProPriceInput() {
    return await this.proPriceInput.getAttribute('value');
  }

  async setStockInput(stock) {
    await this.stockInput.sendKeys(stock);
  }

  async getStockInput() {
    return await this.stockInput.getAttribute('value');
  }

  async setDescriptionInput(description) {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput() {
    return await this.descriptionInput.getAttribute('value');
  }

  async setPictureInput(picture) {
    await this.pictureInput.sendKeys(picture);
  }

  async getPictureInput() {
    return await this.pictureInput.getAttribute('value');
  }

  async brandSelectLastOption(timeout?: number) {
    await this.brandSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async brandSelectOption(option) {
    await this.brandSelect.sendKeys(option);
  }

  getBrandSelect(): ElementFinder {
    return this.brandSelect;
  }

  async getBrandSelectedOption() {
    return await this.brandSelect.element(by.css('option:checked')).getText();
  }

  async cpuSelectLastOption(timeout?: number) {
    await this.cpuSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async cpuSelectOption(option) {
    await this.cpuSelect.sendKeys(option);
  }

  getCpuSelect(): ElementFinder {
    return this.cpuSelect;
  }

  async getCpuSelectedOption() {
    return await this.cpuSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ProductDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-product-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-product'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}

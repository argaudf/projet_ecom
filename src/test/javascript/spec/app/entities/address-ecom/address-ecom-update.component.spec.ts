import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { AddressEcomUpdateComponent } from 'app/entities/address-ecom/address-ecom-update.component';
import { AddressEcomService } from 'app/entities/address-ecom/address-ecom.service';
import { AddressEcom } from 'app/shared/model/address-ecom.model';

describe('Component Tests', () => {
  describe('AddressEcom Management Update Component', () => {
    let comp: AddressEcomUpdateComponent;
    let fixture: ComponentFixture<AddressEcomUpdateComponent>;
    let service: AddressEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [AddressEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AddressEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AddressEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AddressEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AddressEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AddressEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetEcomTestModule } from '../../../test.module';
import { AddressEcomDeleteDialogComponent } from 'app/entities/address-ecom/address-ecom-delete-dialog.component';
import { AddressEcomService } from 'app/entities/address-ecom/address-ecom.service';

describe('Component Tests', () => {
  describe('AddressEcom Management Delete Component', () => {
    let comp: AddressEcomDeleteDialogComponent;
    let fixture: ComponentFixture<AddressEcomDeleteDialogComponent>;
    let service: AddressEcomService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [AddressEcomDeleteDialogComponent]
      })
        .overrideTemplate(AddressEcomDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AddressEcomDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AddressEcomService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

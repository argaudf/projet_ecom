import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { AddressEcomDetailComponent } from 'app/entities/address-ecom/address-ecom-detail.component';
import { AddressEcom } from 'app/shared/model/address-ecom.model';

describe('Component Tests', () => {
  describe('AddressEcom Management Detail Component', () => {
    let comp: AddressEcomDetailComponent;
    let fixture: ComponentFixture<AddressEcomDetailComponent>;
    const route = ({ data: of({ address: new AddressEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [AddressEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AddressEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AddressEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.address).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { UserInfoEcomUpdateComponent } from 'app/entities/user-info-ecom/user-info-ecom-update.component';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';
import { UserInfoEcom } from 'app/shared/model/user-info-ecom.model';

describe('Component Tests', () => {
  describe('UserInfoEcom Management Update Component', () => {
    let comp: UserInfoEcomUpdateComponent;
    let fixture: ComponentFixture<UserInfoEcomUpdateComponent>;
    let service: UserInfoEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [UserInfoEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserInfoEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserInfoEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserInfoEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserInfoEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserInfoEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

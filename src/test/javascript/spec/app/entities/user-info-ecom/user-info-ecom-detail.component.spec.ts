import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { UserInfoEcomDetailComponent } from 'app/entities/user-info-ecom/user-info-ecom-detail.component';
import { UserInfoEcom } from 'app/shared/model/user-info-ecom.model';

describe('Component Tests', () => {
  describe('UserInfoEcom Management Detail Component', () => {
    let comp: UserInfoEcomDetailComponent;
    let fixture: ComponentFixture<UserInfoEcomDetailComponent>;
    const route = ({ data: of({ userInfo: new UserInfoEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [UserInfoEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserInfoEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserInfoEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userInfo).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

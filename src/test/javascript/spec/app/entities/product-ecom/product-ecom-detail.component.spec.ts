import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { ProductEcomDetailComponent } from 'app/entities/product-ecom/product-ecom-detail.component';
import { ProductEcom } from 'app/shared/model/product-ecom.model';

describe('Component Tests', () => {
  describe('ProductEcom Management Detail Component', () => {
    let comp: ProductEcomDetailComponent;
    let fixture: ComponentFixture<ProductEcomDetailComponent>;
    const route = ({ data: of({ product: new ProductEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [ProductEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ProductEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.product).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

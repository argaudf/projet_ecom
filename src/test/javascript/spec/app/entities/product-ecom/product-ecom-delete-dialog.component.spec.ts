import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetEcomTestModule } from '../../../test.module';
import { ProductEcomDeleteDialogComponent } from 'app/entities/product-ecom/product-ecom-delete-dialog.component';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';

describe('Component Tests', () => {
  describe('ProductEcom Management Delete Component', () => {
    let comp: ProductEcomDeleteDialogComponent;
    let fixture: ComponentFixture<ProductEcomDeleteDialogComponent>;
    let service: ProductEcomService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [ProductEcomDeleteDialogComponent]
      })
        .overrideTemplate(ProductEcomDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductEcomDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductEcomService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

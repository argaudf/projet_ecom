import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { ProductEcomUpdateComponent } from 'app/entities/product-ecom/product-ecom-update.component';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';
import { ProductEcom } from 'app/shared/model/product-ecom.model';

describe('Component Tests', () => {
  describe('ProductEcom Management Update Component', () => {
    let comp: ProductEcomUpdateComponent;
    let fixture: ComponentFixture<ProductEcomUpdateComponent>;
    let service: ProductEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [ProductEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ProductEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

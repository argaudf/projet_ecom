import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { HarddriveEcomDetailComponent } from 'app/entities/harddrive-ecom/harddrive-ecom-detail.component';
import { HarddriveEcom } from 'app/shared/model/harddrive-ecom.model';

describe('Component Tests', () => {
  describe('HarddriveEcom Management Detail Component', () => {
    let comp: HarddriveEcomDetailComponent;
    let fixture: ComponentFixture<HarddriveEcomDetailComponent>;
    const route = ({ data: of({ harddrive: new HarddriveEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [HarddriveEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(HarddriveEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HarddriveEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.harddrive).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

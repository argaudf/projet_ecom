import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetEcomTestModule } from '../../../test.module';
import { HarddriveEcomDeleteDialogComponent } from 'app/entities/harddrive-ecom/harddrive-ecom-delete-dialog.component';
import { HarddriveEcomService } from 'app/entities/harddrive-ecom/harddrive-ecom.service';

describe('Component Tests', () => {
  describe('HarddriveEcom Management Delete Component', () => {
    let comp: HarddriveEcomDeleteDialogComponent;
    let fixture: ComponentFixture<HarddriveEcomDeleteDialogComponent>;
    let service: HarddriveEcomService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [HarddriveEcomDeleteDialogComponent]
      })
        .overrideTemplate(HarddriveEcomDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HarddriveEcomDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HarddriveEcomService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { HarddriveEcomUpdateComponent } from 'app/entities/harddrive-ecom/harddrive-ecom-update.component';
import { HarddriveEcomService } from 'app/entities/harddrive-ecom/harddrive-ecom.service';
import { HarddriveEcom } from 'app/shared/model/harddrive-ecom.model';

describe('Component Tests', () => {
  describe('HarddriveEcom Management Update Component', () => {
    let comp: HarddriveEcomUpdateComponent;
    let fixture: ComponentFixture<HarddriveEcomUpdateComponent>;
    let service: HarddriveEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [HarddriveEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(HarddriveEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HarddriveEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HarddriveEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new HarddriveEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new HarddriveEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

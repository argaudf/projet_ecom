import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { CommandProductEcomDetailComponent } from 'app/entities/command-product-ecom/command-product-ecom-detail.component';
import { CommandProductEcom } from 'app/shared/model/command-product-ecom.model';

describe('Component Tests', () => {
  describe('CommandProductEcom Management Detail Component', () => {
    let comp: CommandProductEcomDetailComponent;
    let fixture: ComponentFixture<CommandProductEcomDetailComponent>;
    const route = ({ data: of({ commandProduct: new CommandProductEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [CommandProductEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CommandProductEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CommandProductEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.commandProduct).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

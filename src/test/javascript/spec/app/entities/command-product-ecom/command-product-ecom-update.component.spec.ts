import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { CommandProductEcomUpdateComponent } from 'app/entities/command-product-ecom/command-product-ecom-update.component';
import { CommandProductEcomService } from 'app/entities/command-product-ecom/command-product-ecom.service';
import { CommandProductEcom } from 'app/shared/model/command-product-ecom.model';

describe('Component Tests', () => {
  describe('CommandProductEcom Management Update Component', () => {
    let comp: CommandProductEcomUpdateComponent;
    let fixture: ComponentFixture<CommandProductEcomUpdateComponent>;
    let service: CommandProductEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [CommandProductEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CommandProductEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CommandProductEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CommandProductEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CommandProductEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CommandProductEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

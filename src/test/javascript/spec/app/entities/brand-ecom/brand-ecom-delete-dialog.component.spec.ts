import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetEcomTestModule } from '../../../test.module';
import { BrandEcomDeleteDialogComponent } from 'app/entities/brand-ecom/brand-ecom-delete-dialog.component';
import { BrandEcomService } from 'app/entities/brand-ecom/brand-ecom.service';

describe('Component Tests', () => {
  describe('BrandEcom Management Delete Component', () => {
    let comp: BrandEcomDeleteDialogComponent;
    let fixture: ComponentFixture<BrandEcomDeleteDialogComponent>;
    let service: BrandEcomService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [BrandEcomDeleteDialogComponent]
      })
        .overrideTemplate(BrandEcomDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BrandEcomDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BrandEcomService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

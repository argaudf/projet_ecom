import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { BrandEcomDetailComponent } from 'app/entities/brand-ecom/brand-ecom-detail.component';
import { BrandEcom } from 'app/shared/model/brand-ecom.model';

describe('Component Tests', () => {
  describe('BrandEcom Management Detail Component', () => {
    let comp: BrandEcomDetailComponent;
    let fixture: ComponentFixture<BrandEcomDetailComponent>;
    const route = ({ data: of({ brand: new BrandEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [BrandEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BrandEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BrandEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.brand).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { BrandEcomUpdateComponent } from 'app/entities/brand-ecom/brand-ecom-update.component';
import { BrandEcomService } from 'app/entities/brand-ecom/brand-ecom.service';
import { BrandEcom } from 'app/shared/model/brand-ecom.model';

describe('Component Tests', () => {
  describe('BrandEcom Management Update Component', () => {
    let comp: BrandEcomUpdateComponent;
    let fixture: ComponentFixture<BrandEcomUpdateComponent>;
    let service: BrandEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [BrandEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BrandEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BrandEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BrandEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BrandEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BrandEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { WishListEcomDetailComponent } from 'app/entities/wish-list-ecom/wish-list-ecom-detail.component';
import { WishListEcom } from 'app/shared/model/wish-list-ecom.model';

describe('Component Tests', () => {
  describe('WishListEcom Management Detail Component', () => {
    let comp: WishListEcomDetailComponent;
    let fixture: ComponentFixture<WishListEcomDetailComponent>;
    const route = ({ data: of({ wishList: new WishListEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [WishListEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(WishListEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WishListEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.wishList).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

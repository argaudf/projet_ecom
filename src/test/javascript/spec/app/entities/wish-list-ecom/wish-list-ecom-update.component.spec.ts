import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { WishListEcomUpdateComponent } from 'app/entities/wish-list-ecom/wish-list-ecom-update.component';
import { WishListEcomService } from 'app/entities/wish-list-ecom/wish-list-ecom.service';
import { WishListEcom } from 'app/shared/model/wish-list-ecom.model';

describe('Component Tests', () => {
  describe('WishListEcom Management Update Component', () => {
    let comp: WishListEcomUpdateComponent;
    let fixture: ComponentFixture<WishListEcomUpdateComponent>;
    let service: WishListEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [WishListEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(WishListEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WishListEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WishListEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new WishListEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new WishListEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

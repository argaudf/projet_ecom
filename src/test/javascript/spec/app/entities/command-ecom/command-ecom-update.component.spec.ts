import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { CommandEcomUpdateComponent } from 'app/entities/command-ecom/command-ecom-update.component';
import { CommandEcomService } from 'app/entities/command-ecom/command-ecom.service';
import { CommandEcom } from 'app/shared/model/command-ecom.model';

describe('Component Tests', () => {
  describe('CommandEcom Management Update Component', () => {
    let comp: CommandEcomUpdateComponent;
    let fixture: ComponentFixture<CommandEcomUpdateComponent>;
    let service: CommandEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [CommandEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CommandEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CommandEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CommandEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CommandEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CommandEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

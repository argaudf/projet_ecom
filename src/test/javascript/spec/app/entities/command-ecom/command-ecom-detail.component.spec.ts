import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { CommandEcomDetailComponent } from 'app/entities/command-ecom/command-ecom-detail.component';
import { CommandEcom } from 'app/shared/model/command-ecom.model';

describe('Component Tests', () => {
  describe('CommandEcom Management Detail Component', () => {
    let comp: CommandEcomDetailComponent;
    let fixture: ComponentFixture<CommandEcomDetailComponent>;
    const route = ({ data: of({ command: new CommandEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [CommandEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CommandEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CommandEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.command).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

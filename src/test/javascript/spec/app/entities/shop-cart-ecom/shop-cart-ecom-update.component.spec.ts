import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { ShopCartEcomUpdateComponent } from 'app/entities/shop-cart-ecom/shop-cart-ecom-update.component';
import { ShopCartEcomService } from 'app/entities/shop-cart-ecom/shop-cart-ecom.service';
import { ShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';

describe('Component Tests', () => {
  describe('ShopCartEcom Management Update Component', () => {
    let comp: ShopCartEcomUpdateComponent;
    let fixture: ComponentFixture<ShopCartEcomUpdateComponent>;
    let service: ShopCartEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [ShopCartEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShopCartEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShopCartEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShopCartEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShopCartEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShopCartEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

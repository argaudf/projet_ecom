import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { ShopCartEcomDetailComponent } from 'app/entities/shop-cart-ecom/shop-cart-ecom-detail.component';
import { ShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';

describe('Component Tests', () => {
  describe('ShopCartEcom Management Detail Component', () => {
    let comp: ShopCartEcomDetailComponent;
    let fixture: ComponentFixture<ShopCartEcomDetailComponent>;
    const route = ({ data: of({ shopCart: new ShopCartEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [ShopCartEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShopCartEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShopCartEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shopCart).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

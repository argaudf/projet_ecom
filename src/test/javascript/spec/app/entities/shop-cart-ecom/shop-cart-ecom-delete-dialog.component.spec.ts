import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetEcomTestModule } from '../../../test.module';
import { ShopCartEcomDeleteDialogComponent } from 'app/entities/shop-cart-ecom/shop-cart-ecom-delete-dialog.component';
import { ShopCartEcomService } from 'app/entities/shop-cart-ecom/shop-cart-ecom.service';

describe('Component Tests', () => {
  describe('ShopCartEcom Management Delete Component', () => {
    let comp: ShopCartEcomDeleteDialogComponent;
    let fixture: ComponentFixture<ShopCartEcomDeleteDialogComponent>;
    let service: ShopCartEcomService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [ShopCartEcomDeleteDialogComponent]
      })
        .overrideTemplate(ShopCartEcomDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShopCartEcomDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShopCartEcomService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

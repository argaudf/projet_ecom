import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { CpuEcomUpdateComponent } from 'app/entities/cpu-ecom/cpu-ecom-update.component';
import { CpuEcomService } from 'app/entities/cpu-ecom/cpu-ecom.service';
import { CpuEcom } from 'app/shared/model/cpu-ecom.model';

describe('Component Tests', () => {
  describe('CpuEcom Management Update Component', () => {
    let comp: CpuEcomUpdateComponent;
    let fixture: ComponentFixture<CpuEcomUpdateComponent>;
    let service: CpuEcomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [CpuEcomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CpuEcomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CpuEcomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CpuEcomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CpuEcom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CpuEcom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

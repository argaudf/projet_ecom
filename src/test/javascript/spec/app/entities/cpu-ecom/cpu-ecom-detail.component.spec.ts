import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetEcomTestModule } from '../../../test.module';
import { CpuEcomDetailComponent } from 'app/entities/cpu-ecom/cpu-ecom-detail.component';
import { CpuEcom } from 'app/shared/model/cpu-ecom.model';

describe('Component Tests', () => {
  describe('CpuEcom Management Detail Component', () => {
    let comp: CpuEcomDetailComponent;
    let fixture: ComponentFixture<CpuEcomDetailComponent>;
    const route = ({ data: of({ cpu: new CpuEcom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetEcomTestModule],
        declarations: [CpuEcomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CpuEcomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CpuEcomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cpu).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

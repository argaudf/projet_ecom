package com.mycompany.myapp.service;

import java.util.Optional;

import com.mycompany.myapp.service.dto.CommandProductDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Interface for managing {@link com.mycompany.myapp.domain.CommandProduct}.
 */
public interface CommandProductService {
      /**
     * Save a commandProduct.
     *
     * @param commandProductDTO the entity to save.
     * @return the persisted entity.
     */
    @Transactional(rollbackFor = Throwable.class)
    CommandProductDTO save(CommandProductDTO commandProductDTO);

    /**
     * Get all the commandProducts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommandProductDTO> findAll(Pageable pageable);


    /**
     * Get the "id" commandProduct.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CommandProductDTO> findOne(Long id);

    /**
     * Delete the "id" commandProduct.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the commandProduct corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommandProductDTO> search(String query, Pageable pageable);


}

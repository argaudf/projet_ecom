package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = {BrandMapper.class, CpuMapper.class})
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

    @Mapping(source = "brand.id", target = "brandId")
    @Mapping(source = "cpu.id", target = "cpuId")
    ProductDTO toDto(Product product);

    @Mapping(source = "brandId", target = "brand")
    @Mapping(target = "commandEntries", ignore = true)
    @Mapping(target = "removeCommandEntry", ignore = true)
    @Mapping(target = "shopcarts", ignore = true)
    @Mapping(target = "removeShopcart", ignore = true)
    @Mapping(target = "wishlists", ignore = true)
    @Mapping(target = "removeWishlist", ignore = true)
    @Mapping(source = "cpuId", target = "cpu")
    @Mapping(target = "hds", ignore = true)
    @Mapping(target = "removeHd", ignore = true)
    Product toEntity(ProductDTO productDTO);

    default Product fromId(Long id) {
        if (id == null) {
            return null;
        }
        Product product = new Product();
        product.setId(id);
        return product;
    }
}

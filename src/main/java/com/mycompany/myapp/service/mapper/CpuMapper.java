package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.CpuDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Cpu} and its DTO {@link CpuDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CpuMapper extends EntityMapper<CpuDTO, Cpu> {


    @Mapping(target = "products", ignore = true)
    @Mapping(target = "removeProduct", ignore = true)
    Cpu toEntity(CpuDTO cpuDTO);

    default Cpu fromId(Long id) {
        if (id == null) {
            return null;
        }
        Cpu cpu = new Cpu();
        cpu.setId(id);
        return cpu;
    }
}

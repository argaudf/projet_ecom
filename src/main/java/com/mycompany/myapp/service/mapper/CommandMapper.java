package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.CommandDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Command} and its DTO {@link CommandDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserInfoMapper.class, AddressMapper.class})
public interface CommandMapper extends EntityMapper<CommandDTO, Command> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "address.id", target = "addressId")
    CommandDTO toDto(Command command);

    @Mapping(target = "entries", ignore = true)
    @Mapping(target = "removeEntry", ignore = true)
    @Mapping(source = "userId", target = "user")
    @Mapping(source = "addressId", target = "address")
    Command toEntity(CommandDTO commandDTO);

    default Command fromId(Long id) {
        if (id == null) {
            return null;
        }
        Command command = new Command();
        command.setId(id);
        return command;
    }
}

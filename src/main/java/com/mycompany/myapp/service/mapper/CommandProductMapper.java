package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.CommandProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CommandProduct} and its DTO {@link CommandProductDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, CommandMapper.class})
public interface CommandProductMapper extends EntityMapper<CommandProductDTO, CommandProduct> {

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "command.id", target = "commandId")
    CommandProductDTO toDto(CommandProduct commandProduct);

    @Mapping(source = "productId", target = "product")
    @Mapping(source = "commandId", target = "command")
    CommandProduct toEntity(CommandProductDTO commandProductDTO);

    default CommandProduct fromId(Long id) {
        if (id == null) {
            return null;
        }
        CommandProduct commandProduct = new CommandProduct();
        commandProduct.setId(id);
        return commandProduct;
    }
}

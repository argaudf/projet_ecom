package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.HarddriveDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Harddrive} and its DTO {@link HarddriveDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class})
public interface HarddriveMapper extends EntityMapper<HarddriveDTO, Harddrive> {


    @Mapping(target = "removeProduct", ignore = true)

    default Harddrive fromId(Long id) {
        if (id == null) {
            return null;
        }
        Harddrive harddrive = new Harddrive();
        harddrive.setId(id);
        return harddrive;
    }
}

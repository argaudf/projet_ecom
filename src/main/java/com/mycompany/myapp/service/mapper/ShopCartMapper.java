package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ShopCartDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShopCart} and its DTO {@link ShopCartDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserInfoMapper.class, ProductMapper.class})
public interface ShopCartMapper extends EntityMapper<ShopCartDTO, ShopCart> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "product.id", target = "productId")
    ShopCartDTO toDto(ShopCart shopCart);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "productId", target = "product")
    ShopCart toEntity(ShopCartDTO shopCartDTO);

    default ShopCart fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShopCart shopCart = new ShopCart();
        shopCart.setId(id);
        return shopCart;
    }
}

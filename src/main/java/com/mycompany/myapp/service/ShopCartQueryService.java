package com.mycompany.myapp.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.*; // for static metamodels
import com.mycompany.myapp.repository.ShopCartRepository;
import com.mycompany.myapp.repository.search.ShopCartSearchRepository;
import com.mycompany.myapp.service.dto.ShopCartCriteria;
import com.mycompany.myapp.service.dto.ShopCartDTO;
import com.mycompany.myapp.service.mapper.ShopCartMapper;

/**
 * Service for executing complex queries for {@link ShopCart} entities in the database.
 * The main input is a {@link ShopCartCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShopCartDTO} or a {@link Page} of {@link ShopCartDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShopCartQueryService extends QueryService<ShopCart> {

    private final Logger log = LoggerFactory.getLogger(ShopCartQueryService.class);

    private final ShopCartRepository shopCartRepository;

    private final ShopCartMapper shopCartMapper;

    private final ShopCartSearchRepository shopCartSearchRepository;

    public ShopCartQueryService(ShopCartRepository shopCartRepository, ShopCartMapper shopCartMapper, ShopCartSearchRepository shopCartSearchRepository) {
        this.shopCartRepository = shopCartRepository;
        this.shopCartMapper = shopCartMapper;
        this.shopCartSearchRepository = shopCartSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ShopCartDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShopCartDTO> findByCriteria(ShopCartCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShopCart> specification = createSpecification(criteria);
        return shopCartMapper.toDto(shopCartRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShopCartDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShopCartDTO> findByCriteria(ShopCartCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShopCart> specification = createSpecification(criteria);
        return shopCartRepository.findAll(specification, page)
            .map(shopCartMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShopCartCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShopCart> specification = createSpecification(criteria);
        return shopCartRepository.count(specification);
    }

    /**
     * Function to convert {@link ShopCartCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ShopCart> createSpecification(ShopCartCriteria criteria) {
        Specification<ShopCart> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShopCart_.id));
            }
            if (criteria.getQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantity(), ShopCart_.quantity));
            }
            if (criteria.getAddTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAddTime(), ShopCart_.addTime));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(ShopCart_.user, JoinType.LEFT).get(UserInfo_.id)));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductId(),
                    root -> root.join(ShopCart_.product, JoinType.LEFT).get(Product_.id)));
            }
        }
        return specification;
    }
}

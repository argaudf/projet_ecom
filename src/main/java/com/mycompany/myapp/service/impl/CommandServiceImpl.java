package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.CommandService;
import com.mycompany.myapp.domain.Command;
import com.mycompany.myapp.repository.CommandRepository;
import com.mycompany.myapp.repository.search.CommandSearchRepository;
import com.mycompany.myapp.service.dto.CommandDTO;
import com.mycompany.myapp.service.mapper.CommandMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Command}.
 */
@Service
@Transactional
public class CommandServiceImpl implements CommandService {

    private final Logger log = LoggerFactory.getLogger(CommandServiceImpl.class);

    private final CommandRepository commandRepository;

    private final CommandMapper commandMapper;

    private final CommandSearchRepository commandSearchRepository;

    public CommandServiceImpl(CommandRepository commandRepository, CommandMapper commandMapper, CommandSearchRepository commandSearchRepository) {
        this.commandRepository = commandRepository;
        this.commandMapper = commandMapper;
        this.commandSearchRepository = commandSearchRepository;
    }

    /**
     * Save a command.
     *
     * @param commandDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CommandDTO save(CommandDTO commandDTO) {
        log.debug("Request to save Command : {}", commandDTO);
        Command command = commandMapper.toEntity(commandDTO);
        command = commandRepository.save(command);
        CommandDTO result = commandMapper.toDto(command);
        commandSearchRepository.save(command);
        return result;
    }

    /**
     * Get all the commands.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CommandDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Commands");
        return commandRepository.findAll(pageable)
            .map(commandMapper::toDto);
    }


    /**
     * Get one command by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CommandDTO> findOne(Long id) {
        log.debug("Request to get Command : {}", id);
        return commandRepository.findById(id)
            .map(commandMapper::toDto);
    }

    /**
     * Delete the command by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Command : {}", id);
        commandRepository.deleteById(id);
        commandSearchRepository.deleteById(id);
    }

    /**
     * Search for the command corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CommandDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Commands for query {}", query);
        return commandSearchRepository.search(queryStringQuery(query), pageable)
            .map(commandMapper::toDto);
    }
}

package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.HarddriveService;
import com.mycompany.myapp.domain.Harddrive;
import com.mycompany.myapp.repository.HarddriveRepository;
import com.mycompany.myapp.repository.search.HarddriveSearchRepository;
import com.mycompany.myapp.service.dto.HarddriveDTO;
import com.mycompany.myapp.service.mapper.HarddriveMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Harddrive}.
 */
@Service
@Transactional
public class HarddriveServiceImpl implements HarddriveService {

    private final Logger log = LoggerFactory.getLogger(HarddriveServiceImpl.class);

    private final HarddriveRepository harddriveRepository;

    private final HarddriveMapper harddriveMapper;

    private final HarddriveSearchRepository harddriveSearchRepository;

    public HarddriveServiceImpl(HarddriveRepository harddriveRepository, HarddriveMapper harddriveMapper, HarddriveSearchRepository harddriveSearchRepository) {
        this.harddriveRepository = harddriveRepository;
        this.harddriveMapper = harddriveMapper;
        this.harddriveSearchRepository = harddriveSearchRepository;
    }

    /**
     * Save a harddrive.
     *
     * @param harddriveDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HarddriveDTO save(HarddriveDTO harddriveDTO) {
        log.debug("Request to save Harddrive : {}", harddriveDTO);
        Harddrive harddrive = harddriveMapper.toEntity(harddriveDTO);
        harddrive = harddriveRepository.save(harddrive);
        HarddriveDTO result = harddriveMapper.toDto(harddrive);
        harddriveSearchRepository.save(harddrive);
        return result;
    }

    /**
     * Get all the harddrives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HarddriveDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Harddrives");
        return harddriveRepository.findAll(pageable)
            .map(harddriveMapper::toDto);
    }

    /**
     * Get all the harddrives with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<HarddriveDTO> findAllWithEagerRelationships(Pageable pageable) {
        return harddriveRepository.findAllWithEagerRelationships(pageable).map(harddriveMapper::toDto);
    }
    

    /**
     * Get one harddrive by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HarddriveDTO> findOne(Long id) {
        log.debug("Request to get Harddrive : {}", id);
        return harddriveRepository.findOneWithEagerRelationships(id)
            .map(harddriveMapper::toDto);
    }

    /**
     * Delete the harddrive by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Harddrive : {}", id);
        harddriveRepository.deleteById(id);
        harddriveSearchRepository.deleteById(id);
    }

    /**
     * Search for the harddrive corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HarddriveDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Harddrives for query {}", query);
        return harddriveSearchRepository.search(queryStringQuery(query), pageable)
            .map(harddriveMapper::toDto);
    }
}

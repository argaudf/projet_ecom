package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.CpuService;
import com.mycompany.myapp.domain.Cpu;
import com.mycompany.myapp.repository.CpuRepository;
import com.mycompany.myapp.repository.search.CpuSearchRepository;
import com.mycompany.myapp.service.dto.CpuDTO;
import com.mycompany.myapp.service.mapper.CpuMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Cpu}.
 */
@Service
@Transactional
public class CpuServiceImpl implements CpuService {

    private final Logger log = LoggerFactory.getLogger(CpuServiceImpl.class);

    private final CpuRepository cpuRepository;

    private final CpuMapper cpuMapper;

    private final CpuSearchRepository cpuSearchRepository;

    public CpuServiceImpl(CpuRepository cpuRepository, CpuMapper cpuMapper, CpuSearchRepository cpuSearchRepository) {
        this.cpuRepository = cpuRepository;
        this.cpuMapper = cpuMapper;
        this.cpuSearchRepository = cpuSearchRepository;
    }

    /**
     * Save a cpu.
     *
     * @param cpuDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CpuDTO save(CpuDTO cpuDTO) {
        log.debug("Request to save Cpu : {}", cpuDTO);
        Cpu cpu = cpuMapper.toEntity(cpuDTO);
        cpu = cpuRepository.save(cpu);
        CpuDTO result = cpuMapper.toDto(cpu);
        cpuSearchRepository.save(cpu);
        return result;
    }

    /**
     * Get all the cpus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CpuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Cpus");
        return cpuRepository.findAll(pageable)
            .map(cpuMapper::toDto);
    }


    /**
     * Get one cpu by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CpuDTO> findOne(Long id) {
        log.debug("Request to get Cpu : {}", id);
        return cpuRepository.findById(id)
            .map(cpuMapper::toDto);
    }

    /**
     * Delete the cpu by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Cpu : {}", id);
        cpuRepository.deleteById(id);
        cpuSearchRepository.deleteById(id);
    }

    /**
     * Search for the cpu corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CpuDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Cpus for query {}", query);
        return cpuSearchRepository.search(queryStringQuery(query), pageable)
            .map(cpuMapper::toDto);
    }
}

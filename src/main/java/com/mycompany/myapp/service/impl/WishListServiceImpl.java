package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.WishListService;
import com.mycompany.myapp.domain.WishList;
import com.mycompany.myapp.repository.WishListRepository;
import com.mycompany.myapp.repository.search.WishListSearchRepository;
import com.mycompany.myapp.service.dto.WishListDTO;
import com.mycompany.myapp.service.mapper.WishListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link WishList}.
 */
@Service
@Transactional
public class WishListServiceImpl implements WishListService {

    private final Logger log = LoggerFactory.getLogger(WishListServiceImpl.class);

    private final WishListRepository wishListRepository;

    private final WishListMapper wishListMapper;

    private final WishListSearchRepository wishListSearchRepository;

    public WishListServiceImpl(WishListRepository wishListRepository, WishListMapper wishListMapper, WishListSearchRepository wishListSearchRepository) {
        this.wishListRepository = wishListRepository;
        this.wishListMapper = wishListMapper;
        this.wishListSearchRepository = wishListSearchRepository;
    }

    /**
     * Save a wishList.
     *
     * @param wishListDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WishListDTO save(WishListDTO wishListDTO) {
        log.debug("Request to save WishList : {}", wishListDTO);
        WishList wishList = wishListMapper.toEntity(wishListDTO);
        wishList = wishListRepository.save(wishList);
        WishListDTO result = wishListMapper.toDto(wishList);
        wishListSearchRepository.save(wishList);
        return result;
    }

    /**
     * Get all the wishLists.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WishListDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WishLists");
        return wishListRepository.findAll(pageable)
            .map(wishListMapper::toDto);
    }

    /**
     * Get all the wishLists with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<WishListDTO> findAllWithEagerRelationships(Pageable pageable) {
        return wishListRepository.findAllWithEagerRelationships(pageable).map(wishListMapper::toDto);
    }
    

    /**
     * Get one wishList by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WishListDTO> findOne(Long id) {
        log.debug("Request to get WishList : {}", id);
        return wishListRepository.findOneWithEagerRelationships(id)
            .map(wishListMapper::toDto);
    }

    /**
     * Delete the wishList by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WishList : {}", id);
        wishListRepository.deleteById(id);
        wishListSearchRepository.deleteById(id);
    }

    /**
     * Search for the wishList corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WishListDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WishLists for query {}", query);
        return wishListSearchRepository.search(queryStringQuery(query), pageable)
            .map(wishListMapper::toDto);
    }
}

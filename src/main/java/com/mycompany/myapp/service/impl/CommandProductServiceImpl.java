package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.CommandProductService;
import com.mycompany.myapp.domain.CommandProduct;
import com.mycompany.myapp.repository.CommandProductRepository;
import com.mycompany.myapp.repository.search.CommandProductSearchRepository;
import com.mycompany.myapp.service.dto.CommandProductDTO;
import com.mycompany.myapp.service.mapper.CommandProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CommandProduct}.
 */
@Service
@Transactional
public class CommandProductServiceImpl implements CommandProductService {

    private final Logger log = LoggerFactory.getLogger(CommandProductServiceImpl.class);

    private final CommandProductRepository commandProductRepository;

    private final CommandProductMapper commandProductMapper;

    private final CommandProductSearchRepository commandProductSearchRepository;

    public CommandProductServiceImpl(CommandProductRepository commandProductRepository, CommandProductMapper commandProductMapper, CommandProductSearchRepository commandProductSearchRepository) {
        this.commandProductRepository = commandProductRepository;
        this.commandProductMapper = commandProductMapper;
        this.commandProductSearchRepository = commandProductSearchRepository;
    }

    /**
     * Save a commandProduct.
     *
     * @param commandProductDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CommandProductDTO save(CommandProductDTO commandProductDTO) {
        log.debug("Request to save CommandProduct : {}", commandProductDTO);
        CommandProduct commandProduct = commandProductMapper.toEntity(commandProductDTO);
        commandProduct = commandProductRepository.save(commandProduct);
        CommandProductDTO result = commandProductMapper.toDto(commandProduct);
        commandProductSearchRepository.save(commandProduct);
        return result;
    }

    /**
     * Get all the commandProducts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CommandProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommandProducts");
        return commandProductRepository.findAll(pageable)
            .map(commandProductMapper::toDto);
    }


    /**
     * Get one commandProduct by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CommandProductDTO> findOne(Long id) {
        log.debug("Request to get CommandProduct : {}", id);
        return commandProductRepository.findById(id)
            .map(commandProductMapper::toDto);
    }

    /**
     * Delete the commandProduct by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CommandProduct : {}", id);
        commandProductRepository.deleteById(id);
        commandProductSearchRepository.deleteById(id);
    }

    /**
     * Search for the commandProduct corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CommandProductDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CommandProducts for query {}", query);
        return commandProductSearchRepository.search(queryStringQuery(query), pageable)
            .map(commandProductMapper::toDto);
    }
}

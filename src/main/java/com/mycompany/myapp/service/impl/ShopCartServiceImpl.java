package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.ShopCartService;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.repository.ShopCartRepository;
import com.mycompany.myapp.repository.search.ShopCartSearchRepository;
import com.mycompany.myapp.service.dto.ShopCartDTO;
import com.mycompany.myapp.service.mapper.ShopCartMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ShopCart}.
 */
@Service
@Transactional
public class ShopCartServiceImpl implements ShopCartService {

    private final Logger log = LoggerFactory.getLogger(ShopCartServiceImpl.class);

    private final ShopCartRepository shopCartRepository;

    private final ShopCartMapper shopCartMapper;

    private final ShopCartSearchRepository shopCartSearchRepository;

    public ShopCartServiceImpl(ShopCartRepository shopCartRepository, ShopCartMapper shopCartMapper, ShopCartSearchRepository shopCartSearchRepository) {
        this.shopCartRepository = shopCartRepository;
        this.shopCartMapper = shopCartMapper;
        this.shopCartSearchRepository = shopCartSearchRepository;
    }

    /**
     * Save a shopCart.
     *
     * @param shopCartDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShopCartDTO save(ShopCartDTO shopCartDTO) {
        log.debug("Request to save ShopCart : {}", shopCartDTO);
        ShopCart shopCart = shopCartMapper.toEntity(shopCartDTO);
        shopCart = shopCartRepository.save(shopCart);
        ShopCartDTO result = shopCartMapper.toDto(shopCart);
        shopCartSearchRepository.save(shopCart);
        return result;
    }

    /**
     * Get all the shopCarts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShopCartDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShopCarts");
        return shopCartRepository.findAll(pageable)
            .map(shopCartMapper::toDto);
    }


    /**
     * Get one shopCart by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShopCartDTO> findOne(Long id) {
        log.debug("Request to get ShopCart : {}", id);
        return shopCartRepository.findById(id)
            .map(shopCartMapper::toDto);
    }
    @Override
    @Transactional(readOnly = true)
	public Optional<ShopCartDTO> findByPro(Long id) {
    	 log.debug("Request to get ShopCart : {}", id);
		// TODO Auto-generated method stub
		return shopCartRepository.findByPro(id).map(shopCartMapper::toDto);
	}

    /**
     * Delete the shopCart by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShopCart : {}", id);
        shopCartRepository.deleteById(id);
        shopCartSearchRepository.deleteById(id);
    }
    @Override
    public void updateStock(Long id) {
    	 log.debug("Request to update ShopCart : {}", id);
    	 shopCartRepository.updateStock(id);
    }
    /**
     * Search for the shopCart corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShopCartDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShopCarts for query {}", query);
        return shopCartSearchRepository.search(queryStringQuery(query), pageable)
            .map(shopCartMapper::toDto);
    }

	@Override
	public Optional<ShopCartDTO> findOneByuid(Long id) {
		// TODO Auto-generated method stub
		return shopCartRepository.findOneByuid(id).map(shopCartMapper::toDto);
	}

	/*
	 * @Override public List<ShopCartDTO> findAllByuid(Long id) { // TODO
	 * Auto-generated method stub return shopCartRepository.findAllByuid(id).; }
	 */

	
	
	/*
	 * @Override public Optional<ShopCartDTO> findByuid(Long id) {
	 * log.debug("Request to get ShopCart : {}", id); return
	 * shopCartRepository.findOneByuid(id).map(shopCartMapper::toDto); }
	 */
}

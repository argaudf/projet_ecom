package com.mycompany.myapp.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Address} entity.
 */
public class AddressDTO implements Serializable {

    private Long id;

    private String addressComplete;

    private String receiveName;

    private String receiveTel;


    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressComplete() {
        return addressComplete;
    }

    public void setAddressComplete(String addressComplete) {
        this.addressComplete = addressComplete;
    }

    public String getReceiveName() {
        return receiveName;
    }

    public void setReceiveName(String receiveName) {
        this.receiveName = receiveName;
    }

    public String getReceiveTel() {
        return receiveTel;
    }

    public void setReceiveTel(String receiveTel) {
        this.receiveTel = receiveTel;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userInfoId) {
        this.userId = userInfoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AddressDTO addressDTO = (AddressDTO) o;
        if (addressDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), addressDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AddressDTO{" +
            "id=" + getId() +
            ", addressComplete='" + getAddressComplete() + "'" +
            ", receiveName='" + getReceiveName() + "'" +
            ", receiveTel='" + getReceiveTel() + "'" +
            ", user=" + getUserId() +
            "}";
    }
}

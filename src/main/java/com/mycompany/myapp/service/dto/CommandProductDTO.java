package com.mycompany.myapp.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.CommandProduct} entity.
 */
public class CommandProductDTO implements Serializable {

    private Long id;

    private Integer quantity;

    private LocalDate addTime;


    private Long productId;

    private Long commandId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public LocalDate getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDate addTime) {
        this.addTime = addTime;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCommandId() {
        return commandId;
    }

    public void setCommandId(Long commandId) {
        this.commandId = commandId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CommandProductDTO commandProductDTO = (CommandProductDTO) o;
        if (commandProductDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), commandProductDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CommandProductDTO{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            ", addTime='" + getAddTime() + "'" +
            ", product=" + getProductId() +
            ", command=" + getCommandId() +
            "}";
    }
}

package com.mycompany.myapp.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.ShopCart} entity.
 */
public class ShopCartDTO implements Serializable {

    private Long id;

    private Integer quantity;

    private LocalDate addTime;


    private Long userId;

    private Long productId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public LocalDate getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDate addTime) {
        this.addTime = addTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userInfoId) {
        this.userId = userInfoId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShopCartDTO shopCartDTO = (ShopCartDTO) o;
        if (shopCartDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shopCartDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShopCartDTO{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            ", addTime='" + getAddTime() + "'" +
            ", user=" + getUserId() +
            ", product=" + getProductId() +
            "}";
    }
}

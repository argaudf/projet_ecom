package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mycompany.myapp.domain.UserInfo} entity. This class is used
 * in {@link com.mycompany.myapp.web.rest.UserInfoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-infos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserInfoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter userStatus;

    private StringFilter tel;

    private StringFilter email;

    private StringFilter passwordQ;

    private StringFilter passwordA;

    private LongFilter userId;

    private LongFilter commandsId;

    private LongFilter shopcartId;

    private LongFilter addressId;

    public UserInfoCriteria(){
    }

    public UserInfoCriteria(UserInfoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.userStatus = other.userStatus == null ? null : other.userStatus.copy();
        this.tel = other.tel == null ? null : other.tel.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.passwordQ = other.passwordQ == null ? null : other.passwordQ.copy();
        this.passwordA = other.passwordA == null ? null : other.passwordA.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.commandsId = other.commandsId == null ? null : other.commandsId.copy();
        this.shopcartId = other.shopcartId == null ? null : other.shopcartId.copy();
        this.addressId = other.addressId == null ? null : other.addressId.copy();
    }

    @Override
    public UserInfoCriteria copy() {
        return new UserInfoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(StringFilter userStatus) {
        this.userStatus = userStatus;
    }

    public StringFilter getTel() {
        return tel;
    }

    public void setTel(StringFilter tel) {
        this.tel = tel;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getPasswordQ() {
        return passwordQ;
    }

    public void setPasswordQ(StringFilter passwordQ) {
        this.passwordQ = passwordQ;
    }

    public StringFilter getPasswordA() {
        return passwordA;
    }

    public void setPasswordA(StringFilter passwordA) {
        this.passwordA = passwordA;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getCommandsId() {
        return commandsId;
    }

    public void setCommandsId(LongFilter commandsId) {
        this.commandsId = commandsId;
    }

    public LongFilter getShopcartId() {
        return shopcartId;
    }

    public void setShopcartId(LongFilter shopcartId) {
        this.shopcartId = shopcartId;
    }

    public LongFilter getAddressId() {
        return addressId;
    }

    public void setAddressId(LongFilter addressId) {
        this.addressId = addressId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserInfoCriteria that = (UserInfoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(userStatus, that.userStatus) &&
            Objects.equals(tel, that.tel) &&
            Objects.equals(email, that.email) &&
            Objects.equals(passwordQ, that.passwordQ) &&
            Objects.equals(passwordA, that.passwordA) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(commandsId, that.commandsId) &&
            Objects.equals(shopcartId, that.shopcartId) &&
            Objects.equals(addressId, that.addressId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        userStatus,
        tel,
        email,
        passwordQ,
        passwordA,
        userId,
        commandsId,
        shopcartId,
        addressId
        );
    }

    @Override
    public String toString() {
        return "UserInfoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (userStatus != null ? "userStatus=" + userStatus + ", " : "") +
                (tel != null ? "tel=" + tel + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (passwordQ != null ? "passwordQ=" + passwordQ + ", " : "") +
                (passwordA != null ? "passwordA=" + passwordA + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (commandsId != null ? "commandsId=" + commandsId + ", " : "") +
                (shopcartId != null ? "shopcartId=" + shopcartId + ", " : "") +
                (addressId != null ? "addressId=" + addressId + ", " : "") +
            "}";
    }

}

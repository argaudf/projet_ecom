package com.mycompany.myapp.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Command} entity.
 */
public class CommandDTO implements Serializable {

    private Long id;

    private Integer status;


    private Long userId;

    private Long addressId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userInfoId) {
        this.userId = userInfoId;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CommandDTO commandDTO = (CommandDTO) o;
        if (commandDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), commandDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CommandDTO{" +
            "id=" + getId() +
            ", status=" + getStatus() +
            ", user=" + getUserId() +
            ", address=" + getAddressId() +
            "}";
    }
}

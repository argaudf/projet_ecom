package com.mycompany.myapp.service.dto;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.WishList} entity.
 */
public class WishListDTO implements Serializable {

    private Long id;


    private Long userId;

    private Set<ProductDTO> products = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userInfoId) {
        this.userId = userInfoId;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WishListDTO wishListDTO = (WishListDTO) o;
        if (wishListDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wishListDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WishListDTO{" +
            "id=" + getId() +
            ", user=" + getUserId() +
            "}";
    }
}

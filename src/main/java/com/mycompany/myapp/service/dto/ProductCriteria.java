package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mycompany.myapp.domain.Product} entity. This class is used
 * in {@link com.mycompany.myapp.web.rest.ProductResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /products?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProductCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter proName;

    private DoubleFilter proPrice;

    private IntegerFilter stock;

    private StringFilter description;

    private StringFilter picture;

    private LongFilter brandId;

    private LongFilter commandEntryId;

    private LongFilter shopcartId;

    private LongFilter wishlistId;

    private LongFilter cpuId;

    private LongFilter hdId;

    public ProductCriteria(){
    }

    public ProductCriteria(ProductCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.proName = other.proName == null ? null : other.proName.copy();
        this.proPrice = other.proPrice == null ? null : other.proPrice.copy();
        this.stock = other.stock == null ? null : other.stock.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.picture = other.picture == null ? null : other.picture.copy();
        this.brandId = other.brandId == null ? null : other.brandId.copy();
        this.commandEntryId = other.commandEntryId == null ? null : other.commandEntryId.copy();
        this.shopcartId = other.shopcartId == null ? null : other.shopcartId.copy();
        this.wishlistId = other.wishlistId == null ? null : other.wishlistId.copy();
        this.cpuId = other.cpuId == null ? null : other.cpuId.copy();
        this.hdId = other.hdId == null ? null : other.hdId.copy();
    }

    @Override
    public ProductCriteria copy() {
        return new ProductCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getProName() {
        return proName;
    }

    public void setProName(StringFilter proName) {
        this.proName = proName;
    }

    public DoubleFilter getProPrice() {
        return proPrice;
    }

    public void setProPrice(DoubleFilter proPrice) {
        this.proPrice = proPrice;
    }

    public IntegerFilter getStock() {
        return stock;
    }

    public void setStock(IntegerFilter stock) {
        this.stock = stock;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getPicture() {
        return picture;
    }

    public void setPicture(StringFilter picture) {
        this.picture = picture;
    }

    public LongFilter getBrandId() {
        return brandId;
    }

    public void setBrandId(LongFilter brandId) {
        this.brandId = brandId;
    }

    public LongFilter getCommandEntryId() {
        return commandEntryId;
    }

    public void setCommandEntryId(LongFilter commandEntryId) {
        this.commandEntryId = commandEntryId;
    }

    public LongFilter getShopcartId() {
        return shopcartId;
    }

    public void setShopcartId(LongFilter shopcartId) {
        this.shopcartId = shopcartId;
    }

    public LongFilter getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(LongFilter wishlistId) {
        this.wishlistId = wishlistId;
    }

    public LongFilter getCpuId() {
        return cpuId;
    }

    public void setCpuId(LongFilter cpuId) {
        this.cpuId = cpuId;
    }

    public LongFilter getHdId() {
        return hdId;
    }

    public void setHdId(LongFilter hdId) {
        this.hdId = hdId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProductCriteria that = (ProductCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(proName, that.proName) &&
            Objects.equals(proPrice, that.proPrice) &&
            Objects.equals(stock, that.stock) &&
            Objects.equals(description, that.description) &&
            Objects.equals(picture, that.picture) &&
            Objects.equals(brandId, that.brandId) &&
            Objects.equals(commandEntryId, that.commandEntryId) &&
            Objects.equals(shopcartId, that.shopcartId) &&
            Objects.equals(wishlistId, that.wishlistId) &&
            Objects.equals(cpuId, that.cpuId) &&
            Objects.equals(hdId, that.hdId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        proName,
        proPrice,
        stock,
        description,
        picture,
        brandId,
        commandEntryId,
        shopcartId,
        wishlistId,
        cpuId,
        hdId
        );
    }

    @Override
    public String toString() {
        return "ProductCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (proName != null ? "proName=" + proName + ", " : "") +
                (proPrice != null ? "proPrice=" + proPrice + ", " : "") +
                (stock != null ? "stock=" + stock + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (picture != null ? "picture=" + picture + ", " : "") +
                (brandId != null ? "brandId=" + brandId + ", " : "") +
                (commandEntryId != null ? "commandEntryId=" + commandEntryId + ", " : "") +
                (shopcartId != null ? "shopcartId=" + shopcartId + ", " : "") +
                (wishlistId != null ? "wishlistId=" + wishlistId + ", " : "") +
                (cpuId != null ? "cpuId=" + cpuId + ", " : "") +
                (hdId != null ? "hdId=" + hdId + ", " : "") +
            "}";
    }

}

package com.mycompany.myapp.service.dto;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Harddrive} entity.
 */
public class HarddriveDTO implements Serializable {

    private Long id;

    private Integer size;


    private Set<ProductDTO> products = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HarddriveDTO harddriveDTO = (HarddriveDTO) o;
        if (harddriveDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), harddriveDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HarddriveDTO{" +
            "id=" + getId() +
            ", size=" + getSize() +
            "}";
    }
}

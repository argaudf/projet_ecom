package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mycompany.myapp.domain.CommandProduct} entity. This class is used
 * in {@link com.mycompany.myapp.web.rest.CommandProductResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /command-products?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CommandProductCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter quantity;

    private LocalDateFilter addTime;

    private LongFilter productId;

    private LongFilter commandId;

    public CommandProductCriteria(){
    }

    public CommandProductCriteria(CommandProductCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.quantity = other.quantity == null ? null : other.quantity.copy();
        this.addTime = other.addTime == null ? null : other.addTime.copy();
        this.productId = other.productId == null ? null : other.productId.copy();
        this.commandId = other.commandId == null ? null : other.commandId.copy();
    }

    @Override
    public CommandProductCriteria copy() {
        return new CommandProductCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getQuantity() {
        return quantity;
    }

    public void setQuantity(IntegerFilter quantity) {
        this.quantity = quantity;
    }

    public LocalDateFilter getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateFilter addTime) {
        this.addTime = addTime;
    }

    public LongFilter getProductId() {
        return productId;
    }

    public void setProductId(LongFilter productId) {
        this.productId = productId;
    }

    public LongFilter getCommandId() {
        return commandId;
    }

    public void setCommandId(LongFilter commandId) {
        this.commandId = commandId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CommandProductCriteria that = (CommandProductCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(quantity, that.quantity) &&
            Objects.equals(addTime, that.addTime) &&
            Objects.equals(productId, that.productId) &&
            Objects.equals(commandId, that.commandId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        quantity,
        addTime,
        productId,
        commandId
        );
    }

    @Override
    public String toString() {
        return "CommandProductCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (quantity != null ? "quantity=" + quantity + ", " : "") +
                (addTime != null ? "addTime=" + addTime + ", " : "") +
                (productId != null ? "productId=" + productId + ", " : "") +
                (commandId != null ? "commandId=" + commandId + ", " : "") +
            "}";
    }

}

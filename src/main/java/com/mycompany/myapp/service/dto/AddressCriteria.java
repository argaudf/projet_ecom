package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mycompany.myapp.domain.Address} entity. This class is used
 * in {@link com.mycompany.myapp.web.rest.AddressResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /addresses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AddressCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter addressComplete;

    private StringFilter receiveName;

    private StringFilter receiveTel;

    private LongFilter userId;

    private LongFilter commandId;

    public AddressCriteria(){
    }

    public AddressCriteria(AddressCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.addressComplete = other.addressComplete == null ? null : other.addressComplete.copy();
        this.receiveName = other.receiveName == null ? null : other.receiveName.copy();
        this.receiveTel = other.receiveTel == null ? null : other.receiveTel.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.commandId = other.commandId == null ? null : other.commandId.copy();
    }

    @Override
    public AddressCriteria copy() {
        return new AddressCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAddressComplete() {
        return addressComplete;
    }

    public void setAddressComplete(StringFilter addressComplete) {
        this.addressComplete = addressComplete;
    }

    public StringFilter getReceiveName() {
        return receiveName;
    }

    public void setReceiveName(StringFilter receiveName) {
        this.receiveName = receiveName;
    }

    public StringFilter getReceiveTel() {
        return receiveTel;
    }

    public void setReceiveTel(StringFilter receiveTel) {
        this.receiveTel = receiveTel;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getCommandId() {
        return commandId;
    }

    public void setCommandId(LongFilter commandId) {
        this.commandId = commandId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AddressCriteria that = (AddressCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(addressComplete, that.addressComplete) &&
            Objects.equals(receiveName, that.receiveName) &&
            Objects.equals(receiveTel, that.receiveTel) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(commandId, that.commandId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        addressComplete,
        receiveName,
        receiveTel,
        userId,
        commandId
        );
    }

    @Override
    public String toString() {
        return "AddressCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (addressComplete != null ? "addressComplete=" + addressComplete + ", " : "") +
                (receiveName != null ? "receiveName=" + receiveName + ", " : "") +
                (receiveTel != null ? "receiveTel=" + receiveTel + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (commandId != null ? "commandId=" + commandId + ", " : "") +
            "}";
    }

}

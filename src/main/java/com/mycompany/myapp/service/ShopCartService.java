package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.service.dto.ShopCartDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.mycompany.myapp.domain.ShopCart}.
 */
public interface ShopCartService {

    /**
     * Save a shopCart.
     *
     * @param shopCartDTO the entity to save.
     * @return the persisted entity.
     */
    @Transactional(rollbackFor = Throwable.class)
    ShopCartDTO save(ShopCartDTO shopCartDTO);

    /**
     * Get all the shopCarts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShopCartDTO> findAll(Pageable pageable);
    void updateStock(Long id);
    Optional<ShopCartDTO> findByPro(Long id);
    /**
     * Get the "id" shopCart.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShopCartDTO> findOne(Long id);
   // List<ShopCartDTO> findAllByuid(Long id);
    /**
     * Delete the "id" shopCart.
     *
     * @param id the id of the entity.
     */
    @Transactional(rollbackFor = Throwable.class)
    void delete(Long id);

    /**
     * Search for the shopCart corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShopCartDTO> search(String query, Pageable pageable);
    Optional<ShopCartDTO> findOneByuid(Long id);
   
}

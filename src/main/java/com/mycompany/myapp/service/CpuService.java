package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.CpuDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.mycompany.myapp.domain.Cpu}.
 */
public interface CpuService {

    /**
     * Save a cpu.
     *
     * @param cpuDTO the entity to save.
     * @return the persisted entity.
     */
    CpuDTO save(CpuDTO cpuDTO);

    /**
     * Get all the cpus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CpuDTO> findAll(Pageable pageable);


    /**
     * Get the "id" cpu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CpuDTO> findOne(Long id);

    /**
     * Delete the "id" cpu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the cpu corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CpuDTO> search(String query, Pageable pageable);
}

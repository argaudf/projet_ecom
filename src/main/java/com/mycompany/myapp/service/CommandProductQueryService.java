package com.mycompany.myapp.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mycompany.myapp.domain.CommandProduct;
import com.mycompany.myapp.domain.*; // for static metamodels
import com.mycompany.myapp.repository.CommandProductRepository;
import com.mycompany.myapp.repository.search.CommandProductSearchRepository;
import com.mycompany.myapp.service.dto.CommandProductCriteria;
import com.mycompany.myapp.service.dto.CommandProductDTO;
import com.mycompany.myapp.service.mapper.CommandProductMapper;

/**
 * Service for executing complex queries for {@link CommandProduct} entities in the database.
 * The main input is a {@link CommandProductCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommandProductDTO} or a {@link Page} of {@link CommandProductDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommandProductQueryService extends QueryService<CommandProduct> {

    private final Logger log = LoggerFactory.getLogger(CommandProductQueryService.class);

    private final CommandProductRepository commandProductRepository;

    private final CommandProductMapper commandProductMapper;

    private final CommandProductSearchRepository commandProductSearchRepository;

    public CommandProductQueryService(CommandProductRepository commandProductRepository, CommandProductMapper commandProductMapper, CommandProductSearchRepository commandProductSearchRepository) {
        this.commandProductRepository = commandProductRepository;
        this.commandProductMapper = commandProductMapper;
        this.commandProductSearchRepository = commandProductSearchRepository;
    }

    /**
     * Return a {@link List} of {@link CommandProductDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommandProductDTO> findByCriteria(CommandProductCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CommandProduct> specification = createSpecification(criteria);
        return commandProductMapper.toDto(commandProductRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CommandProductDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommandProductDTO> findByCriteria(CommandProductCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CommandProduct> specification = createSpecification(criteria);
        return commandProductRepository.findAll(specification, page)
            .map(commandProductMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommandProductCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CommandProduct> specification = createSpecification(criteria);
        return commandProductRepository.count(specification);
    }

    /**
     * Function to convert {@link CommandProductCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CommandProduct> createSpecification(CommandProductCriteria criteria) {
        Specification<CommandProduct> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CommandProduct_.id));
            }
            if (criteria.getQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantity(), CommandProduct_.quantity));
            }
            if (criteria.getAddTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAddTime(), CommandProduct_.addTime));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductId(),
                    root -> root.join(CommandProduct_.product, JoinType.LEFT).get(Product_.id)));
            }
            if (criteria.getCommandId() != null) {
                specification = specification.and(buildSpecification(criteria.getCommandId(),
                    root -> root.join(CommandProduct_.command, JoinType.LEFT).get(Command_.id)));
            }
        }
        return specification;
    }
}

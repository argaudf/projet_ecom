package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.HarddriveDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.mycompany.myapp.domain.Harddrive}.
 */
public interface HarddriveService {

    /**
     * Save a harddrive.
     *
     * @param harddriveDTO the entity to save.
     * @return the persisted entity.
     */
    HarddriveDTO save(HarddriveDTO harddriveDTO);

    /**
     * Get all the harddrives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HarddriveDTO> findAll(Pageable pageable);

    /**
     * Get all the harddrives with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<HarddriveDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" harddrive.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HarddriveDTO> findOne(Long id);

    /**
     * Delete the "id" harddrive.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the harddrive corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HarddriveDTO> search(String query, Pageable pageable);
}

package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.WishListDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.mycompany.myapp.domain.WishList}.
 */
public interface WishListService {

    /**
     * Save a wishList.
     *
     * @param wishListDTO the entity to save.
     * @return the persisted entity.
     */
    WishListDTO save(WishListDTO wishListDTO);

    /**
     * Get all the wishLists.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WishListDTO> findAll(Pageable pageable);

    /**
     * Get all the wishLists with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<WishListDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" wishList.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WishListDTO> findOne(Long id);

    /**
     * Delete the "id" wishList.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the wishList corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WishListDTO> search(String query, Pageable pageable);
}

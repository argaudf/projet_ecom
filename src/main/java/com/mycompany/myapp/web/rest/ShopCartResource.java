package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.ShopCartService;
import com.mycompany.myapp.service.UserService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.service.dto.ShopCartDTO;
import com.mycompany.myapp.service.dto.ShopCartCriteria;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.repository.ShopCartRepository;
import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.service.ProductService;
import com.mycompany.myapp.service.ShopCartQueryService;

import com.mycompany.myapp.service.dto.ProductDTO;
import java.time.LocalDate;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ShopCart}.
 */
@RestController
@RequestMapping("/api")
public class ShopCartResource {

    private final Logger log = LoggerFactory.getLogger(ShopCartResource.class);

    private static final String ENTITY_NAME = "shopCart";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShopCartService shopCartService;

    private final ShopCartQueryService shopCartQueryService;

    private final UserService userService;
    public ShopCartRepository shopCartRepository;
    
    private final ProductResource productResource;
    private final ProductService productService;

    public ShopCartResource(ShopCartService shopCartService, ShopCartQueryService shopCartQueryService,
            UserService userService,ShopCartRepository shopCartRepository,ProductResource productResource,ProductService productService) {
    	this.shopCartRepository = shopCartRepository;
        this.shopCartService = shopCartService;
        this.shopCartQueryService = shopCartQueryService;
        this.userService = userService;
        this.productResource = productResource;
        this.productService = productService;
    }

    /**
     * {@code POST  /shop-carts} : Create a new shopCart.
     *
     * @param shopCartDTO the shopCartDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new shopCartDTO, or with status {@code 400 (Bad Request)} if
     *         the shopCart has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shop-carts")
    public ResponseEntity<ShopCartDTO> createShopCart(@RequestBody ShopCartDTO shopCartDTO) throws URISyntaxException {
        log.debug("REST request to save ShopCart : {}", shopCartDTO);
        if (shopCartDTO.getId() != null) {
            throw new BadRequestAlertException("A new shopCart cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopCartDTO result = shopCartService.save(shopCartDTO);
        return ResponseEntity
                .created(new URI("/api/shop-carts/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /shop-carts} : Updates an existing shopCart.
     *
     * @param shopCartDTO the shopCartDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated shopCartDTO, or with status {@code 400 (Bad Request)} if
     *         the shopCartDTO is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the shopCartDTO couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shop-carts")
    public ResponseEntity<ShopCartDTO> updateShopCart(@RequestBody ShopCartDTO shopCartDTO) throws URISyntaxException {
        log.debug("REST request to update ShopCart : {}", shopCartDTO);
        if (shopCartDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShopCartDTO result = shopCartService.save(shopCartDTO);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopCartDTO.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /shop-carts} : get all the shopCarts.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of shopCarts in body.
     */
    @GetMapping("/shop-carts")
    public ResponseEntity<List<ShopCartDTO>> getAllShopCarts(ShopCartCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ShopCarts by criteria: {}", criteria);
        Page<ShopCartDTO> page = shopCartQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shop-carts/count} : count all the shopCarts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/shop-carts/count")
    public ResponseEntity<Long> countShopCarts(ShopCartCriteria criteria) {
        log.debug("REST request to count ShopCarts by criteria: {}", criteria);
        return ResponseEntity.ok().body(shopCartQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shop-carts/:id} : get the "id" shopCart.
     *
     * @param id the id of the shopCartDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the shopCartDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shop-carts/{id}")
    public ResponseEntity<ShopCartDTO> getShopCart(@PathVariable Long id) {
        log.debug("REST request to get ShopCart : {}", id);
        Optional<ShopCartDTO> shopCartDTO = shopCartService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shopCartDTO);
    }
    
    
    
    
    
    
    

    /**
     * {@code DELETE  /shop-carts/:id} : delete the "id" shopCart.
     *
     * @param id the id of the shopCartDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shop-carts/{id}")
    public ResponseEntity<Void> deleteShopCart(@PathVariable Long id) {
        log.debug("REST request to delete ShopCart : {}", id);
        shopCartService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /* delete shopcart and update stock*/
    @DeleteMapping("/shop-carts/delete/{id}")
    public ResponseEntity<Void> deleteShopCartAndUpdateStock(@PathVariable Long id) {
        log.debug("REST request to delete ShopCart : {}", id);
        ShopCartDTO shopCartDTO = shopCartService.findOne(id).get();

        ProductDTO productDTO = productService.findOne(shopCartDTO.getProductId()).get();
        productDTO.setStock(productDTO.getStock()+shopCartDTO.getQuantity());
        productService.save(productDTO);

        shopCartService.delete(id);

        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code SEARCH  /_search/shop-carts?query=:query} : search for the shopCart
     * corresponding to the query.
     *
     * @param query    the query of the shopCart search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/shop-carts")
    public ResponseEntity<List<ShopCartDTO>> searchShopCarts(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ShopCarts for query {}", query);
        Page<ShopCartDTO> page = shopCartService.search(query, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /*
     * Add a new product to the shopcart's user
     */
	
	
    @PutMapping("/shop-carts/update/{login}/{id}") 
    @Transactional(rollbackFor = Throwable.class)
    public ResponseEntity<ShopCartDTO>
        addToShopCart2(@PathVariable String login, @PathVariable String id) throws URISyntaxException {
        //log.debug("REST request to add Product to ShopCart : {}", shopCartDTO);

        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        if(!(userLogin.isPresent() && userLogin.get().equals(login))) { throw new
        BadRequestAlertException("User not connected", ENTITY_NAME, "notconnected");
        }

        Optional<ShopCartDTO> shopCartDTO_o = shopCartService.findOne(Long.valueOf(id));
        ShopCartDTO shopCartDTO = shopCartDTO_o.get();
        //TODO: verif user
        /*
        if(!(shopCartDTO.isPresent() && shopCartDTO.get().getUserId()==)) { throw new
        BadRequestAlertException("User not connected", ENTITY_NAME, "notconnected");
        }*/

        shopCartDTO.setQuantity(shopCartDTO.getQuantity()+1);
        ShopCartDTO result2 = shopCartService.save(shopCartDTO);

        ProductDTO productDTO = productService.findOne(shopCartDTO.getProductId()).get();
        if(productDTO.getStock()<=0){
            throw new BadRequestAlertException("No Stock", ENTITY_NAME, "nostock");
        }
        productDTO.setStock(productDTO.getStock()-1);
        productService.save(productDTO);

        return ResponseEntity
                .created(new URI("/api/shop-carts/update/" + login + "/" +
                        result2.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME,
                        result2.getId().toString())) .body(result2); 
    }


    @PostMapping("/shop-carts/{login}")
        @Transactional(rollbackFor = Throwable.class)
        public ResponseEntity<ShopCartDTO> addToShopCart(@PathVariable String login, @RequestBody ProductDTO productDTO)
        throws URISyntaxException {
        log.debug("REST request to add Product to ShopCart : {}", productDTO);

        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin(); if
        (!(userLogin.isPresent() && userLogin.get().equals(login))) { throw new
        BadRequestAlertException("User not connected", ENTITY_NAME, "notconnected");
        }

        Optional<User> user = userService.getUserWithAuthoritiesByLogin(login);

        ShopCartDTO shopCartDTO = new ShopCartDTO(); 
        shopCartDTO.setQuantity(1);
        shopCartDTO.setAddTime(LocalDate.now());
        shopCartDTO.setUserId(user.get().getId());
        shopCartDTO.setProductId(productDTO.getId());
        ShopCartDTO result = shopCartService.save(shopCartDTO); 
        
        if(productDTO.getStock()<=0){
            throw new BadRequestAlertException("No Stock", ENTITY_NAME, "nostock");
        }
        productDTO.setStock(productDTO.getStock()-1);
        productService.save(productDTO);

        return null;
	  
	 
	} 
}

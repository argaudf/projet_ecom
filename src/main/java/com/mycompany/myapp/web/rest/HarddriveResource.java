package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.HarddriveService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.service.dto.HarddriveDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Harddrive}.
 */
@RestController
@RequestMapping("/api")
public class HarddriveResource {

    private final Logger log = LoggerFactory.getLogger(HarddriveResource.class);

    private static final String ENTITY_NAME = "harddrive";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HarddriveService harddriveService;

    public HarddriveResource(HarddriveService harddriveService) {
        this.harddriveService = harddriveService;
    }

    /**
     * {@code POST  /harddrives} : Create a new harddrive.
     *
     * @param harddriveDTO the harddriveDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new harddriveDTO, or with status {@code 400 (Bad Request)} if the harddrive has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/harddrives")
    public ResponseEntity<HarddriveDTO> createHarddrive(@RequestBody HarddriveDTO harddriveDTO) throws URISyntaxException {
        log.debug("REST request to save Harddrive : {}", harddriveDTO);
        if (harddriveDTO.getId() != null) {
            throw new BadRequestAlertException("A new harddrive cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HarddriveDTO result = harddriveService.save(harddriveDTO);
        return ResponseEntity.created(new URI("/api/harddrives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /harddrives} : Updates an existing harddrive.
     *
     * @param harddriveDTO the harddriveDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated harddriveDTO,
     * or with status {@code 400 (Bad Request)} if the harddriveDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the harddriveDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/harddrives")
    public ResponseEntity<HarddriveDTO> updateHarddrive(@RequestBody HarddriveDTO harddriveDTO) throws URISyntaxException {
        log.debug("REST request to update Harddrive : {}", harddriveDTO);
        if (harddriveDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HarddriveDTO result = harddriveService.save(harddriveDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, harddriveDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /harddrives} : get all the harddrives.
     *

     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of harddrives in body.
     */
    @GetMapping("/harddrives")
    public ResponseEntity<List<HarddriveDTO>> getAllHarddrives(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Harddrives");
        Page<HarddriveDTO> page;
        if (eagerload) {
            page = harddriveService.findAllWithEagerRelationships(pageable);
        } else {
            page = harddriveService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /harddrives/:id} : get the "id" harddrive.
     *
     * @param id the id of the harddriveDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the harddriveDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/harddrives/{id}")
    public ResponseEntity<HarddriveDTO> getHarddrive(@PathVariable Long id) {
        log.debug("REST request to get Harddrive : {}", id);
        Optional<HarddriveDTO> harddriveDTO = harddriveService.findOne(id);
        return ResponseUtil.wrapOrNotFound(harddriveDTO);
    }

    /**
     * {@code DELETE  /harddrives/:id} : delete the "id" harddrive.
     *
     * @param id the id of the harddriveDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/harddrives/{id}")
    public ResponseEntity<Void> deleteHarddrive(@PathVariable Long id) {
        log.debug("REST request to delete Harddrive : {}", id);
        harddriveService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/harddrives?query=:query} : search for the harddrive corresponding
     * to the query.
     *
     * @param query the query of the harddrive search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/harddrives")
    public ResponseEntity<List<HarddriveDTO>> searchHarddrives(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Harddrives for query {}", query);
        Page<HarddriveDTO> page = harddriveService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}

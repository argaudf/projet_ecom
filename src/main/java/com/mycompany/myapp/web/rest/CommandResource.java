package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.CommandRepository;
import com.mycompany.myapp.repository.ShopCartRepository;
import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.service.AddressService;
import com.mycompany.myapp.service.CommandProductService;
import com.mycompany.myapp.service.CommandQueryService;
import com.mycompany.myapp.service.CommandService;
import com.mycompany.myapp.service.ShopCartService;
import com.mycompany.myapp.service.UserService;
import com.mycompany.myapp.service.dto.AddressDTO;
import com.mycompany.myapp.service.dto.CommandCriteria;
import com.mycompany.myapp.service.dto.CommandDTO;
import com.mycompany.myapp.service.dto.CommandProductDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Command}.
 */
@RestController
@RequestMapping("/api")
public class CommandResource {

	private final Logger log = LoggerFactory.getLogger(CommandResource.class);

	private static final String ENTITY_NAME = "command";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final CommandService commandService;

	private final CommandQueryService commandQueryService;

	private final UserService userService;
	private final ShopCartService shopCartService;
	private final CommandProductService commandProductService;
	private final AddressService addressService;
	public ShopCartRepository shopCartRepository;
	public CommandRepository commandRepository;

	public CommandResource(CommandService commandService, CommandQueryService commandQueryService,
			UserService userService, ShopCartService shopCartService, CommandProductService commandProductService,
			AddressService addressService, ShopCartRepository shopCartRepository, CommandRepository commandRepository) {
		this.commandService = commandService;
		this.commandQueryService = commandQueryService;
		this.commandRepository = commandRepository;
		this.shopCartRepository = shopCartRepository;
		this.addressService = addressService;
		this.commandProductService = commandProductService;
		this.shopCartService = shopCartService;
		this.userService = userService;
	}

	/**
	 * {@code POST  /commands} : Create a new command.
	 *
	 * @param commandDTO the commandDTO to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new commandDTO, or with status {@code 400 (Bad Request)} if
	 *         the command has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/commands")
	public ResponseEntity<CommandDTO> createCommand(@RequestBody CommandDTO commandDTO) throws URISyntaxException {
		log.debug("REST request to save Command : {}", commandDTO);
		if (commandDTO.getId() != null) {
			throw new BadRequestAlertException("A new command cannot already have an ID", ENTITY_NAME, "idexists");
		}
		CommandDTO result = commandService.save(commandDTO);
		return ResponseEntity
				.created(new URI("/api/commands/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
	 * {@code PUT  /commands} : Updates an existing command.
	 *
	 * @param commandDTO the commandDTO to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated commandDTO, or with status {@code 400 (Bad Request)} if
	 *         the commandDTO is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the commandDTO couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/commands")
	public ResponseEntity<CommandDTO> updateCommand(@RequestBody CommandDTO commandDTO) throws URISyntaxException {
		log.debug("REST request to update Command : {}", commandDTO);
		if (commandDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		CommandDTO result = commandService.save(commandDTO);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, commandDTO.getId().toString()))
				.body(result);
	}

	/**
	 * {@code GET  /commands} : get all the commands.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of commands in body.
	 */
	@GetMapping("/commands")
	public ResponseEntity<List<CommandDTO>> getAllCommands(CommandCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Commands by criteria: {}", criteria);
		Page<CommandDTO> page = commandQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * {@code GET  /commands/count} : count all the commands.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/commands/count")
	public ResponseEntity<Long> countCommands(CommandCriteria criteria) {
		log.debug("REST request to count Commands by criteria: {}", criteria);
		return ResponseEntity.ok().body(commandQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /commands/:id} : get the "id" command.
	 *
	 * @param id the id of the commandDTO to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the commandDTO, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/commands/{id}")
	public ResponseEntity<CommandDTO> getCommand(@PathVariable Long id) {
		log.debug("REST request to get Command : {}", id);
		Optional<CommandDTO> commandDTO = commandService.findOne(id);
		return ResponseUtil.wrapOrNotFound(commandDTO);
	}

	/**
	 * {@code DELETE  /commands/:id} : delete the "id" command.
	 *
	 * @param id the id of the commandDTO to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/commands/{id}")
	public ResponseEntity<Void> deleteCommand(@PathVariable Long id) {
		log.debug("REST request to delete Command : {}", id);
		commandService.delete(id);
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
				.build();
	}

	/**
	 * {@code SEARCH  /_search/commands?query=:query} : search for the command
	 * corresponding to the query.
	 *
	 * @param query    the query of the command search.
	 * @param pageable the pagination information.
	 * @return the result of the search.
	 */
	@GetMapping("/_search/commands")
	public ResponseEntity<List<CommandDTO>> searchCommands(@RequestParam String query, Pageable pageable) {
		log.debug("REST request to search for a page of Commands for query {}", query);
		Page<CommandDTO> page = commandService.search(query, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	@PostMapping("/commands/{login}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<CommandDTO> createCommand(@PathVariable String login, @RequestBody AddressDTO address)
			throws URISyntaxException {
		log.debug("REST request to save Command : {}");
		Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
		if (!(userLogin.isPresent() && userLogin.get().equals(login))) {
			throw new BadRequestAlertException("User not connected", ENTITY_NAME, "notconnected");
		}
		Integer newStatusInteger = 0;
		Optional<User> user = userService.getUserWithAuthoritiesByLogin(login);
		List<ShopCart> scl = shopCartRepository.findAllByuid(user.get().getId());

		CommandDTO commandDTO = new CommandDTO();
		// shopCart.getQuantity();
		commandDTO.setStatus(newStatusInteger);
		commandDTO.setUserId(user.get().getId());
		commandDTO.setAddressId(address.getId());
		CommandDTO result1 = commandService.save(commandDTO);

		for (ShopCart shopCart : scl) {
			CommandProductDTO commandProductDTO = new CommandProductDTO();
			commandProductDTO.setCommandId(result1.getId());
			commandProductDTO.setProductId(shopCart.getProduct().getId());
			commandProductDTO.setQuantity(shopCart.getQuantity());
			commandProductDTO.setAddTime(LocalDate.now());
			commandProductService.save(commandProductDTO);

			shopCartService.delete(shopCart.getId());

		}

		return null;

	}

}

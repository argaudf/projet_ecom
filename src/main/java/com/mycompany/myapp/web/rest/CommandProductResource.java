package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.CommandProductService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.service.dto.CommandProductDTO;
import com.mycompany.myapp.service.dto.CommandProductCriteria;
import com.mycompany.myapp.service.CommandProductQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.CommandProduct}.
 */
@RestController
@RequestMapping("/api")
public class CommandProductResource {

    private final Logger log = LoggerFactory.getLogger(CommandProductResource.class);

    private static final String ENTITY_NAME = "commandProduct";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommandProductService commandProductService;

    private final CommandProductQueryService commandProductQueryService;

    public CommandProductResource(CommandProductService commandProductService, CommandProductQueryService commandProductQueryService) {
        this.commandProductService = commandProductService;
        this.commandProductQueryService = commandProductQueryService;
    }

    /**
     * {@code POST  /command-products} : Create a new commandProduct.
     *
     * @param commandProductDTO the commandProductDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commandProductDTO, or with status {@code 400 (Bad Request)} if the commandProduct has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/command-products")
    public ResponseEntity<CommandProductDTO> createCommandProduct(@RequestBody CommandProductDTO commandProductDTO) throws URISyntaxException {
        log.debug("REST request to save CommandProduct : {}", commandProductDTO);
        if (commandProductDTO.getId() != null) {
            throw new BadRequestAlertException("A new commandProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommandProductDTO result = commandProductService.save(commandProductDTO);
        return ResponseEntity.created(new URI("/api/command-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /command-products} : Updates an existing commandProduct.
     *
     * @param commandProductDTO the commandProductDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commandProductDTO,
     * or with status {@code 400 (Bad Request)} if the commandProductDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commandProductDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/command-products")
    public ResponseEntity<CommandProductDTO> updateCommandProduct(@RequestBody CommandProductDTO commandProductDTO) throws URISyntaxException {
        log.debug("REST request to update CommandProduct : {}", commandProductDTO);
        if (commandProductDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CommandProductDTO result = commandProductService.save(commandProductDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, commandProductDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /command-products} : get all the commandProducts.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commandProducts in body.
     */
    @GetMapping("/command-products")
    public ResponseEntity<List<CommandProductDTO>> getAllCommandProducts(CommandProductCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CommandProducts by criteria: {}", criteria);
        Page<CommandProductDTO> page = commandProductQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /command-products/count} : count all the commandProducts.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/command-products/count")
    public ResponseEntity<Long> countCommandProducts(CommandProductCriteria criteria) {
        log.debug("REST request to count CommandProducts by criteria: {}", criteria);
        return ResponseEntity.ok().body(commandProductQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /command-products/:id} : get the "id" commandProduct.
     *
     * @param id the id of the commandProductDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commandProductDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/command-products/{id}")
    public ResponseEntity<CommandProductDTO> getCommandProduct(@PathVariable Long id) {
        log.debug("REST request to get CommandProduct : {}", id);
        Optional<CommandProductDTO> commandProductDTO = commandProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commandProductDTO);
    }

    /**
     * {@code DELETE  /command-products/:id} : delete the "id" commandProduct.
     *
     * @param id the id of the commandProductDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/command-products/{id}")
    public ResponseEntity<Void> deleteCommandProduct(@PathVariable Long id) {
        log.debug("REST request to delete CommandProduct : {}", id);
        commandProductService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/command-products?query=:query} : search for the commandProduct corresponding
     * to the query.
     *
     * @param query the query of the commandProduct search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/command-products")
    public ResponseEntity<List<CommandProductDTO>> searchCommandProducts(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CommandProducts for query {}", query);
        Page<CommandProductDTO> page = commandProductService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}

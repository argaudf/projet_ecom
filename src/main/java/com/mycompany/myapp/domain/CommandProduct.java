package com.mycompany.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A CommandProduct.
 */
@Entity
@Table(name = "command_product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "commandproduct")
public class CommandProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "add_time")
    private LocalDate addTime;

    @ManyToOne
    @JsonIgnoreProperties("commandEntries")
    private Product product;

    @ManyToOne
    @JsonIgnoreProperties("entries")
    private Command command;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public CommandProduct quantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public LocalDate getAddTime() {
        return addTime;
    }

    public CommandProduct addTime(LocalDate addTime) {
        this.addTime = addTime;
        return this;
    }

    public void setAddTime(LocalDate addTime) {
        this.addTime = addTime;
    }

    public Product getProduct() {
        return product;
    }

    public CommandProduct product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Command getCommand() {
        return command;
    }

    public CommandProduct command(Command command) {
        this.command = command;
        return this;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommandProduct)) {
            return false;
        }
        return id != null && id.equals(((CommandProduct) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CommandProduct{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            ", addTime='" + getAddTime() + "'" +
            "}";
    }
}

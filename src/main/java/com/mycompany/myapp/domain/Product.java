package com.mycompany.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "pro_name")
    private String proName;

    @Column(name = "pro_price")
    private Double proPrice;

    @Column(name = "stock")
    private Integer stock;

    @Column(name = "description")
    private String description;

    @Column(name = "picture")
    private String picture;

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Brand brand;

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CommandProduct> commandEntries = new HashSet<>();

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShopCart> shopcarts = new HashSet<>();

    @ManyToMany(mappedBy = "products")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<WishList> wishlists = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Cpu cpu;

    @ManyToMany(mappedBy = "products")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Harddrive> hds = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProName() {
        return proName;
    }

    public Product proName(String proName) {
        this.proName = proName;
        return this;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public Double getProPrice() {
        return proPrice;
    }

    public Product proPrice(Double proPrice) {
        this.proPrice = proPrice;
        return this;
    }

    public void setProPrice(Double proPrice) {
        this.proPrice = proPrice;
    }

    public Integer getStock() {
        return stock;
    }

    public Product stock(Integer stock) {
        this.stock = stock;
        return this;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public Product description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public Product picture(String picture) {
        this.picture = picture;
        return this;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Brand getBrand() {
        return brand;
    }

    public Product brand(Brand brand) {
        this.brand = brand;
        return this;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Set<CommandProduct> getCommandEntries() {
        return commandEntries;
    }

    public Product commandEntries(Set<CommandProduct> commandProducts) {
        this.commandEntries = commandProducts;
        return this;
    }

    public Product addCommandEntry(CommandProduct commandProduct) {
        this.commandEntries.add(commandProduct);
        commandProduct.setProduct(this);
        return this;
    }

    public Product removeCommandEntry(CommandProduct commandProduct) {
        this.commandEntries.remove(commandProduct);
        commandProduct.setProduct(null);
        return this;
    }

    public void setCommandEntries(Set<CommandProduct> commandProducts) {
        this.commandEntries = commandProducts;
    }

    public Set<ShopCart> getShopcarts() {
        return shopcarts;
    }

    public Product shopcarts(Set<ShopCart> shopCarts) {
        this.shopcarts = shopCarts;
        return this;
    }

    public Product addShopcart(ShopCart shopCart) {
        this.shopcarts.add(shopCart);
        shopCart.setProduct(this);
        return this;
    }

    public Product removeShopcart(ShopCart shopCart) {
        this.shopcarts.remove(shopCart);
        shopCart.setProduct(null);
        return this;
    }

    public void setShopcarts(Set<ShopCart> shopCarts) {
        this.shopcarts = shopCarts;
    }

    public Set<WishList> getWishlists() {
        return wishlists;
    }

    public Product wishlists(Set<WishList> wishLists) {
        this.wishlists = wishLists;
        return this;
    }

    public Product addWishlist(WishList wishList) {
        this.wishlists.add(wishList);
        wishList.getProducts().add(this);
        return this;
    }

    public Product removeWishlist(WishList wishList) {
        this.wishlists.remove(wishList);
        wishList.getProducts().remove(this);
        return this;
    }

    public void setWishlists(Set<WishList> wishLists) {
        this.wishlists = wishLists;
    }

    public Cpu getCpu() {
        return cpu;
    }

    public Product cpu(Cpu cpu) {
        this.cpu = cpu;
        return this;
    }

    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    public Set<Harddrive> getHds() {
        return hds;
    }

    public Product hds(Set<Harddrive> harddrives) {
        this.hds = harddrives;
        return this;
    }

    public Product addHd(Harddrive harddrive) {
        this.hds.add(harddrive);
        harddrive.getProducts().add(this);
        return this;
    }

    public Product removeHd(Harddrive harddrive) {
        this.hds.remove(harddrive);
        harddrive.getProducts().remove(this);
        return this;
    }

    public void setHds(Set<Harddrive> harddrives) {
        this.hds = harddrives;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return id != null && id.equals(((Product) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", proName='" + getProName() + "'" +
            ", proPrice=" + getProPrice() +
            ", stock=" + getStock() +
            ", description='" + getDescription() + "'" +
            ", picture='" + getPicture() + "'" +
            "}";
    }
}

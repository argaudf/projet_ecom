package com.mycompany.myapp.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Cpu.
 */
@Entity
@Table(name = "cpu")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "cpu")
public class Cpu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "frequency")
    private Double frequency;

    @OneToMany(mappedBy = "cpu")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Product> products = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Cpu name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getFrequency() {
        return frequency;
    }

    public Cpu frequency(Double frequency) {
        this.frequency = frequency;
        return this;
    }

    public void setFrequency(Double frequency) {
        this.frequency = frequency;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public Cpu products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public Cpu addProduct(Product product) {
        this.products.add(product);
        product.setCpu(this);
        return this;
    }

    public Cpu removeProduct(Product product) {
        this.products.remove(product);
        product.setCpu(null);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cpu)) {
            return false;
        }
        return id != null && id.equals(((Cpu) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cpu{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", frequency=" + getFrequency() +
            "}";
    }
}

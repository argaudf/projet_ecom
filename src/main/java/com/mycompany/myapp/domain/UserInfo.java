package com.mycompany.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A UserInfo.
 */
@Entity
@Table(name = "user_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "userinfo")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "user_status")
    private String userStatus;

    @Column(name = "tel")
    private String tel;

    @Column(name = "email")
    private String email;

    @Column(name = "password_q")
    private String passwordQ;

    @Column(name = "password_a")
    private String passwordA;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Command> commands = new HashSet<>();

    @OneToMany(mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShopCart> shopcarts = new HashSet<>();

    @OneToMany(mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Address> addresses = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public UserInfo userStatus(String userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getTel() {
        return tel;
    }

    public UserInfo tel(String tel) {
        this.tel = tel;
        return this;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public UserInfo email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordQ() {
        return passwordQ;
    }

    public UserInfo passwordQ(String passwordQ) {
        this.passwordQ = passwordQ;
        return this;
    }

    public void setPasswordQ(String passwordQ) {
        this.passwordQ = passwordQ;
    }

    public String getPasswordA() {
        return passwordA;
    }

    public UserInfo passwordA(String passwordA) {
        this.passwordA = passwordA;
        return this;
    }

    public void setPasswordA(String passwordA) {
        this.passwordA = passwordA;
    }

    public User getUser() {
        return user;
    }

    public UserInfo user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Command> getCommands() {
        return commands;
    }

    public UserInfo commands(Set<Command> commands) {
        this.commands = commands;
        return this;
    }

    public UserInfo addCommands(Command command) {
        this.commands.add(command);
        command.setUser(this);
        return this;
    }

    public UserInfo removeCommands(Command command) {
        this.commands.remove(command);
        command.setUser(null);
        return this;
    }

    public void setCommands(Set<Command> commands) {
        this.commands = commands;
    }

    public Set<ShopCart> getShopcarts() {
        return shopcarts;
    }

    public UserInfo shopcarts(Set<ShopCart> shopCarts) {
        this.shopcarts = shopCarts;
        return this;
    }

    public UserInfo addShopcart(ShopCart shopCart) {
        this.shopcarts.add(shopCart);
        shopCart.setUser(this);
        return this;
    }

    public UserInfo removeShopcart(ShopCart shopCart) {
        this.shopcarts.remove(shopCart);
        shopCart.setUser(null);
        return this;
    }

    public void setShopcarts(Set<ShopCart> shopCarts) {
        this.shopcarts = shopCarts;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public UserInfo addresses(Set<Address> addresses) {
        this.addresses = addresses;
        return this;
    }

    public UserInfo addAddress(Address address) {
        this.addresses.add(address);
        address.setUser(this);
        return this;
    }

    public UserInfo removeAddress(Address address) {
        this.addresses.remove(address);
        address.setUser(null);
        return this;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserInfo)) {
            return false;
        }
        return id != null && id.equals(((UserInfo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
            "id=" + getId() +
            ", userStatus='" + getUserStatus() + "'" +
            ", tel='" + getTel() + "'" +
            ", email='" + getEmail() + "'" +
            ", passwordQ='" + getPasswordQ() + "'" +
            ", passwordA='" + getPasswordA() + "'" +
            "}";
    }
}

package com.mycompany.myapp.domain;

import javax.persistence.Column;

import org.springframework.security.core.Transient;
@Transient
public class ResultProduct {
	
	
	private String proName;
	
	
	private Integer stock;
	
	
	public ResultProduct() {
		
	}
	public ResultProduct(String proName, Integer stock) {
		super();
		this.proName = proName;
		this.stock = stock;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	
	@Override
	public String toString() {
		
		return "ResultProducts"+
				"productName"+getProName()+
				"productStock"+getStock();
	}
}


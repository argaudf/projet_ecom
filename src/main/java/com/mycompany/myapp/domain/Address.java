package com.mycompany.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Address.
 */
@Entity
@Table(name = "address")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "address")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "address_complete")
    private String addressComplete;

    @Column(name = "receive_name")
    private String receiveName;

    @Column(name = "receive_tel")
    private String receiveTel;

    @ManyToOne
    @JsonIgnoreProperties("addresses")
    private UserInfo user;

    @OneToMany(mappedBy = "address")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Command> commands = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressComplete() {
        return addressComplete;
    }

    public Address addressComplete(String addressComplete) {
        this.addressComplete = addressComplete;
        return this;
    }

    public void setAddressComplete(String addressComplete) {
        this.addressComplete = addressComplete;
    }

    public String getReceiveName() {
        return receiveName;
    }

    public Address receiveName(String receiveName) {
        this.receiveName = receiveName;
        return this;
    }

    public void setReceiveName(String receiveName) {
        this.receiveName = receiveName;
    }

    public String getReceiveTel() {
        return receiveTel;
    }

    public Address receiveTel(String receiveTel) {
        this.receiveTel = receiveTel;
        return this;
    }

    public void setReceiveTel(String receiveTel) {
        this.receiveTel = receiveTel;
    }

    public UserInfo getUser() {
        return user;
    }

    public Address user(UserInfo userInfo) {
        this.user = userInfo;
        return this;
    }

    public void setUser(UserInfo userInfo) {
        this.user = userInfo;
    }

    public Set<Command> getCommands() {
        return commands;
    }

    public Address commands(Set<Command> commands) {
        this.commands = commands;
        return this;
    }

    public Address addCommand(Command command) {
        this.commands.add(command);
        command.setAddress(this);
        return this;
    }

    public Address removeCommand(Command command) {
        this.commands.remove(command);
        command.setAddress(null);
        return this;
    }

    public void setCommands(Set<Command> commands) {
        this.commands = commands;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Address)) {
            return false;
        }
        return id != null && id.equals(((Address) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Address{" +
            "id=" + getId() +
            ", addressComplete='" + getAddressComplete() + "'" +
            ", receiveName='" + getReceiveName() + "'" +
            ", receiveTel='" + getReceiveTel() + "'" +
            "}";
    }
}

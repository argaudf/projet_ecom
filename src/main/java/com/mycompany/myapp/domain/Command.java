package com.mycompany.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Command.
 */
@Entity
@Table(name = "command")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "command")
public class Command implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "status")
    private Integer status;

    @OneToMany(mappedBy = "command")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CommandProduct> entries = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("commands")
    private UserInfo user;

    @ManyToOne
    @JsonIgnoreProperties("commands")
    private Address address;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public Command status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Set<CommandProduct> getEntries() {
        return entries;
    }

    public Command entries(Set<CommandProduct> commandProducts) {
        this.entries = commandProducts;
        return this;
    }

    public Command addEntry(CommandProduct commandProduct) {
        this.entries.add(commandProduct);
        commandProduct.setCommand(this);
        return this;
    }

    public Command removeEntry(CommandProduct commandProduct) {
        this.entries.remove(commandProduct);
        commandProduct.setCommand(null);
        return this;
    }

    public void setEntries(Set<CommandProduct> commandProducts) {
        this.entries = commandProducts;
    }

    public UserInfo getUser() {
        return user;
    }

    public Command user(UserInfo userInfo) {
        this.user = userInfo;
        return this;
    }

    public void setUser(UserInfo userInfo) {
        this.user = userInfo;
    }

    public Address getAddress() {
        return address;
    }

    public Command address(Address address) {
        this.address = address;
        return this;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Command)) {
            return false;
        }
        return id != null && id.equals(((Command) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Command{" +
            "id=" + getId() +
            ", status=" + getStatus() +
            "}";
    }
}

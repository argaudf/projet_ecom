package com.mycompany.myapp.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.checkerframework.checker.units.qual.s;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import javax.activation.MailcapCommandMap;

import org.checkerframework.checker.units.qual.Current;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.FinalizablePhantomReference;
import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.repository.ShopCartRepository;
import com.mycompany.myapp.repository.UserInfoRepository;
import com.mycompany.myapp.service.dto.ShopCartDTO;

import liquibase.structure.core.Data;

@RestController
@RequestMapping("/cart")
public class PanierController {

	@Autowired
	private ShopCartRepository shopCartRepository;
	
	
	 @Autowired private UserInfoRepository uIR;
	
	
	/**
	 * Affichier les produit dans panier
	 * @return
	 */
	@GetMapping(value = "/panier")
	public List<ShopCart> getShopCarts(){
		return shopCartRepository.findAll();
	}
	
	/**
	 * Find by id
	 * @param addTime
	 * @return
	 */
	
	 @GetMapping(value = "/{id}")
	 public Optional<ShopCart> getCarts(@PathVariable("id") Long id){
		return shopCartRepository.findById(id);
		 
	 }
	 /**
	  * Delete by id
	  * @param id
	  */
	 @GetMapping(value = "/delete/{id}")
	 public void deleteCartById(@PathVariable("id") Long id) {
		shopCartRepository.deleteById(id);
		
	 }
	
	 /**
	  * DeleteAll
	  */
	 @GetMapping(value = "/delete/all")
	 public void deleteAll() {
		 shopCartRepository.deleteAll();
	 }
	 
	 /**
	  * Insert
	  * @param shopCart
	  * @return
	  */
	 @PostMapping(value = "/save")
		public ShopCart InsertShopCart(@RequestBody ShopCart shopCart) {
			return shopCartRepository.save(shopCart);
		}
	 
	 /**
	  * Find by userid
	  * @param id
	  * @return details de userInfo, shopCart et Product
	  */
	 
	 @Transactional
		@RequestMapping("/updatequantity/{id}")
		@ResponseBody
		public void updateQuantity(@PathVariable("id") Long id) {
			shopCartRepository.updateStock(id);
			
			
			
		}

	 @GetMapping(value = "/pro/{id}") 
	 public Optional<ShopCart> findBypro(@PathVariable("id") Long id){ 
		 return shopCartRepository.findByPro(id); }
	 @GetMapping(value = "/shopu/{id}") 
	 public Optional<ShopCart> findOneByuid(@PathVariable("id") Long id){ 
		 return shopCartRepository.findOneByuid(id); }
	 @GetMapping(value = "/shopu2/{id}") 
	 public List<ShopCart> findAllByuid(@PathVariable("id") Long id){ 
		 return shopCartRepository.findAllByuid(id); }
	/**
	 * vide panier
	 * @param id of user
	 */
	 @GetMapping(value= "/deleteprobyUid/{id}")
	 public void deleteProByUserId(@PathVariable("id") Long id) {
		
		 Long sid = shopCartRepository.findOneByuid(id).get().getId();
		 deleteCartById(sid);
		 
		 
	 }
	 

	/*
	 * @PostMapping(value = "/testt/{id,quantity}") public List<ShopCart>
	 * addShopCart(@PathVariable("id") Long id,@PathVariable("quantity") Integer
	 * quantity) { return shopCartRepository.findAll();
	 * shopCartRepository.save(entity); }
	 */
	 @RequestMapping("/mytest")
	 public ShopCart save(@RequestBody ShopCart shopCart) {
		 ShopCart sCart = new ShopCart();
		 sCart.setQuantity(shopCart.getQuantity());
		 sCart.setAddTime(shopCart.getAddTime());
		 sCart.setUser(shopCart.getUser());
		 sCart.setProduct(shopCart.getProduct());
		//System.out.println(shopCartRepository.findAll());
		 
		 return shopCartRepository.save(sCart);
		 
	 }
	 @RequestMapping(value = "/add",method=RequestMethod.POST)
	 public ShopCart ins(@RequestBody Map<String, Object> parameters) {
		 ShopCart shopCart = new ShopCart();
		 shopCart.setQuantity((Integer)parameters.get("quantity"));
		 shopCart.setAddTime(LocalDate.parse(parameters.get("addTime").toString()));
		//shopCart.setUser((UserInfo)parameters.get("user")); 
		Map<String, Object> parameters3 = (Map<String, Object>) parameters.get("user");
		Long long1 =Long.valueOf(parameters3.get("id").toString());
		shopCart.setUser(uIR.findOnBy(long1));
		//uIR.findOnBy(long1);
		// uIR.findOnBy(id);
		 //shopCart.setUser(userInfo);
		// shopCart.setAddTime(LocalDate.parse((CharSequence) parameters.get("addTime")));
		 //shopCart.setQuantity((Integer)parameters.get("quantity"));
		// shopCart.setUser(userInfo);
		//return shopCartRepository.save(shopCart);
		return shopCartRepository.save(shopCart); 
		 
	 }
	 @RequestMapping(value = "/add3",method=RequestMethod.POST)
	 public ShopCart ins3(@RequestParam("id") Long
			  id,@RequestParam("quantity") Integer quantity) {
		 ShopCart shopCart = new ShopCart();
		shopCart.setId(id);
		//shopCart.setAddTime(addTime);
		//shopCart.setProduct(product);
		shopCart.setQuantity(quantity);
		//shopCart.setUser(user);
		return shopCartRepository.save(shopCart); 
		 
	 }
	 @RequestMapping(value = "/add2",method=RequestMethod.POST)
	 public Object ins2(@RequestBody Map<String, Object> parameters,@RequestBody Map<String, Object> parameters2) {
		 ShopCart shopCart = new ShopCart();
		// parameters.get("id");
		// shopCart.setQuantity((Integer)parameters.get("quantity"));
		// shopCart.setAddTime(LocalDate.parse(parameters.get("addTime").toString()));
		// parameters.get("user");
		// uIR.findOnBy(id);
		// shopCart.setUser(userInfo);
		// shopCart.setAddTime(LocalDate.parse((CharSequence) parameters.get("addTime")));
		 //shopCart.setQuantity((Integer)parameters.get("quantity"));
		// shopCart.setUser(userInfo);
		//return shopCartRepository.save(shopCart);
		return parameters.get("id"); 
		 
	 }
	/*
	 * @RequestMapping(value = "/add2",method=RequestMethod.POST) public String
	 * ins2(@RequestBody Map<String, Object> parameters) { ShopCart shopCart = new
	 * ShopCart(); shopCart.setQuantity((Integer)parameters.get("quantity"));
	 * shopCart.setAddTime(LocalDate.parse(parameters.get("addTime").toString()));
	 * // shopCart.setAddTime(LocalDate.parse((CharSequence)
	 * parameters.get("addTime")));
	 * //shopCart.setQuantity((Integer)parameters.get("quantity")); //
	 * shopCart.setUser(userInfo); //return shopCartRepository.save(shopCart);
	 * return parameters.get("addTime").toString();
	 * 
	 * }
	 */
	/*
	 * @RequestMapping(value = "/sum",method=RequestMethod.POST) public int
	 * computeSum(@RequestBody Map<String, Integer> parameters) { int number1 =
	 * parameters.get("number1"); int number2 = parameters.get("number2"); return
	 * number1+number2; }
	 */
	/*
	 * @RequestMapping("/mytest2") public ShopCart save(@RequestBody Product
	 * product,@RequestBody UserInfo userInfo) { ShopCart shopCart = new ShopCart();
	 * // shopCart.setAddTime(addTime); // shopCart.setQuantity(quantity);
	 * shopCart.setUser(userInfo); shopCart.setProduct(product); return
	 * shopCartRepository.save(shopCart); }
	 */
	 
	/*
	 * @RequestMapping("/test3") public ShopCart insertCart() { ShopCart shopCart =
	 * new ShopCart(); shopCart.setAddTime(addTime); shopCart.setQuantity(quantity);
	 * return shopCart;
	 * 
	 * }
	 */
	
	 
}



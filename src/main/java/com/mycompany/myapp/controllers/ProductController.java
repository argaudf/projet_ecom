package com.mycompany.myapp.controllers;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.repository.ProductRepository;

@RestController
@RequestMapping("/test")
public class ProductController {
	
	@Autowired
	public ProductRepository productRepository;
	/**
	 * obtenir la liste des produits
	 */
	@GetMapping(value = "/list")
	public List<Product> productList(){
		return productRepository.findAll();
	}

	
	@GetMapping("/{id}")
	public Optional<Product> getProductInfo(@PathVariable("id") Long id) {
		
	Optional<Product> optional = productRepository.findById(id);
		return optional;
	}
	/**
	 * modifier le stock
	 * ]
	 */
	@Transactional
	@RequestMapping("/updatestock/{id}")
	@ResponseBody
	public void updateStock(@PathVariable("id") Long id) {
		productRepository.updateStock(id);
		
		
		
	}

	

}

package com.mycompany.myapp.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.Address;
import com.mycompany.myapp.domain.Command;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.repository.CommandRepository;
import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.service.dto.AddressDTO;
import com.mycompany.myapp.service.dto.CommandDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;

@RestController
@RequestMapping("/command")
public class CommandController {
	
	@Autowired
	private CommandRepository commandRepository ;
	
	@GetMapping(value = "/find/{id}")
	public Optional<Command> findCommandByuid(@PathVariable("id") Long id){
		return commandRepository.findCommandByuid(id);
	}

	 @RequestMapping("/commands")
	    public Command createCommand(@RequestBody UserInfo userInfo ) throws URISyntaxException {
	       // log.debug("REST request to save Command : {}", addressDTO);
	       // Optional<String> userOptional = SecurityUtils.getCurrentUserLogin();
		/*
		 * if (!(userOptional.isPresent()&& userOptional.get().equals(login))) { throw
		 * new BadRequestAlertException("User not connect ", ENTITY_NAME,
		 * "notconnected");
		 * 
		 * }
		 */
	       // Optional<User> user = userService.getUserWithAuthoritiesByLogin(login);
	        Command command = new Command();
	        command.setStatus(100);
	        command.setUser(userInfo);
	       // command.setAddress(address);
	       
	        
	       return commandRepository.save(command);
	    }
	 @GetMapping(value = "/delete/{id}")
	 public void deleteCartById(@PathVariable("id") Long id) {
		commandRepository.deleteById(id);
		
	 }
	 @GetMapping(value = "/findAll")
		public List<Command> getCommands(){
			return commandRepository.findAll();
		}
}

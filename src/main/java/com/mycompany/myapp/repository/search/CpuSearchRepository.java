package com.mycompany.myapp.repository.search;
import com.mycompany.myapp.domain.Cpu;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Cpu} entity.
 */
public interface CpuSearchRepository extends ElasticsearchRepository<Cpu, Long> {
}

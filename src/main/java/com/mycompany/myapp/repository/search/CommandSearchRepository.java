package com.mycompany.myapp.repository.search;
import com.mycompany.myapp.domain.Command;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Command} entity.
 */
public interface CommandSearchRepository extends ElasticsearchRepository<Command, Long> {
}

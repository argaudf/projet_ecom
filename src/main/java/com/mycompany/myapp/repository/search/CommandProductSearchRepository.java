package com.mycompany.myapp.repository.search;
import com.mycompany.myapp.domain.CommandProduct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link CommandProduct} entity.
 */
public interface CommandProductSearchRepository extends ElasticsearchRepository<CommandProduct, Long> {
}

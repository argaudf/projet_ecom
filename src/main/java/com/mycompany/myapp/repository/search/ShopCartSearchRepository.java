package com.mycompany.myapp.repository.search;
import com.mycompany.myapp.domain.ShopCart;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ShopCart} entity.
 */
public interface ShopCartSearchRepository extends ElasticsearchRepository<ShopCart, Long> {
}

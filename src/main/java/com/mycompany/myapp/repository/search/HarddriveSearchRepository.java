package com.mycompany.myapp.repository.search;
import com.mycompany.myapp.domain.Harddrive;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Harddrive} entity.
 */
public interface HarddriveSearchRepository extends ElasticsearchRepository<Harddrive, Long> {
}

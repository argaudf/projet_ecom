package com.mycompany.myapp.repository;
import com.mycompany.myapp.domain.WishList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the WishList entity.
 */
@Repository
public interface WishListRepository extends JpaRepository<WishList, Long> {

    @Query(value = "select distinct wishList from WishList wishList left join fetch wishList.products",
        countQuery = "select count(distinct wishList) from WishList wishList")
    Page<WishList> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct wishList from WishList wishList left join fetch wishList.products")
    List<WishList> findAllWithEagerRelationships();

    @Query("select wishList from WishList wishList left join fetch wishList.products where wishList.id =:id")
    Optional<WishList> findOneWithEagerRelationships(@Param("id") Long id);

}

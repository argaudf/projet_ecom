package com.mycompany.myapp.repository;
import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.domain.ShopCart;
import com.mycompany.myapp.domain.UserInfo;
import com.mycompany.myapp.service.dto.ShopCartDTO;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.annotation.ParametersAreNonnullByDefault;


/**
 * Spring Data  repository for the ShopCart entity.
 */

@Repository
public interface ShopCartRepository extends JpaRepository<ShopCart, Long>, JpaSpecificationExecutor<ShopCart> {

	
	  @Query(value =
	  "select distinct shopCart from ShopCart shopCart left join fetch shopCart.product"
	 , countQuery = "select count(distinct shopCart) from ShopCart shopCart")
	  Page<ShopCart> findAllWithEagerRelationships(Pageable pageable);
	 
	  @Query("select distinct shopCart from ShopCart shopCart left join fetch shopCart.product") 
	  List<ShopCart> findAllWithEagerRelationships();
	  
	  @Query("select shopCart from ShopCart shopCart left join fetch shopCart.product where shopCart.id =:id") 
	  Optional<ShopCart> findOneWithEagerRelationships(@Param("id") Long id);
	 
	 @Query("select shopCart from ShopCart shopCart left join fetch shopCart.product left join fetch shopCart.user where shopCart.user.id =:id") 
	 Optional<ShopCart>findOneByuid(@Param("id") Long id);
	 
	
	
	  @Query(value = "insert into ShopCart(id,quantity,addTime,user,product) values(?1,?2,?3,?4,?5) ",
	  nativeQuery=true) List<ShopCart> addShopCart(@Param("id") Long
	  id,@Param("quantity") Integer quantity,@Param("addTime") LocalDate addTime,@Param("user") UserInfo user,@Param("product") Product product );
	 
	 ShopCart save(ShopCart shopCart);
	 
	@Query("select shopCart from ShopCart shopCart left join fetch shopCart.product where shopCart.product.id =:id")
	Optional<ShopCart> findByPro(@Param("id") Long id);
	 
	 
	@Transactional
	 @Modifying(clearAutomatically = true)
	 @Query("update ShopCart set quantity = quantity+1 where id =?1  ")
	 public void updateStock(Long id);
	
	 
	 @Query("select shopCart from ShopCart shopCart left join fetch shopCart.product left join fetch shopCart.user where shopCart.user.id =:id")
	List<ShopCart> findAllByuid(@Param("id") Long id);
	 
	 
}

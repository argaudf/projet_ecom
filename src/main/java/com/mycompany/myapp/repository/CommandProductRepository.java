package com.mycompany.myapp.repository;
import com.mycompany.myapp.domain.CommandProduct;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CommandProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommandProductRepository extends JpaRepository<CommandProduct, Long>, JpaSpecificationExecutor<CommandProduct> {

}

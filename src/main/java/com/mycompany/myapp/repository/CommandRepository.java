package com.mycompany.myapp.repository;
import java.util.Optional;

import com.mycompany.myapp.domain.Command;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Command entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommandRepository extends JpaRepository<Command, Long>, JpaSpecificationExecutor<Command> {
    @Query("select command from Command command left join fetch command.user left join fetch command.entries where command.user.id =:id")
	 Optional<Command> findCommandByuid(@Param("id") Long id);

}

package com.mycompany.myapp.repository;
import com.mycompany.myapp.domain.Harddrive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Harddrive entity.
 */
@Repository
public interface HarddriveRepository extends JpaRepository<Harddrive, Long> {

    @Query(value = "select distinct harddrive from Harddrive harddrive left join fetch harddrive.products",
        countQuery = "select count(distinct harddrive) from Harddrive harddrive")
    Page<Harddrive> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct harddrive from Harddrive harddrive left join fetch harddrive.products")
    List<Harddrive> findAllWithEagerRelationships();

    @Query("select harddrive from Harddrive harddrive left join fetch harddrive.products where harddrive.id =:id")
    Optional<Harddrive> findOneWithEagerRelationships(@Param("id") Long id);

}

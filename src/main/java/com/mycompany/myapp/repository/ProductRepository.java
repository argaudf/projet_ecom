package com.mycompany.myapp.repository;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.domain.ResultProduct;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
    @Query("select new com.mycompany.myapp.domain.ResultProduct(proName,stock) from Product")
	List<ResultProduct> findProNamesAndStocks();
	/**
	 * update le stock
	 */
	
	 @Transactional
	 @Modifying(clearAutomatically = true)
	 @Query("update Product set stock = stock-1 where id =?1  ")
	 public void updateStock(Long id);
	 
	 /**
	  * trouver un produit par id
	  */
	 @Query("select p from Product p where p.id=?1")
	 public Optional<Product> findById(Long id);
	 



}

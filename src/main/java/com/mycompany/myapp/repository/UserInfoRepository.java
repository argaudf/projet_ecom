package com.mycompany.myapp.repository;
import com.mycompany.myapp.domain.UserInfo;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UserInfo entity.
 */

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Long>, JpaSpecificationExecutor<UserInfo> {

	@Query("select userInfo from UserInfo userInfo where userInfo.id =:id")
	UserInfo findOnBy(@Param("id") Long id);
	
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';

import { PasswordStrengthBarComponent } from './password/password-strength-bar.component';
import { RegisterComponent } from './register/register.component';
import { ActivateComponent } from './activate/activate.component';
import { PasswordComponent } from './password/password.component';
import { PasswordResetInitComponent } from './password-reset/init/password-reset-init.component';
import { PasswordResetFinishComponent } from './password-reset/finish/password-reset-finish.component';
import { SettingsComponent } from './settings/settings.component';
import { accountState } from './account.route';
import { AddressesViewComponent } from 'app/addresses-view/addresses-view.component';
import { AddressCardComponent } from 'app/address-card/address-card.component';
import { AddressesViewModule } from 'app/addresses-view/addresses-view.module';

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(accountState), AddressesViewModule],
  declarations: [
    ActivateComponent,
    RegisterComponent,
    PasswordComponent,
    PasswordStrengthBarComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    SettingsComponent,
    AddressesViewComponent,
    AddressCardComponent
  ]
})
export class ProjetEcomAccountModule {}

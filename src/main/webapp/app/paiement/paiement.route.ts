import { Route } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { PaiementComponent } from './paiement.component';

export const PaiementRoute: Route = {
  path: 'checkout',
  component: PaiementComponent,
  data: {
    authorities: [],
    pageTitle: 'projetEcomApp.paiement.title'
  },
  resolve: {
    pagingParams: JhiResolvePagingParams
  }
};

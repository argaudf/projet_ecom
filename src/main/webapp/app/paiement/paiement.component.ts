import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { PaiementService } from 'app/paiement.service';
import { AddressEcomService } from 'app/entities/address-ecom/address-ecom.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { IAddressEcom } from 'app/shared/model/address-ecom.model';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { Subscription, Observable } from 'rxjs';
import { User } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';

@Component({
  selector: 'jhi-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {
  editForm = this.fb.group({
    cardNumber: [],
    month: [],
    year: [],
    cryptogram: [],
    addressId: []
  });

  months: any;
  years: any;
  addresses: IAddressEcom[];
  account: Account;
  authSubscription: Subscription;
  email: any;
  aPaye = false;

  constructor(
    private fb: FormBuilder,
    private paiementService: PaiementService,
    protected addressService: AddressEcomService,
    protected jhiAlertService: JhiAlertService,
    private accountService: AccountService,
    protected usersService: UserService,
    private eventManager: JhiEventManager,
    private userInfoService: UserInfoEcomService
  ) {
    this.months = [];
    this.years = [];

    for (let x = 1; x <= 12; x++) {
      if (x >= 1 && x <= 9) {
        this.months.push({ Id: x, Name: '0' + x });
      } else {
        this.months.push({ Id: x, Name: x });
      }
    }
    for (let x = 0; x <= 10; x++) {
      const y = new Date().getFullYear();
      this.years.push({ Id: x, Name: x + y });
    }
  }

  ngOnInit() {
    this.accountService.identity().then((account: Account) => {
      this.account = account;
      this.usersService.find(this.account.login).subscribe(
        (res: HttpResponse<User>) => {
          this.onUserIdFind(res.body.id);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    });
    this.registerAuthenticationSuccess();
  }

  registerAuthenticationSuccess() {
    this.authSubscription = this.eventManager.subscribe('authenticationSuccess', message => {
      this.accountService.identity().then(account => {
        this.account = account;
      });
    });
  }

  onUserIdFind(userId: number) {
    this.userInfoService.query({ 'userId.equals': userId }).subscribe(
      (res: HttpResponse<IUserInfoEcom[]>) => {
        if (res.body.length > 0) {
          this.email = res.body[0].email;
          this.addressService.query({ 'userId.equals': res.body[0].id }).subscribe(
            (res2: HttpResponse<IAddressEcom[]>) => {
              this.addresses = res2.body;
            },
            (res2: HttpErrorResponse) => this.onError(res2.message)
          );
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  save() {
    this.subscribeToSaveResponse(this.paiementService.payer(this.account.login, this.addresses[this.editForm.get(['addressId']).value])); //TODO
  }

  previousState() {
    window.history.back();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserInfoEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.aPaye = true;
  }

  protected onSaveError() {}
}

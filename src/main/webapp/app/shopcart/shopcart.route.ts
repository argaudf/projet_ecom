import { Route } from '@angular/router';
import { ShopcartComponent } from './shopcart.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const ShopCartRoute: Route = {
  path: 'shopcart',
  component: ShopcartComponent,
  data: {
    authorities: [],
    pageTitle: 'shopcart.title'
  },
  resolve: {
    pagingParams: JhiResolvePagingParams
  }
};

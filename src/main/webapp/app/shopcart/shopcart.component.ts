import { Component, OnInit } from '@angular/core';
import { ProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';
import { JhiAlertService } from 'ng-jhipster';
import { ShopCartEcomService } from 'app/entities/shop-cart-ecom/shop-cart-ecom.service';
import { ShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.model';

/* eslint-disable no-console */

@Component({
  selector: 'jhi-shopcart',
  templateUrl: './shopcart.component.html',
  styleUrls: ['./shopcart.component.scss']
})
export class ShopcartComponent implements OnInit {
  products: { product: ProductEcom; quantity: number; shopcartId: number }[];
  accountId: number;

  constructor(
    protected productService: ProductEcomService,
    protected jhiAlertService: JhiAlertService,
    protected shopcartService: ShopCartEcomService,
    protected accountService: AccountService,
    protected usersService: UserService
  ) {
    this.products = [];
  }

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    if (this.accountService.isAuthenticated()) {
      this.accountService.identity().then((account: Account) => {
        this.usersService.find(account.login).subscribe(
          (res: HttpResponse<User>) => {
            this.accountId = res.body.id;
            this.getShopcart();
          },
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      });
    }
  }

  getShopcart() {
    this.shopcartService
      .query({
        'userId.equals': this.accountId
      })
      .subscribe(
        (res: HttpResponse<ShopCartEcom[]>) => this.requestProducts(res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  requestProducts(data: ShopCartEcom[]) {
    let i: number;
    console.log('data size : ' + data.length.toString());
    for (i = 0; i < data.length; i++) {
      console.log('data[' + i.toString() + '].productId = ' + data[i].productId.toString() + ' quantity = ' + data[i].quantity.toString());
      const amount = data[i].quantity;
      const scId = data[i].id;
      this.productService
        .find(data[i].productId)
        .subscribe(
          (res: HttpResponse<ProductEcom>) => this.products.push({ product: res.body, quantity: amount, shopcartId: scId }),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  getTotal() {
    let sum = 0;
    for (let i = 0; i < this.products.length; ++i) {
      sum += this.products[i].product.proPrice * this.products[i].quantity;
    }
    return sum;
  }

  onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  delete(i: number) {
    this.shopcartService
      .deleteAndUpdate(this.products[i].shopcartId)
      .subscribe((res: HttpResponse<any>) => this.products.splice(i, 1), (res: HttpErrorResponse) => this.onError(res.message));
  }
}

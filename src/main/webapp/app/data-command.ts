import { CommandEcom } from './shared/model/command-ecom.model';
import { ProductEcom } from './shared/model/product-ecom.model';

export class DataCommand {
  command: CommandEcom;
  content: {
    product: ProductEcom;
    quantity: number;
  }[];
}

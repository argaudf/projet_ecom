import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { ShopCartRoute } from './shopcart/shopcart.route';
import { PaiementRoute } from './paiement/paiement.route';
import { ProductViewComponent } from './product-view/product-view.component';
import { AddressesRoute } from './addresses-view/addresses-view.route';
import { CommandesRoute } from './consulter-commandes/consulter-commandes.route';

const LAYOUT_ROUTES = [navbarRoute, ShopCartRoute, PaiementRoute, AddressesRoute, CommandesRoute, ...errorRoute];
const routes: Routes = [{ path: 'product-details/:id/view', component: ProductViewComponent }];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { enableTracing: true }),
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          loadChildren: () => import('./admin/admin.module').then(m => m.ProjetEcomAdminModule)
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.ProjetEcomAccountModule)
        },
        ...LAYOUT_ROUTES
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    )
  ],
  exports: [RouterModule]
})
export class ProjetEcomAppRoutingModule {}

import { Component, OnInit, Input } from '@angular/core';
import { IProductEcom } from 'app/shared/model/product-ecom.model';
import { ActivatedRoute } from '@angular/router';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ICpuEcom } from 'app/shared/model/cpu-ecom.model';
import { CpuEcomService } from 'app/entities/cpu-ecom/cpu-ecom.service';
import { IBrandEcom } from 'app/shared/model/brand-ecom.model';
import { BrandEcomService } from 'app/entities/brand-ecom/brand-ecom.service';
import { Observable, Subscription } from 'rxjs';
import { ShopCartEcomService } from 'app/entities/shop-cart-ecom/shop-cart-ecom.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.model';
import { IShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';

@Component({
  selector: 'jhi-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {
  @Input()
  product: IProductEcom;
  productId: number;
  cpu: ICpuEcom;
  brand: IBrandEcom;
  account: Account;
  accountId: number;
  authSubscription: Subscription;
  shopcart: IShopCartEcom;

  constructor(
    protected jhiAlertService: JhiAlertService,
    private route: ActivatedRoute,
    private productService: ProductEcomService,
    private cpuEcomService: CpuEcomService,
    private brandEcomService: BrandEcomService,
    private shopcartService: ShopCartEcomService,
    private accountService: AccountService,
    private usersService: UserService,
    private eventManager: JhiEventManager
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => (this.productId = params['id']));
    this.productService.find(this.productId).subscribe((res: HttpResponse<IProductEcom>) => this.onfindproduct(res));

    if (this.accountService.isAuthenticated()) {
      this.accountService.identity().then((account: Account) => {
        this.account = account;
      });
      this.accountService.identity().then((account: Account) => {
        this.usersService.find(account.login).subscribe(
          (res: HttpResponse<User>) => {
            this.accountId = res.body.id;
          },
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      });
    }
    this.registerAuthenticationSuccess();
  }

  onfindproduct(res: HttpResponse<IProductEcom>) {
    this.product = res.body;
    if (this.product.brandId) {
      this.brandEcomService.find(this.product.brandId).subscribe((res1: HttpResponse<IBrandEcom>) => (this.brand = res1.body));
    }
    if (this.product.cpuId) {
      this.cpuEcomService.find(this.product.cpuId).subscribe((res2: HttpResponse<ICpuEcom>) => (this.cpu = res2.body));
    }
  }

  registerAuthenticationSuccess() {
    this.authSubscription = this.eventManager.subscribe('authenticationSuccess', message => {
      this.accountService.identity().then(account => {
        this.account = account;
      });
      this.accountService.identity().then((account: Account) => {
        this.usersService.find(account.login).subscribe(
          (res: HttpResponse<User>) => {
            this.accountId = res.body.id;
          },
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      });
    });
  }

  previousState() {
    window.history.back();
  }

  public addToShopCart() {
    if (this.accountService.isAuthenticated()) {
      this.shopcartService.query({ 'productId.equals': this.product.id }).subscribe(
        (res: HttpResponse<IShopCartEcom[]>) => {
          if (res.body.length > 0) {
            this.shopcart = res.body[0];
            this.shopcart.quantity = this.shopcart.quantity + 1;
            this.subscribeToSaveResponse(this.shopcartService.updateProduct(this.account.login, this.shopcart.id));
          } else {
            this.subscribeToSaveResponse(this.shopcartService.addProduct(this.account.login, this.product));
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    } else {
      window.alert("Connectez-vous avant d'ajouter un produit à votre panier");
    }
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    window.alert('Success');
  }

  protected onSaveError() {
    window.alert('Failure');
  }
}

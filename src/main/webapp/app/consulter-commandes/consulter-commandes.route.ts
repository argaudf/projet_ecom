import { Route } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { ConsulterCommandesComponent } from './consulter-commandes.component';

export const CommandesRoute: Route = {
  path: 'commandes',
  component: ConsulterCommandesComponent,
  data: {
    authorities: [],
    pageTitle: 'commandes.title'
  },
  resolve: {
    pagingParams: JhiResolvePagingParams
  }
};

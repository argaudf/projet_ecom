import { ProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';
import { JhiAlertService } from 'ng-jhipster';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.model';
import { CommandEcomService } from 'app/entities/command-ecom/command-ecom.service';
import { CommandProductEcomService } from 'app/entities/command-product-ecom/command-product-ecom.service';
import { CommandEcom } from 'app/shared/model/command-ecom.model';
import { CommandProductEcom } from 'app/shared/model/command-product-ecom.model';
import { OnInit, Component } from '@angular/core';
import { DataCommand } from 'app/data-command';
import { watchFile } from 'fs';

@Component({
  selector: 'jhi-consulter-commandes',
  templateUrl: './consulter-commandes.component.html',
  styleUrls: ['./consulter-commandes.component.scss']
})
export class ConsulterCommandesComponent implements OnInit {
  commands: DataCommand[] = [];
  accountId: number;

  constructor(
    protected productService: ProductEcomService,
    protected jhiAlertService: JhiAlertService,
    protected commandService: CommandEcomService,
    protected commandProductsService: CommandProductEcomService,
    protected accountService: AccountService,
    protected usersService: UserService
  ) {}

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    if (this.accountService.isAuthenticated()) {
      this.accountService.identity().then((account: Account) => {
        this.usersService.find(account.login).subscribe(
          (res: HttpResponse<User>) => {
            this.accountId = res.body.id;
            this.getCommands();
          },
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      });
    }
  }

  getCommands() {
    this.commandService
      .query({
        'userId.equals': this.accountId
      })
      .subscribe(
        (res: HttpResponse<CommandEcom[]>) => this.requestCommandProducts(res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  requestCommandProducts(data: CommandEcom[]) {
    let i: number;
    for (i = 0; i < data.length; i++) {
      const command = data[i];
      this.commandProductsService
        .query({
          'commandId.equals': data[i].id
        })
        .subscribe(
          (res: HttpResponse<CommandProductEcom[]>) => this.ajouterCommande(command, res.body, i),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  ajouterCommande(command: CommandEcom, data: CommandProductEcom[], i: number) {
    const obj: DataCommand = new DataCommand();
    obj.command = command;
    obj.content = [];
    this.commands.push(obj);
    this.requestProducts(data, i);
  }

  requestProducts(data: CommandProductEcom[], pos: number) {
    let i: number;
    for (i = 0; i < data.length; i++) {
      const amount = data[i].quantity;
      this.productService
        .find(data[i].productId)
        .subscribe(
          (res: HttpResponse<ProductEcom>) => this.commands[pos].content.push({ product: res.body, quantity: amount }),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  getTotal(id) {
    let sum = 0;
    for (let i = 0; i < this.commands[id].content.length; ++i) {
      sum += this.commands[id].content[i].product.proPrice * this.commands[id].content[i].quantity;
    }
    return sum;
  }

  onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}

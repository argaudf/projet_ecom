import { IProductEcom } from 'app/shared/model/product-ecom.model';

export interface IBrandEcom {
  id?: number;
  brandName?: string;
  products?: IProductEcom[];
}

export class BrandEcom implements IBrandEcom {
  constructor(public id?: number, public brandName?: string, public products?: IProductEcom[]) {}
}

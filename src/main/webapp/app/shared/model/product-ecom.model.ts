import { ICommandProductEcom } from 'app/shared/model/command-product-ecom.model';
import { IShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';
import { IWishListEcom } from 'app/shared/model/wish-list-ecom.model';
import { IHarddriveEcom } from 'app/shared/model/harddrive-ecom.model';

export interface IProductEcom {
  id?: number;
  proName?: string;
  proPrice?: number;
  stock?: number;
  description?: string;
  picture?: string;
  brandId?: number;
  commandEntries?: ICommandProductEcom[];
  shopcarts?: IShopCartEcom[];
  wishlists?: IWishListEcom[];
  cpuId?: number;
  hds?: IHarddriveEcom[];
}

export class ProductEcom implements IProductEcom {
  constructor(
    public id?: number,
    public proName?: string,
    public proPrice?: number,
    public stock?: number,
    public description?: string,
    public picture?: string,
    public brandId?: number,
    public commandEntries?: ICommandProductEcom[],
    public shopcarts?: IShopCartEcom[],
    public wishlists?: IWishListEcom[],
    public cpuId?: number,
    public hds?: IHarddriveEcom[]
  ) {}
}

import { Moment } from 'moment';

export interface IShopCartEcom {
  id?: number;
  quantity?: number;
  addTime?: Moment;
  userId?: number;
  productId?: number;
}

export class ShopCartEcom implements IShopCartEcom {
  constructor(public id?: number, public quantity?: number, public addTime?: Moment, public userId?: number, public productId?: number) {}
}

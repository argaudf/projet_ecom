import { Moment } from 'moment';

export interface ICommandProductEcom {
  id?: number;
  quantity?: number;
  addTime?: Moment;
  productId?: number;
  commandId?: number;
}

export class CommandProductEcom implements ICommandProductEcom {
  constructor(
    public id?: number,
    public quantity?: number,
    public addTime?: Moment,
    public productId?: number,
    public commandId?: number
  ) {}
}

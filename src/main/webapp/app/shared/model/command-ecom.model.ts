import { ICommandProductEcom } from 'app/shared/model/command-product-ecom.model';

export interface ICommandEcom {
  id?: number;
  status?: number;
  entries?: ICommandProductEcom[];
  userId?: number;
  addressId?: number;
}

export class CommandEcom implements ICommandEcom {
  constructor(
    public id?: number,
    public status?: number,
    public entries?: ICommandProductEcom[],
    public userId?: number,
    public addressId?: number
  ) {}
}

import { ICommandEcom } from 'app/shared/model/command-ecom.model';
import { IShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';
import { IAddressEcom } from 'app/shared/model/address-ecom.model';

export interface IUserInfoEcom {
  id?: number;
  userStatus?: string;
  tel?: string;
  email?: string;
  passwordQ?: string;
  passwordA?: string;
  userId?: number;
  commands?: ICommandEcom[];
  shopcarts?: IShopCartEcom[];
  addresses?: IAddressEcom[];
}

export class UserInfoEcom implements IUserInfoEcom {
  constructor(
    public id?: number,
    public userStatus?: string,
    public tel?: string,
    public email?: string,
    public passwordQ?: string,
    public passwordA?: string,
    public userId?: number,
    public commands?: ICommandEcom[],
    public shopcarts?: IShopCartEcom[],
    public addresses?: IAddressEcom[]
  ) {}
}

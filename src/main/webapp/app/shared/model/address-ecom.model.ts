import { ICommandEcom } from 'app/shared/model/command-ecom.model';

export interface IAddressEcom {
  id?: number;
  addressComplete?: string;
  receiveName?: string;
  receiveTel?: string;
  userId?: number;
  commands?: ICommandEcom[];
}

export class AddressEcom implements IAddressEcom {
  constructor(
    public id?: number,
    public addressComplete?: string,
    public receiveName?: string,
    public receiveTel?: string,
    public userId?: number,
    public commands?: ICommandEcom[]
  ) {}
}

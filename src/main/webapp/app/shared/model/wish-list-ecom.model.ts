import { IProductEcom } from 'app/shared/model/product-ecom.model';

export interface IWishListEcom {
  id?: number;
  userId?: number;
  products?: IProductEcom[];
}

export class WishListEcom implements IWishListEcom {
  constructor(public id?: number, public userId?: number, public products?: IProductEcom[]) {}
}

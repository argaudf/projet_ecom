import { IProductEcom } from 'app/shared/model/product-ecom.model';

export interface ICpuEcom {
  id?: number;
  name?: string;
  frequency?: number;
  products?: IProductEcom[];
}

export class CpuEcom implements ICpuEcom {
  constructor(public id?: number, public name?: string, public frequency?: number, public products?: IProductEcom[]) {}
}

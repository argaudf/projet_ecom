import { IProductEcom } from 'app/shared/model/product-ecom.model';

export interface IHarddriveEcom {
  id?: number;
  size?: number;
  products?: IProductEcom[];
}

export class HarddriveEcom implements IHarddriveEcom {
  constructor(public id?: number, public size?: number, public products?: IProductEcom[]) {}
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUserInfoEcom } from './shared/model/user-info-ecom.model';
import { SERVER_API_URL } from './app.constants';
import { map } from 'rxjs/operators';
import { IAddressEcom } from './shared/model/address-ecom.model';

type EntityResponseType = HttpResponse<IUserInfoEcom>;

@Injectable({
  providedIn: 'root'
})
export class PaiementService {
  public resourceUrl = SERVER_API_URL + 'api/commands';

  constructor(protected http: HttpClient) {}

  /*public payer(login: String, address: IAddressEcom): Observable<EntityResponseType> {
    return this.http.post<IUserInfoEcom>(`${this.resourceUrl}/${login}/${address.id}`, address, { observe: 'response' });
  }*/

  public payer(login: String, address: IAddressEcom): Observable<EntityResponseType> {
    return this.http.post<IAddressEcom>(this.resourceUrl + '/' + login, address, { observe: 'response' });
  }
}

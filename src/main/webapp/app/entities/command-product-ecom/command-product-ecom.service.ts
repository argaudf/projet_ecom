import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICommandProductEcom } from 'app/shared/model/command-product-ecom.model';

type EntityResponseType = HttpResponse<ICommandProductEcom>;
type EntityArrayResponseType = HttpResponse<ICommandProductEcom[]>;

@Injectable({ providedIn: 'root' })
export class CommandProductEcomService {
  public resourceUrl = SERVER_API_URL + 'api/command-products';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/command-products';

  constructor(protected http: HttpClient) {}

  create(commandProduct: ICommandProductEcom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commandProduct);
    return this.http
      .post<ICommandProductEcom>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(commandProduct: ICommandProductEcom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commandProduct);
    return this.http
      .put<ICommandProductEcom>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICommandProductEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICommandProductEcom[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICommandProductEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(commandProduct: ICommandProductEcom): ICommandProductEcom {
    const copy: ICommandProductEcom = Object.assign({}, commandProduct, {
      addTime: commandProduct.addTime != null && commandProduct.addTime.isValid() ? commandProduct.addTime.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.addTime = res.body.addTime != null ? moment(res.body.addTime) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((commandProduct: ICommandProductEcom) => {
        commandProduct.addTime = commandProduct.addTime != null ? moment(commandProduct.addTime) : null;
      });
    }
    return res;
  }
}

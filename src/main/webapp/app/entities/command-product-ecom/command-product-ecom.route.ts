import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CommandProductEcom } from 'app/shared/model/command-product-ecom.model';
import { CommandProductEcomService } from './command-product-ecom.service';
import { CommandProductEcomComponent } from './command-product-ecom.component';
import { CommandProductEcomDetailComponent } from './command-product-ecom-detail.component';
import { CommandProductEcomUpdateComponent } from './command-product-ecom-update.component';
import { CommandProductEcomDeletePopupComponent } from './command-product-ecom-delete-dialog.component';
import { ICommandProductEcom } from 'app/shared/model/command-product-ecom.model';

@Injectable({ providedIn: 'root' })
export class CommandProductEcomResolve implements Resolve<ICommandProductEcom> {
  constructor(private service: CommandProductEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICommandProductEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CommandProductEcom>) => response.ok),
        map((commandProduct: HttpResponse<CommandProductEcom>) => commandProduct.body)
      );
    }
    return of(new CommandProductEcom());
  }
}

export const commandProductRoute: Routes = [
  {
    path: '',
    component: CommandProductEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.commandProduct.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CommandProductEcomDetailComponent,
    resolve: {
      commandProduct: CommandProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.commandProduct.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CommandProductEcomUpdateComponent,
    resolve: {
      commandProduct: CommandProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.commandProduct.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CommandProductEcomUpdateComponent,
    resolve: {
      commandProduct: CommandProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.commandProduct.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const commandProductPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CommandProductEcomDeletePopupComponent,
    resolve: {
      commandProduct: CommandProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.commandProduct.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

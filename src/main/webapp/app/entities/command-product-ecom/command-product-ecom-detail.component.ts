import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICommandProductEcom } from 'app/shared/model/command-product-ecom.model';

@Component({
  selector: 'jhi-command-product-ecom-detail',
  templateUrl: './command-product-ecom-detail.component.html'
})
export class CommandProductEcomDetailComponent implements OnInit {
  commandProduct: ICommandProductEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ commandProduct }) => {
      this.commandProduct = commandProduct;
    });
  }

  previousState() {
    window.history.back();
  }
}

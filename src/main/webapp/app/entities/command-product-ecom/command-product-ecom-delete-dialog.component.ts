import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICommandProductEcom } from 'app/shared/model/command-product-ecom.model';
import { CommandProductEcomService } from './command-product-ecom.service';

@Component({
  selector: 'jhi-command-product-ecom-delete-dialog',
  templateUrl: './command-product-ecom-delete-dialog.component.html'
})
export class CommandProductEcomDeleteDialogComponent {
  commandProduct: ICommandProductEcom;

  constructor(
    protected commandProductService: CommandProductEcomService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.commandProductService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'commandProductListModification',
        content: 'Deleted an commandProduct'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-command-product-ecom-delete-popup',
  template: ''
})
export class CommandProductEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ commandProduct }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CommandProductEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.commandProduct = commandProduct;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/command-product-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/command-product-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

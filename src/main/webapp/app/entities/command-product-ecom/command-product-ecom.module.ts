import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { CommandProductEcomComponent } from './command-product-ecom.component';
import { CommandProductEcomDetailComponent } from './command-product-ecom-detail.component';
import { CommandProductEcomUpdateComponent } from './command-product-ecom-update.component';
import {
  CommandProductEcomDeletePopupComponent,
  CommandProductEcomDeleteDialogComponent
} from './command-product-ecom-delete-dialog.component';
import { commandProductRoute, commandProductPopupRoute } from './command-product-ecom.route';

const ENTITY_STATES = [...commandProductRoute, ...commandProductPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CommandProductEcomComponent,
    CommandProductEcomDetailComponent,
    CommandProductEcomUpdateComponent,
    CommandProductEcomDeleteDialogComponent,
    CommandProductEcomDeletePopupComponent
  ],
  entryComponents: [CommandProductEcomDeleteDialogComponent]
})
export class ProjetEcomCommandProductEcomModule {}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ICommandProductEcom, CommandProductEcom } from 'app/shared/model/command-product-ecom.model';
import { CommandProductEcomService } from './command-product-ecom.service';
import { IProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';
import { ICommandEcom } from 'app/shared/model/command-ecom.model';
import { CommandEcomService } from 'app/entities/command-ecom/command-ecom.service';

@Component({
  selector: 'jhi-command-product-ecom-update',
  templateUrl: './command-product-ecom-update.component.html'
})
export class CommandProductEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  products: IProductEcom[];

  commands: ICommandEcom[];
  addTimeDp: any;

  editForm = this.fb.group({
    id: [],
    quantity: [],
    addTime: [],
    productId: [],
    commandId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected commandProductService: CommandProductEcomService,
    protected productService: ProductEcomService,
    protected commandService: CommandEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ commandProduct }) => {
      this.updateForm(commandProduct);
    });
    this.productService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProductEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProductEcom[]>) => response.body)
      )
      .subscribe((res: IProductEcom[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.commandService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICommandEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICommandEcom[]>) => response.body)
      )
      .subscribe((res: ICommandEcom[]) => (this.commands = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(commandProduct: ICommandProductEcom) {
    this.editForm.patchValue({
      id: commandProduct.id,
      quantity: commandProduct.quantity,
      addTime: commandProduct.addTime,
      productId: commandProduct.productId,
      commandId: commandProduct.commandId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const commandProduct = this.createFromForm();
    if (commandProduct.id !== undefined) {
      this.subscribeToSaveResponse(this.commandProductService.update(commandProduct));
    } else {
      this.subscribeToSaveResponse(this.commandProductService.create(commandProduct));
    }
  }

  private createFromForm(): ICommandProductEcom {
    return {
      ...new CommandProductEcom(),
      id: this.editForm.get(['id']).value,
      quantity: this.editForm.get(['quantity']).value,
      addTime: this.editForm.get(['addTime']).value,
      productId: this.editForm.get(['productId']).value,
      commandId: this.editForm.get(['commandId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommandProductEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProductById(index: number, item: IProductEcom) {
    return item.id;
  }

  trackCommandById(index: number, item: ICommandEcom) {
    return item.id;
  }
}

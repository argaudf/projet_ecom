import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'user-info-ecom',
        loadChildren: () => import('./user-info-ecom/user-info-ecom.module').then(m => m.ProjetEcomUserInfoEcomModule)
      },
      {
        path: 'address-ecom',
        loadChildren: () => import('./address-ecom/address-ecom.module').then(m => m.ProjetEcomAddressEcomModule)
      },
      {
        path: 'brand-ecom',
        loadChildren: () => import('./brand-ecom/brand-ecom.module').then(m => m.ProjetEcomBrandEcomModule)
      },
      {
        path: 'command-ecom',
        loadChildren: () => import('./command-ecom/command-ecom.module').then(m => m.ProjetEcomCommandEcomModule)
      },
      {
        path: 'command-product-ecom',
        loadChildren: () => import('./command-product-ecom/command-product-ecom.module').then(m => m.ProjetEcomCommandProductEcomModule)
      },
      {
        path: 'product-ecom',
        loadChildren: () => import('./product-ecom/product-ecom.module').then(m => m.ProjetEcomProductEcomModule)
      },
      {
        path: 'shop-cart-ecom',
        loadChildren: () => import('./shop-cart-ecom/shop-cart-ecom.module').then(m => m.ProjetEcomShopCartEcomModule)
      },
      {
        path: 'wish-list-ecom',
        loadChildren: () => import('./wish-list-ecom/wish-list-ecom.module').then(m => m.ProjetEcomWishListEcomModule)
      },
      {
        path: 'cpu-ecom',
        loadChildren: () => import('./cpu-ecom/cpu-ecom.module').then(m => m.ProjetEcomCpuEcomModule)
      },
      {
        path: 'harddrive-ecom',
        loadChildren: () => import('./harddrive-ecom/harddrive-ecom.module').then(m => m.ProjetEcomHarddriveEcomModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class ProjetEcomEntityModule {}

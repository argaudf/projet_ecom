import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHarddriveEcom } from 'app/shared/model/harddrive-ecom.model';

@Component({
  selector: 'jhi-harddrive-ecom-detail',
  templateUrl: './harddrive-ecom-detail.component.html'
})
export class HarddriveEcomDetailComponent implements OnInit {
  harddrive: IHarddriveEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ harddrive }) => {
      this.harddrive = harddrive;
    });
  }

  previousState() {
    window.history.back();
  }
}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IHarddriveEcom, HarddriveEcom } from 'app/shared/model/harddrive-ecom.model';
import { HarddriveEcomService } from './harddrive-ecom.service';
import { IProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';

@Component({
  selector: 'jhi-harddrive-ecom-update',
  templateUrl: './harddrive-ecom-update.component.html'
})
export class HarddriveEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  products: IProductEcom[];

  editForm = this.fb.group({
    id: [],
    size: [],
    products: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected harddriveService: HarddriveEcomService,
    protected productService: ProductEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ harddrive }) => {
      this.updateForm(harddrive);
    });
    this.productService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProductEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProductEcom[]>) => response.body)
      )
      .subscribe((res: IProductEcom[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(harddrive: IHarddriveEcom) {
    this.editForm.patchValue({
      id: harddrive.id,
      size: harddrive.size,
      products: harddrive.products
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const harddrive = this.createFromForm();
    if (harddrive.id !== undefined) {
      this.subscribeToSaveResponse(this.harddriveService.update(harddrive));
    } else {
      this.subscribeToSaveResponse(this.harddriveService.create(harddrive));
    }
  }

  private createFromForm(): IHarddriveEcom {
    return {
      ...new HarddriveEcom(),
      id: this.editForm.get(['id']).value,
      size: this.editForm.get(['size']).value,
      products: this.editForm.get(['products']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHarddriveEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProductById(index: number, item: IProductEcom) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}

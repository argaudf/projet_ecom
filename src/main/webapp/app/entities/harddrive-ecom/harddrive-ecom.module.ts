import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { HarddriveEcomComponent } from './harddrive-ecom.component';
import { HarddriveEcomDetailComponent } from './harddrive-ecom-detail.component';
import { HarddriveEcomUpdateComponent } from './harddrive-ecom-update.component';
import { HarddriveEcomDeletePopupComponent, HarddriveEcomDeleteDialogComponent } from './harddrive-ecom-delete-dialog.component';
import { harddriveRoute, harddrivePopupRoute } from './harddrive-ecom.route';

const ENTITY_STATES = [...harddriveRoute, ...harddrivePopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    HarddriveEcomComponent,
    HarddriveEcomDetailComponent,
    HarddriveEcomUpdateComponent,
    HarddriveEcomDeleteDialogComponent,
    HarddriveEcomDeletePopupComponent
  ],
  entryComponents: [HarddriveEcomDeleteDialogComponent]
})
export class ProjetEcomHarddriveEcomModule {}

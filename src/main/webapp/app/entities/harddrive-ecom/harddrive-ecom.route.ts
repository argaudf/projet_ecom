import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HarddriveEcom } from 'app/shared/model/harddrive-ecom.model';
import { HarddriveEcomService } from './harddrive-ecom.service';
import { HarddriveEcomComponent } from './harddrive-ecom.component';
import { HarddriveEcomDetailComponent } from './harddrive-ecom-detail.component';
import { HarddriveEcomUpdateComponent } from './harddrive-ecom-update.component';
import { HarddriveEcomDeletePopupComponent } from './harddrive-ecom-delete-dialog.component';
import { IHarddriveEcom } from 'app/shared/model/harddrive-ecom.model';

@Injectable({ providedIn: 'root' })
export class HarddriveEcomResolve implements Resolve<IHarddriveEcom> {
  constructor(private service: HarddriveEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHarddriveEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<HarddriveEcom>) => response.ok),
        map((harddrive: HttpResponse<HarddriveEcom>) => harddrive.body)
      );
    }
    return of(new HarddriveEcom());
  }
}

export const harddriveRoute: Routes = [
  {
    path: '',
    component: HarddriveEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.harddrive.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HarddriveEcomDetailComponent,
    resolve: {
      harddrive: HarddriveEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.harddrive.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HarddriveEcomUpdateComponent,
    resolve: {
      harddrive: HarddriveEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.harddrive.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HarddriveEcomUpdateComponent,
    resolve: {
      harddrive: HarddriveEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.harddrive.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const harddrivePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: HarddriveEcomDeletePopupComponent,
    resolve: {
      harddrive: HarddriveEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.harddrive.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

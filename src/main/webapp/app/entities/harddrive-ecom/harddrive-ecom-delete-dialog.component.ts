import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHarddriveEcom } from 'app/shared/model/harddrive-ecom.model';
import { HarddriveEcomService } from './harddrive-ecom.service';

@Component({
  selector: 'jhi-harddrive-ecom-delete-dialog',
  templateUrl: './harddrive-ecom-delete-dialog.component.html'
})
export class HarddriveEcomDeleteDialogComponent {
  harddrive: IHarddriveEcom;

  constructor(
    protected harddriveService: HarddriveEcomService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.harddriveService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'harddriveListModification',
        content: 'Deleted an harddrive'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-harddrive-ecom-delete-popup',
  template: ''
})
export class HarddriveEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ harddrive }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(HarddriveEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.harddrive = harddrive;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/harddrive-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/harddrive-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';

@Component({
  selector: 'jhi-shop-cart-ecom-detail',
  templateUrl: './shop-cart-ecom-detail.component.html'
})
export class ShopCartEcomDetailComponent implements OnInit {
  shopCart: IShopCartEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shopCart }) => {
      this.shopCart = shopCart;
    });
  }

  previousState() {
    window.history.back();
  }
}

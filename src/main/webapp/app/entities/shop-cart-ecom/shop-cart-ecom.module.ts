import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { ShopCartEcomComponent } from './shop-cart-ecom.component';
import { ShopCartEcomDetailComponent } from './shop-cart-ecom-detail.component';
import { ShopCartEcomUpdateComponent } from './shop-cart-ecom-update.component';
import { ShopCartEcomDeletePopupComponent, ShopCartEcomDeleteDialogComponent } from './shop-cart-ecom-delete-dialog.component';
import { shopCartRoute, shopCartPopupRoute } from './shop-cart-ecom.route';

const ENTITY_STATES = [...shopCartRoute, ...shopCartPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShopCartEcomComponent,
    ShopCartEcomDetailComponent,
    ShopCartEcomUpdateComponent,
    ShopCartEcomDeleteDialogComponent,
    ShopCartEcomDeletePopupComponent
  ],
  entryComponents: [ShopCartEcomDeleteDialogComponent]
})
export class ProjetEcomShopCartEcomModule {}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';
import { ShopCartEcomService } from './shop-cart-ecom.service';

@Component({
  selector: 'jhi-shop-cart-ecom-delete-dialog',
  templateUrl: './shop-cart-ecom-delete-dialog.component.html'
})
export class ShopCartEcomDeleteDialogComponent {
  shopCart: IShopCartEcom;

  constructor(
    protected shopCartService: ShopCartEcomService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shopCartService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shopCartListModification',
        content: 'Deleted an shopCart'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shop-cart-ecom-delete-popup',
  template: ''
})
export class ShopCartEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shopCart }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShopCartEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shopCart = shopCart;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shop-cart-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shop-cart-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

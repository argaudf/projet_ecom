import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';
import { IProductEcom } from 'app/shared/model/product-ecom.model';

type EntityResponseType = HttpResponse<IShopCartEcom>;
type EntityArrayResponseType = HttpResponse<IShopCartEcom[]>;

@Injectable({ providedIn: 'root' })
export class ShopCartEcomService {
  public resourceUrl = SERVER_API_URL + 'api/shop-carts';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/shop-carts';

  constructor(protected http: HttpClient) {}

  create(shopCart: IShopCartEcom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shopCart);
    return this.http
      .post<IShopCartEcom>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  public addProduct(login: String, product: IProductEcom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(product);
    return this.http
      .post<IShopCartEcom>(this.resourceUrl + '/' + login, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  public updateProduct(login: String, id: number): Observable<EntityResponseType> {
    return this.http
      .put<any>(this.resourceUrl + '/update/' + login + '/' + id, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shopCart: IShopCartEcom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shopCart);
    return this.http
      .put<IShopCartEcom>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShopCartEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShopCartEcom[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  deleteAndUpdate(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/delete/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShopCartEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(shopCart: IShopCartEcom): IShopCartEcom {
    const copy: IShopCartEcom = Object.assign({}, shopCart, {
      addTime: shopCart.addTime != null && shopCart.addTime.isValid() ? shopCart.addTime.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.addTime = res.body.addTime != null ? moment(res.body.addTime) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shopCart: IShopCartEcom) => {
        shopCart.addTime = shopCart.addTime != null ? moment(shopCart.addTime) : null;
      });
    }
    return res;
  }
}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IShopCartEcom, ShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';
import { ShopCartEcomService } from './shop-cart-ecom.service';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';
import { IProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';

@Component({
  selector: 'jhi-shop-cart-ecom-update',
  templateUrl: './shop-cart-ecom-update.component.html'
})
export class ShopCartEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  userinfos: IUserInfoEcom[];

  products: IProductEcom[];
  addTimeDp: any;

  editForm = this.fb.group({
    id: [],
    quantity: [],
    addTime: [],
    userId: [],
    productId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shopCartService: ShopCartEcomService,
    protected userInfoService: UserInfoEcomService,
    protected productService: ProductEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shopCart }) => {
      this.updateForm(shopCart);
    });
    this.userInfoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUserInfoEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUserInfoEcom[]>) => response.body)
      )
      .subscribe((res: IUserInfoEcom[]) => (this.userinfos = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.productService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProductEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProductEcom[]>) => response.body)
      )
      .subscribe((res: IProductEcom[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shopCart: IShopCartEcom) {
    this.editForm.patchValue({
      id: shopCart.id,
      quantity: shopCart.quantity,
      addTime: shopCart.addTime,
      userId: shopCart.userId,
      productId: shopCart.productId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shopCart = this.createFromForm();
    if (shopCart.id !== undefined) {
      this.subscribeToSaveResponse(this.shopCartService.update(shopCart));
    } else {
      this.subscribeToSaveResponse(this.shopCartService.create(shopCart));
    }
  }

  private createFromForm(): IShopCartEcom {
    return {
      ...new ShopCartEcom(),
      id: this.editForm.get(['id']).value,
      quantity: this.editForm.get(['quantity']).value,
      addTime: this.editForm.get(['addTime']).value,
      userId: this.editForm.get(['userId']).value,
      productId: this.editForm.get(['productId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShopCartEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserInfoById(index: number, item: IUserInfoEcom) {
    return item.id;
  }

  trackProductById(index: number, item: IProductEcom) {
    return item.id;
  }
}

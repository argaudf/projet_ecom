import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';
import { ShopCartEcomService } from './shop-cart-ecom.service';
import { ShopCartEcomComponent } from './shop-cart-ecom.component';
import { ShopCartEcomDetailComponent } from './shop-cart-ecom-detail.component';
import { ShopCartEcomUpdateComponent } from './shop-cart-ecom-update.component';
import { ShopCartEcomDeletePopupComponent } from './shop-cart-ecom-delete-dialog.component';
import { IShopCartEcom } from 'app/shared/model/shop-cart-ecom.model';

@Injectable({ providedIn: 'root' })
export class ShopCartEcomResolve implements Resolve<IShopCartEcom> {
  constructor(private service: ShopCartEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShopCartEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShopCartEcom>) => response.ok),
        map((shopCart: HttpResponse<ShopCartEcom>) => shopCart.body)
      );
    }
    return of(new ShopCartEcom());
  }
}

export const shopCartRoute: Routes = [
  {
    path: '',
    component: ShopCartEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.shopCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShopCartEcomDetailComponent,
    resolve: {
      shopCart: ShopCartEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.shopCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShopCartEcomUpdateComponent,
    resolve: {
      shopCart: ShopCartEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.shopCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShopCartEcomUpdateComponent,
    resolve: {
      shopCart: ShopCartEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.shopCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shopCartPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShopCartEcomDeletePopupComponent,
    resolve: {
      shopCart: ShopCartEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.shopCart.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

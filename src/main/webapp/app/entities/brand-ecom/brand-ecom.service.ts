import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBrandEcom } from 'app/shared/model/brand-ecom.model';

type EntityResponseType = HttpResponse<IBrandEcom>;
type EntityArrayResponseType = HttpResponse<IBrandEcom[]>;

@Injectable({ providedIn: 'root' })
export class BrandEcomService {
  public resourceUrl = SERVER_API_URL + 'api/brands';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/brands';

  constructor(protected http: HttpClient) {}

  create(brand: IBrandEcom): Observable<EntityResponseType> {
    return this.http.post<IBrandEcom>(this.resourceUrl, brand, { observe: 'response' });
  }

  update(brand: IBrandEcom): Observable<EntityResponseType> {
    return this.http.put<IBrandEcom>(this.resourceUrl, brand, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBrandEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBrandEcom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBrandEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IBrandEcom, BrandEcom } from 'app/shared/model/brand-ecom.model';
import { BrandEcomService } from './brand-ecom.service';

@Component({
  selector: 'jhi-brand-ecom-update',
  templateUrl: './brand-ecom-update.component.html'
})
export class BrandEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    brandName: []
  });

  constructor(protected brandService: BrandEcomService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ brand }) => {
      this.updateForm(brand);
    });
  }

  updateForm(brand: IBrandEcom) {
    this.editForm.patchValue({
      id: brand.id,
      brandName: brand.brandName
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const brand = this.createFromForm();
    if (brand.id !== undefined) {
      this.subscribeToSaveResponse(this.brandService.update(brand));
    } else {
      this.subscribeToSaveResponse(this.brandService.create(brand));
    }
  }

  private createFromForm(): IBrandEcom {
    return {
      ...new BrandEcom(),
      id: this.editForm.get(['id']).value,
      brandName: this.editForm.get(['brandName']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBrandEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BrandEcom } from 'app/shared/model/brand-ecom.model';
import { BrandEcomService } from './brand-ecom.service';
import { BrandEcomComponent } from './brand-ecom.component';
import { BrandEcomDetailComponent } from './brand-ecom-detail.component';
import { BrandEcomUpdateComponent } from './brand-ecom-update.component';
import { BrandEcomDeletePopupComponent } from './brand-ecom-delete-dialog.component';
import { IBrandEcom } from 'app/shared/model/brand-ecom.model';

@Injectable({ providedIn: 'root' })
export class BrandEcomResolve implements Resolve<IBrandEcom> {
  constructor(private service: BrandEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBrandEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BrandEcom>) => response.ok),
        map((brand: HttpResponse<BrandEcom>) => brand.body)
      );
    }
    return of(new BrandEcom());
  }
}

export const brandRoute: Routes = [
  {
    path: '',
    component: BrandEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.brand.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BrandEcomDetailComponent,
    resolve: {
      brand: BrandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.brand.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BrandEcomUpdateComponent,
    resolve: {
      brand: BrandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.brand.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BrandEcomUpdateComponent,
    resolve: {
      brand: BrandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.brand.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const brandPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BrandEcomDeletePopupComponent,
    resolve: {
      brand: BrandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.brand.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

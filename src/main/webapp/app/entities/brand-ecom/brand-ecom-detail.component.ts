import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBrandEcom } from 'app/shared/model/brand-ecom.model';

@Component({
  selector: 'jhi-brand-ecom-detail',
  templateUrl: './brand-ecom-detail.component.html'
})
export class BrandEcomDetailComponent implements OnInit {
  brand: IBrandEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brand }) => {
      this.brand = brand;
    });
  }

  previousState() {
    window.history.back();
  }
}

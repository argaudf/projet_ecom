import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { BrandEcomComponent } from './brand-ecom.component';
import { BrandEcomDetailComponent } from './brand-ecom-detail.component';
import { BrandEcomUpdateComponent } from './brand-ecom-update.component';
import { BrandEcomDeletePopupComponent, BrandEcomDeleteDialogComponent } from './brand-ecom-delete-dialog.component';
import { brandRoute, brandPopupRoute } from './brand-ecom.route';

const ENTITY_STATES = [...brandRoute, ...brandPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BrandEcomComponent,
    BrandEcomDetailComponent,
    BrandEcomUpdateComponent,
    BrandEcomDeleteDialogComponent,
    BrandEcomDeletePopupComponent
  ],
  entryComponents: [BrandEcomDeleteDialogComponent]
})
export class ProjetEcomBrandEcomModule {}

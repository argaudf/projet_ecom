import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AddressEcom } from 'app/shared/model/address-ecom.model';
import { AddressEcomService } from './address-ecom.service';
import { AddressEcomComponent } from './address-ecom.component';
import { AddressEcomDetailComponent } from './address-ecom-detail.component';
import { AddressEcomUpdateComponent } from './address-ecom-update.component';
import { AddressEcomDeletePopupComponent } from './address-ecom-delete-dialog.component';
import { IAddressEcom } from 'app/shared/model/address-ecom.model';

@Injectable({ providedIn: 'root' })
export class AddressEcomResolve implements Resolve<IAddressEcom> {
  constructor(private service: AddressEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAddressEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<AddressEcom>) => response.ok),
        map((address: HttpResponse<AddressEcom>) => address.body)
      );
    }
    return of(new AddressEcom());
  }
}

export const addressRoute: Routes = [
  {
    path: '',
    component: AddressEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.address.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AddressEcomDetailComponent,
    resolve: {
      address: AddressEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.address.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AddressEcomUpdateComponent,
    resolve: {
      address: AddressEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.address.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AddressEcomUpdateComponent,
    resolve: {
      address: AddressEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.address.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const addressPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: AddressEcomDeletePopupComponent,
    resolve: {
      address: AddressEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.address.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

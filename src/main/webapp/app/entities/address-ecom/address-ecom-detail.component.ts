import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAddressEcom } from 'app/shared/model/address-ecom.model';

@Component({
  selector: 'jhi-address-ecom-detail',
  templateUrl: './address-ecom-detail.component.html'
})
export class AddressEcomDetailComponent implements OnInit {
  address: IAddressEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ address }) => {
      this.address = address;
    });
  }

  previousState() {
    window.history.back();
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { AddressEcomComponent } from './address-ecom.component';
import { AddressEcomDetailComponent } from './address-ecom-detail.component';
import { AddressEcomUpdateComponent } from './address-ecom-update.component';
import { AddressEcomDeletePopupComponent, AddressEcomDeleteDialogComponent } from './address-ecom-delete-dialog.component';
import { addressRoute, addressPopupRoute } from './address-ecom.route';

const ENTITY_STATES = [...addressRoute, ...addressPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    AddressEcomComponent,
    AddressEcomDetailComponent,
    AddressEcomUpdateComponent,
    AddressEcomDeleteDialogComponent,
    AddressEcomDeletePopupComponent
  ],
  entryComponents: [AddressEcomDeleteDialogComponent]
})
export class ProjetEcomAddressEcomModule {}

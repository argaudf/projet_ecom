import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAddressEcom } from 'app/shared/model/address-ecom.model';

type EntityResponseType = HttpResponse<IAddressEcom>;
type EntityArrayResponseType = HttpResponse<IAddressEcom[]>;

@Injectable({ providedIn: 'root' })
export class AddressEcomService {
  public resourceUrl = SERVER_API_URL + 'api/addresses';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/addresses';

  constructor(protected http: HttpClient) {}

  create(address: IAddressEcom): Observable<EntityResponseType> {
    return this.http.post<IAddressEcom>(this.resourceUrl, address, { observe: 'response' });
  }

  update(address: IAddressEcom): Observable<EntityResponseType> {
    return this.http.put<IAddressEcom>(this.resourceUrl, address, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAddressEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAddressEcom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAddressEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

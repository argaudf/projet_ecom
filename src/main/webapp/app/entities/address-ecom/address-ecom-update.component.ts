import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAddressEcom, AddressEcom } from 'app/shared/model/address-ecom.model';
import { AddressEcomService } from './address-ecom.service';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';

@Component({
  selector: 'jhi-address-ecom-update',
  templateUrl: './address-ecom-update.component.html'
})
export class AddressEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  userinfos: IUserInfoEcom[];

  editForm = this.fb.group({
    id: [],
    addressComplete: [],
    receiveName: [],
    receiveTel: [],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected addressService: AddressEcomService,
    protected userInfoService: UserInfoEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ address }) => {
      this.updateForm(address);
    });
    this.userInfoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUserInfoEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUserInfoEcom[]>) => response.body)
      )
      .subscribe((res: IUserInfoEcom[]) => (this.userinfos = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(address: IAddressEcom) {
    this.editForm.patchValue({
      id: address.id,
      addressComplete: address.addressComplete,
      receiveName: address.receiveName,
      receiveTel: address.receiveTel,
      userId: address.userId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const address = this.createFromForm();
    if (address.id !== undefined) {
      this.subscribeToSaveResponse(this.addressService.update(address));
    } else {
      this.subscribeToSaveResponse(this.addressService.create(address));
    }
  }

  private createFromForm(): IAddressEcom {
    return {
      ...new AddressEcom(),
      id: this.editForm.get(['id']).value,
      addressComplete: this.editForm.get(['addressComplete']).value,
      receiveName: this.editForm.get(['receiveName']).value,
      receiveTel: this.editForm.get(['receiveTel']).value,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAddressEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserInfoById(index: number, item: IUserInfoEcom) {
    return item.id;
  }
}

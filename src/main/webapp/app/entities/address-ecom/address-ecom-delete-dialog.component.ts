import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAddressEcom } from 'app/shared/model/address-ecom.model';
import { AddressEcomService } from './address-ecom.service';

@Component({
  selector: 'jhi-address-ecom-delete-dialog',
  templateUrl: './address-ecom-delete-dialog.component.html'
})
export class AddressEcomDeleteDialogComponent {
  address: IAddressEcom;

  constructor(protected addressService: AddressEcomService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.addressService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'addressListModification',
        content: 'Deleted an address'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-address-ecom-delete-popup',
  template: ''
})
export class AddressEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ address }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(AddressEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.address = address;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/address-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/address-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

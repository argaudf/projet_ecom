import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { ProductEcomComponent } from './product-ecom.component';
import { ProductEcomDetailComponent } from './product-ecom-detail.component';
import { ProductEcomUpdateComponent } from './product-ecom-update.component';
import { ProductEcomDeletePopupComponent, ProductEcomDeleteDialogComponent } from './product-ecom-delete-dialog.component';
import { productRoute, productPopupRoute } from './product-ecom.route';

const ENTITY_STATES = [...productRoute, ...productPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ProductEcomComponent,
    ProductEcomDetailComponent,
    ProductEcomUpdateComponent,
    ProductEcomDeleteDialogComponent,
    ProductEcomDeletePopupComponent
  ],
  entryComponents: [ProductEcomDeleteDialogComponent]
})
export class ProjetEcomProductEcomModule {}

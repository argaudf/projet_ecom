import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IProductEcom, ProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from './product-ecom.service';
import { IBrandEcom } from 'app/shared/model/brand-ecom.model';
import { BrandEcomService } from 'app/entities/brand-ecom/brand-ecom.service';
import { IWishListEcom } from 'app/shared/model/wish-list-ecom.model';
import { WishListEcomService } from 'app/entities/wish-list-ecom/wish-list-ecom.service';
import { ICpuEcom } from 'app/shared/model/cpu-ecom.model';
import { CpuEcomService } from 'app/entities/cpu-ecom/cpu-ecom.service';
import { IHarddriveEcom } from 'app/shared/model/harddrive-ecom.model';
import { HarddriveEcomService } from 'app/entities/harddrive-ecom/harddrive-ecom.service';

@Component({
  selector: 'jhi-product-ecom-update',
  templateUrl: './product-ecom-update.component.html'
})
export class ProductEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  brands: IBrandEcom[];

  wishlists: IWishListEcom[];

  cpus: ICpuEcom[];

  harddrives: IHarddriveEcom[];

  editForm = this.fb.group({
    id: [],
    proName: [],
    proPrice: [],
    stock: [],
    description: [],
    picture: [],
    brandId: [],
    cpuId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected productService: ProductEcomService,
    protected brandService: BrandEcomService,
    protected wishListService: WishListEcomService,
    protected cpuService: CpuEcomService,
    protected harddriveService: HarddriveEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ product }) => {
      this.updateForm(product);
    });
    this.brandService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBrandEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBrandEcom[]>) => response.body)
      )
      .subscribe((res: IBrandEcom[]) => (this.brands = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.wishListService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IWishListEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWishListEcom[]>) => response.body)
      )
      .subscribe((res: IWishListEcom[]) => (this.wishlists = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.cpuService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICpuEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICpuEcom[]>) => response.body)
      )
      .subscribe((res: ICpuEcom[]) => (this.cpus = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.harddriveService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IHarddriveEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IHarddriveEcom[]>) => response.body)
      )
      .subscribe((res: IHarddriveEcom[]) => (this.harddrives = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(product: IProductEcom) {
    this.editForm.patchValue({
      id: product.id,
      proName: product.proName,
      proPrice: product.proPrice,
      stock: product.stock,
      description: product.description,
      picture: product.picture,
      brandId: product.brandId,
      cpuId: product.cpuId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const product = this.createFromForm();
    if (product.id !== undefined) {
      this.subscribeToSaveResponse(this.productService.update(product));
    } else {
      this.subscribeToSaveResponse(this.productService.create(product));
    }
  }

  private createFromForm(): IProductEcom {
    return {
      ...new ProductEcom(),
      id: this.editForm.get(['id']).value,
      proName: this.editForm.get(['proName']).value,
      proPrice: this.editForm.get(['proPrice']).value,
      stock: this.editForm.get(['stock']).value,
      description: this.editForm.get(['description']).value,
      picture: this.editForm.get(['picture']).value,
      brandId: this.editForm.get(['brandId']).value,
      cpuId: this.editForm.get(['cpuId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBrandById(index: number, item: IBrandEcom) {
    return item.id;
  }

  trackWishListById(index: number, item: IWishListEcom) {
    return item.id;
  }

  trackCpuById(index: number, item: ICpuEcom) {
    return item.id;
  }

  trackHarddriveById(index: number, item: IHarddriveEcom) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}

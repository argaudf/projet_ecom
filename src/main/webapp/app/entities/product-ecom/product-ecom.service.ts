import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductEcom } from 'app/shared/model/product-ecom.model';

type EntityResponseType = HttpResponse<IProductEcom>;
type EntityArrayResponseType = HttpResponse<IProductEcom[]>;

@Injectable({ providedIn: 'root' })
export class ProductEcomService {
  public resourceUrl = SERVER_API_URL + 'api/products';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/products';

  constructor(protected http: HttpClient) {}

  create(product: IProductEcom): Observable<EntityResponseType> {
    return this.http.post<IProductEcom>(this.resourceUrl, product, { observe: 'response' });
  }

  update(product: IProductEcom): Observable<EntityResponseType> {
    return this.http.put<IProductEcom>(this.resourceUrl, product, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductEcom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

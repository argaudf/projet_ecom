import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductEcom } from 'app/shared/model/product-ecom.model';

@Component({
  selector: 'jhi-product-ecom-detail',
  templateUrl: './product-ecom-detail.component.html'
})
export class ProductEcomDetailComponent implements OnInit {
  product: IProductEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ product }) => {
      this.product = product;
    });
  }

  previousState() {
    window.history.back();
  }
}

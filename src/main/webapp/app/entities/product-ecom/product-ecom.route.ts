import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from './product-ecom.service';
import { ProductEcomComponent } from './product-ecom.component';
import { ProductEcomDetailComponent } from './product-ecom-detail.component';
import { ProductEcomUpdateComponent } from './product-ecom-update.component';
import { ProductEcomDeletePopupComponent } from './product-ecom-delete-dialog.component';
import { IProductEcom } from 'app/shared/model/product-ecom.model';

@Injectable({ providedIn: 'root' })
export class ProductEcomResolve implements Resolve<IProductEcom> {
  constructor(private service: ProductEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IProductEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ProductEcom>) => response.ok),
        map((product: HttpResponse<ProductEcom>) => product.body)
      );
    }
    return of(new ProductEcom());
  }
}

export const productRoute: Routes = [
  {
    path: '',
    component: ProductEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.product.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ProductEcomDetailComponent,
    resolve: {
      product: ProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.product.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ProductEcomUpdateComponent,
    resolve: {
      product: ProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.product.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ProductEcomUpdateComponent,
    resolve: {
      product: ProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.product.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const productPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ProductEcomDeletePopupComponent,
    resolve: {
      product: ProductEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.product.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

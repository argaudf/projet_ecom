import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { UserInfoEcomComponent } from './user-info-ecom.component';
import { UserInfoEcomDetailComponent } from './user-info-ecom-detail.component';
import { UserInfoEcomUpdateComponent } from './user-info-ecom-update.component';
import { UserInfoEcomDeletePopupComponent, UserInfoEcomDeleteDialogComponent } from './user-info-ecom-delete-dialog.component';
import { userInfoRoute, userInfoPopupRoute } from './user-info-ecom.route';

const ENTITY_STATES = [...userInfoRoute, ...userInfoPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserInfoEcomComponent,
    UserInfoEcomDetailComponent,
    UserInfoEcomUpdateComponent,
    UserInfoEcomDeleteDialogComponent,
    UserInfoEcomDeletePopupComponent
  ],
  entryComponents: [UserInfoEcomDeleteDialogComponent]
})
export class ProjetEcomUserInfoEcomModule {}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IUserInfoEcom, UserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from './user-info-ecom.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-user-info-ecom-update',
  templateUrl: './user-info-ecom-update.component.html'
})
export class UserInfoEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    userStatus: [],
    tel: [],
    email: [],
    passwordQ: [],
    passwordA: [],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected userInfoService: UserInfoEcomService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userInfo }) => {
      this.updateForm(userInfo);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(userInfo: IUserInfoEcom) {
    this.editForm.patchValue({
      id: userInfo.id,
      userStatus: userInfo.userStatus,
      tel: userInfo.tel,
      email: userInfo.email,
      passwordQ: userInfo.passwordQ,
      passwordA: userInfo.passwordA,
      userId: userInfo.userId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userInfo = this.createFromForm();
    if (userInfo.id !== undefined) {
      this.subscribeToSaveResponse(this.userInfoService.update(userInfo));
    } else {
      this.subscribeToSaveResponse(this.userInfoService.create(userInfo));
    }
  }

  private createFromForm(): IUserInfoEcom {
    return {
      ...new UserInfoEcom(),
      id: this.editForm.get(['id']).value,
      userStatus: this.editForm.get(['userStatus']).value,
      tel: this.editForm.get(['tel']).value,
      email: this.editForm.get(['email']).value,
      passwordQ: this.editForm.get(['passwordQ']).value,
      passwordA: this.editForm.get(['passwordA']).value,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserInfoEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}

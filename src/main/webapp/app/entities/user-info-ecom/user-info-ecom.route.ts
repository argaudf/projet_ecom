import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from './user-info-ecom.service';
import { UserInfoEcomComponent } from './user-info-ecom.component';
import { UserInfoEcomDetailComponent } from './user-info-ecom-detail.component';
import { UserInfoEcomUpdateComponent } from './user-info-ecom-update.component';
import { UserInfoEcomDeletePopupComponent } from './user-info-ecom-delete-dialog.component';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';

@Injectable({ providedIn: 'root' })
export class UserInfoEcomResolve implements Resolve<IUserInfoEcom> {
  constructor(private service: UserInfoEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserInfoEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<UserInfoEcom>) => response.ok),
        map((userInfo: HttpResponse<UserInfoEcom>) => userInfo.body)
      );
    }
    return of(new UserInfoEcom());
  }
}

export const userInfoRoute: Routes = [
  {
    path: '',
    component: UserInfoEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.userInfo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserInfoEcomDetailComponent,
    resolve: {
      userInfo: UserInfoEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.userInfo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserInfoEcomUpdateComponent,
    resolve: {
      userInfo: UserInfoEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.userInfo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserInfoEcomUpdateComponent,
    resolve: {
      userInfo: UserInfoEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.userInfo.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userInfoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserInfoEcomDeletePopupComponent,
    resolve: {
      userInfo: UserInfoEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.userInfo.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

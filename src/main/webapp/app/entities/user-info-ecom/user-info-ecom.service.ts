import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';

type EntityResponseType = HttpResponse<IUserInfoEcom>;
type EntityArrayResponseType = HttpResponse<IUserInfoEcom[]>;

@Injectable({ providedIn: 'root' })
export class UserInfoEcomService {
  public resourceUrl = SERVER_API_URL + 'api/user-infos';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/user-infos';

  constructor(protected http: HttpClient) {}

  create(userInfo: IUserInfoEcom): Observable<EntityResponseType> {
    return this.http.post<IUserInfoEcom>(this.resourceUrl, userInfo, { observe: 'response' });
  }

  update(userInfo: IUserInfoEcom): Observable<EntityResponseType> {
    return this.http.put<IUserInfoEcom>(this.resourceUrl, userInfo, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserInfoEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserInfoEcom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserInfoEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

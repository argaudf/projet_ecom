import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from './user-info-ecom.service';

@Component({
  selector: 'jhi-user-info-ecom-delete-dialog',
  templateUrl: './user-info-ecom-delete-dialog.component.html'
})
export class UserInfoEcomDeleteDialogComponent {
  userInfo: IUserInfoEcom;

  constructor(
    protected userInfoService: UserInfoEcomService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userInfoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'userInfoListModification',
        content: 'Deleted an userInfo'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-info-ecom-delete-popup',
  template: ''
})
export class UserInfoEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userInfo }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserInfoEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userInfo = userInfo;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/user-info-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/user-info-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

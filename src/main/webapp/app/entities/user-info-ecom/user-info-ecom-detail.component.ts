import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';

@Component({
  selector: 'jhi-user-info-ecom-detail',
  templateUrl: './user-info-ecom-detail.component.html'
})
export class UserInfoEcomDetailComponent implements OnInit {
  userInfo: IUserInfoEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userInfo }) => {
      this.userInfo = userInfo;
    });
  }

  previousState() {
    window.history.back();
  }
}

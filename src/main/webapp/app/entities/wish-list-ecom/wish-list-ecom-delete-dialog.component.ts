import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWishListEcom } from 'app/shared/model/wish-list-ecom.model';
import { WishListEcomService } from './wish-list-ecom.service';

@Component({
  selector: 'jhi-wish-list-ecom-delete-dialog',
  templateUrl: './wish-list-ecom-delete-dialog.component.html'
})
export class WishListEcomDeleteDialogComponent {
  wishList: IWishListEcom;

  constructor(
    protected wishListService: WishListEcomService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.wishListService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'wishListListModification',
        content: 'Deleted an wishList'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-wish-list-ecom-delete-popup',
  template: ''
})
export class WishListEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ wishList }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(WishListEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.wishList = wishList;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/wish-list-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/wish-list-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

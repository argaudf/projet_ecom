import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { WishListEcomComponent } from './wish-list-ecom.component';
import { WishListEcomDetailComponent } from './wish-list-ecom-detail.component';
import { WishListEcomUpdateComponent } from './wish-list-ecom-update.component';
import { WishListEcomDeletePopupComponent, WishListEcomDeleteDialogComponent } from './wish-list-ecom-delete-dialog.component';
import { wishListRoute, wishListPopupRoute } from './wish-list-ecom.route';

const ENTITY_STATES = [...wishListRoute, ...wishListPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    WishListEcomComponent,
    WishListEcomDetailComponent,
    WishListEcomUpdateComponent,
    WishListEcomDeleteDialogComponent,
    WishListEcomDeletePopupComponent
  ],
  entryComponents: [WishListEcomDeleteDialogComponent]
})
export class ProjetEcomWishListEcomModule {}

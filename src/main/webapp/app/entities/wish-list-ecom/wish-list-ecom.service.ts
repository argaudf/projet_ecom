import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWishListEcom } from 'app/shared/model/wish-list-ecom.model';

type EntityResponseType = HttpResponse<IWishListEcom>;
type EntityArrayResponseType = HttpResponse<IWishListEcom[]>;

@Injectable({ providedIn: 'root' })
export class WishListEcomService {
  public resourceUrl = SERVER_API_URL + 'api/wish-lists';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/wish-lists';

  constructor(protected http: HttpClient) {}

  create(wishList: IWishListEcom): Observable<EntityResponseType> {
    return this.http.post<IWishListEcom>(this.resourceUrl, wishList, { observe: 'response' });
  }

  update(wishList: IWishListEcom): Observable<EntityResponseType> {
    return this.http.put<IWishListEcom>(this.resourceUrl, wishList, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWishListEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWishListEcom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWishListEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

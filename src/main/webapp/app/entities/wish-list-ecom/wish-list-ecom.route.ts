import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { WishListEcom } from 'app/shared/model/wish-list-ecom.model';
import { WishListEcomService } from './wish-list-ecom.service';
import { WishListEcomComponent } from './wish-list-ecom.component';
import { WishListEcomDetailComponent } from './wish-list-ecom-detail.component';
import { WishListEcomUpdateComponent } from './wish-list-ecom-update.component';
import { WishListEcomDeletePopupComponent } from './wish-list-ecom-delete-dialog.component';
import { IWishListEcom } from 'app/shared/model/wish-list-ecom.model';

@Injectable({ providedIn: 'root' })
export class WishListEcomResolve implements Resolve<IWishListEcom> {
  constructor(private service: WishListEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWishListEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<WishListEcom>) => response.ok),
        map((wishList: HttpResponse<WishListEcom>) => wishList.body)
      );
    }
    return of(new WishListEcom());
  }
}

export const wishListRoute: Routes = [
  {
    path: '',
    component: WishListEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.wishList.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WishListEcomDetailComponent,
    resolve: {
      wishList: WishListEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.wishList.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WishListEcomUpdateComponent,
    resolve: {
      wishList: WishListEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.wishList.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WishListEcomUpdateComponent,
    resolve: {
      wishList: WishListEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.wishList.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const wishListPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: WishListEcomDeletePopupComponent,
    resolve: {
      wishList: WishListEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.wishList.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IWishListEcom, WishListEcom } from 'app/shared/model/wish-list-ecom.model';
import { WishListEcomService } from './wish-list-ecom.service';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';
import { IProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';

@Component({
  selector: 'jhi-wish-list-ecom-update',
  templateUrl: './wish-list-ecom-update.component.html'
})
export class WishListEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUserInfoEcom[];

  products: IProductEcom[];

  editForm = this.fb.group({
    id: [],
    userId: [],
    products: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected wishListService: WishListEcomService,
    protected userInfoService: UserInfoEcomService,
    protected productService: ProductEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ wishList }) => {
      this.updateForm(wishList);
    });
    this.userInfoService
      .query({ filter: 'wishlist-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IUserInfoEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUserInfoEcom[]>) => response.body)
      )
      .subscribe(
        (res: IUserInfoEcom[]) => {
          if (!this.editForm.get('userId').value) {
            this.users = res;
          } else {
            this.userInfoService
              .find(this.editForm.get('userId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IUserInfoEcom>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IUserInfoEcom>) => subResponse.body)
              )
              .subscribe(
                (subRes: IUserInfoEcom) => (this.users = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.productService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProductEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProductEcom[]>) => response.body)
      )
      .subscribe((res: IProductEcom[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(wishList: IWishListEcom) {
    this.editForm.patchValue({
      id: wishList.id,
      userId: wishList.userId,
      products: wishList.products
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const wishList = this.createFromForm();
    if (wishList.id !== undefined) {
      this.subscribeToSaveResponse(this.wishListService.update(wishList));
    } else {
      this.subscribeToSaveResponse(this.wishListService.create(wishList));
    }
  }

  private createFromForm(): IWishListEcom {
    return {
      ...new WishListEcom(),
      id: this.editForm.get(['id']).value,
      userId: this.editForm.get(['userId']).value,
      products: this.editForm.get(['products']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWishListEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserInfoById(index: number, item: IUserInfoEcom) {
    return item.id;
  }

  trackProductById(index: number, item: IProductEcom) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}

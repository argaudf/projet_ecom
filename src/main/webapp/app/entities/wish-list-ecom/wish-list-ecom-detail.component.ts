import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWishListEcom } from 'app/shared/model/wish-list-ecom.model';

@Component({
  selector: 'jhi-wish-list-ecom-detail',
  templateUrl: './wish-list-ecom-detail.component.html'
})
export class WishListEcomDetailComponent implements OnInit {
  wishList: IWishListEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ wishList }) => {
      this.wishList = wishList;
    });
  }

  previousState() {
    window.history.back();
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICommandEcom } from 'app/shared/model/command-ecom.model';

@Component({
  selector: 'jhi-command-ecom-detail',
  templateUrl: './command-ecom-detail.component.html'
})
export class CommandEcomDetailComponent implements OnInit {
  command: ICommandEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ command }) => {
      this.command = command;
    });
  }

  previousState() {
    window.history.back();
  }
}

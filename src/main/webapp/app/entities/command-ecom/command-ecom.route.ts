import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CommandEcom } from 'app/shared/model/command-ecom.model';
import { CommandEcomService } from './command-ecom.service';
import { CommandEcomComponent } from './command-ecom.component';
import { CommandEcomDetailComponent } from './command-ecom-detail.component';
import { CommandEcomUpdateComponent } from './command-ecom-update.component';
import { CommandEcomDeletePopupComponent } from './command-ecom-delete-dialog.component';
import { ICommandEcom } from 'app/shared/model/command-ecom.model';

@Injectable({ providedIn: 'root' })
export class CommandEcomResolve implements Resolve<ICommandEcom> {
  constructor(private service: CommandEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICommandEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CommandEcom>) => response.ok),
        map((command: HttpResponse<CommandEcom>) => command.body)
      );
    }
    return of(new CommandEcom());
  }
}

export const commandRoute: Routes = [
  {
    path: '',
    component: CommandEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.command.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CommandEcomDetailComponent,
    resolve: {
      command: CommandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.command.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CommandEcomUpdateComponent,
    resolve: {
      command: CommandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.command.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CommandEcomUpdateComponent,
    resolve: {
      command: CommandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.command.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const commandPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CommandEcomDeletePopupComponent,
    resolve: {
      command: CommandEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.command.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

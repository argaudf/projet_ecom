import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICommandEcom } from 'app/shared/model/command-ecom.model';

type EntityResponseType = HttpResponse<ICommandEcom>;
type EntityArrayResponseType = HttpResponse<ICommandEcom[]>;

@Injectable({ providedIn: 'root' })
export class CommandEcomService {
  public resourceUrl = SERVER_API_URL + 'api/commands';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/commands';

  constructor(protected http: HttpClient) {}

  create(command: ICommandEcom): Observable<EntityResponseType> {
    return this.http.post<ICommandEcom>(this.resourceUrl, command, { observe: 'response' });
  }

  update(command: ICommandEcom): Observable<EntityResponseType> {
    return this.http.put<ICommandEcom>(this.resourceUrl, command, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICommandEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICommandEcom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICommandEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

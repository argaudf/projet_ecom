import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICommandEcom } from 'app/shared/model/command-ecom.model';
import { CommandEcomService } from './command-ecom.service';

@Component({
  selector: 'jhi-command-ecom-delete-dialog',
  templateUrl: './command-ecom-delete-dialog.component.html'
})
export class CommandEcomDeleteDialogComponent {
  command: ICommandEcom;

  constructor(protected commandService: CommandEcomService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.commandService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'commandListModification',
        content: 'Deleted an command'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-command-ecom-delete-popup',
  template: ''
})
export class CommandEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ command }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CommandEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.command = command;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/command-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/command-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

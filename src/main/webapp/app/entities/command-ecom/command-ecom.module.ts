import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { CommandEcomComponent } from './command-ecom.component';
import { CommandEcomDetailComponent } from './command-ecom-detail.component';
import { CommandEcomUpdateComponent } from './command-ecom-update.component';
import { CommandEcomDeletePopupComponent, CommandEcomDeleteDialogComponent } from './command-ecom-delete-dialog.component';
import { commandRoute, commandPopupRoute } from './command-ecom.route';

const ENTITY_STATES = [...commandRoute, ...commandPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CommandEcomComponent,
    CommandEcomDetailComponent,
    CommandEcomUpdateComponent,
    CommandEcomDeleteDialogComponent,
    CommandEcomDeletePopupComponent
  ],
  entryComponents: [CommandEcomDeleteDialogComponent]
})
export class ProjetEcomCommandEcomModule {}

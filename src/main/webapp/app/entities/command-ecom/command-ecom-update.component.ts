import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICommandEcom, CommandEcom } from 'app/shared/model/command-ecom.model';
import { CommandEcomService } from './command-ecom.service';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';
import { IAddressEcom } from 'app/shared/model/address-ecom.model';
import { AddressEcomService } from 'app/entities/address-ecom/address-ecom.service';

@Component({
  selector: 'jhi-command-ecom-update',
  templateUrl: './command-ecom-update.component.html'
})
export class CommandEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  userinfos: IUserInfoEcom[];

  addresses: IAddressEcom[];

  editForm = this.fb.group({
    id: [],
    status: [],
    userId: [],
    addressId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected commandService: CommandEcomService,
    protected userInfoService: UserInfoEcomService,
    protected addressService: AddressEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ command }) => {
      this.updateForm(command);
    });
    this.userInfoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUserInfoEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUserInfoEcom[]>) => response.body)
      )
      .subscribe((res: IUserInfoEcom[]) => (this.userinfos = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.addressService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IAddressEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IAddressEcom[]>) => response.body)
      )
      .subscribe((res: IAddressEcom[]) => (this.addresses = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(command: ICommandEcom) {
    this.editForm.patchValue({
      id: command.id,
      status: command.status,
      userId: command.userId,
      addressId: command.addressId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const command = this.createFromForm();
    if (command.id !== undefined) {
      this.subscribeToSaveResponse(this.commandService.update(command));
    } else {
      this.subscribeToSaveResponse(this.commandService.create(command));
    }
  }

  private createFromForm(): ICommandEcom {
    return {
      ...new CommandEcom(),
      id: this.editForm.get(['id']).value,
      status: this.editForm.get(['status']).value,
      userId: this.editForm.get(['userId']).value,
      addressId: this.editForm.get(['addressId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommandEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserInfoById(index: number, item: IUserInfoEcom) {
    return item.id;
  }

  trackAddressById(index: number, item: IAddressEcom) {
    return item.id;
  }
}

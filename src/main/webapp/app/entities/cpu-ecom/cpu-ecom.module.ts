import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { CpuEcomComponent } from './cpu-ecom.component';
import { CpuEcomDetailComponent } from './cpu-ecom-detail.component';
import { CpuEcomUpdateComponent } from './cpu-ecom-update.component';
import { CpuEcomDeletePopupComponent, CpuEcomDeleteDialogComponent } from './cpu-ecom-delete-dialog.component';
import { cpuRoute, cpuPopupRoute } from './cpu-ecom.route';

const ENTITY_STATES = [...cpuRoute, ...cpuPopupRoute];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CpuEcomComponent,
    CpuEcomDetailComponent,
    CpuEcomUpdateComponent,
    CpuEcomDeleteDialogComponent,
    CpuEcomDeletePopupComponent
  ],
  entryComponents: [CpuEcomDeleteDialogComponent]
})
export class ProjetEcomCpuEcomModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICpuEcom } from 'app/shared/model/cpu-ecom.model';

@Component({
  selector: 'jhi-cpu-ecom-detail',
  templateUrl: './cpu-ecom-detail.component.html'
})
export class CpuEcomDetailComponent implements OnInit {
  cpu: ICpuEcom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cpu }) => {
      this.cpu = cpu;
    });
  }

  previousState() {
    window.history.back();
  }
}

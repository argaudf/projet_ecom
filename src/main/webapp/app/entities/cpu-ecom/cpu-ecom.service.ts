import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICpuEcom } from 'app/shared/model/cpu-ecom.model';

type EntityResponseType = HttpResponse<ICpuEcom>;
type EntityArrayResponseType = HttpResponse<ICpuEcom[]>;

@Injectable({ providedIn: 'root' })
export class CpuEcomService {
  public resourceUrl = SERVER_API_URL + 'api/cpus';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/cpus';

  constructor(protected http: HttpClient) {}

  create(cpu: ICpuEcom): Observable<EntityResponseType> {
    return this.http.post<ICpuEcom>(this.resourceUrl, cpu, { observe: 'response' });
  }

  update(cpu: ICpuEcom): Observable<EntityResponseType> {
    return this.http.put<ICpuEcom>(this.resourceUrl, cpu, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICpuEcom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICpuEcom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICpuEcom[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

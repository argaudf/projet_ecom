import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICpuEcom } from 'app/shared/model/cpu-ecom.model';
import { CpuEcomService } from './cpu-ecom.service';

@Component({
  selector: 'jhi-cpu-ecom-delete-dialog',
  templateUrl: './cpu-ecom-delete-dialog.component.html'
})
export class CpuEcomDeleteDialogComponent {
  cpu: ICpuEcom;

  constructor(protected cpuService: CpuEcomService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.cpuService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'cpuListModification',
        content: 'Deleted an cpu'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-cpu-ecom-delete-popup',
  template: ''
})
export class CpuEcomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cpu }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CpuEcomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.cpu = cpu;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/cpu-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/cpu-ecom', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICpuEcom, CpuEcom } from 'app/shared/model/cpu-ecom.model';
import { CpuEcomService } from './cpu-ecom.service';

@Component({
  selector: 'jhi-cpu-ecom-update',
  templateUrl: './cpu-ecom-update.component.html'
})
export class CpuEcomUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    frequency: []
  });

  constructor(protected cpuService: CpuEcomService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ cpu }) => {
      this.updateForm(cpu);
    });
  }

  updateForm(cpu: ICpuEcom) {
    this.editForm.patchValue({
      id: cpu.id,
      name: cpu.name,
      frequency: cpu.frequency
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const cpu = this.createFromForm();
    if (cpu.id !== undefined) {
      this.subscribeToSaveResponse(this.cpuService.update(cpu));
    } else {
      this.subscribeToSaveResponse(this.cpuService.create(cpu));
    }
  }

  private createFromForm(): ICpuEcom {
    return {
      ...new CpuEcom(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      frequency: this.editForm.get(['frequency']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICpuEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}

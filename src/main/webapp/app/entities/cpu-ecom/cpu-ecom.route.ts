import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CpuEcom } from 'app/shared/model/cpu-ecom.model';
import { CpuEcomService } from './cpu-ecom.service';
import { CpuEcomComponent } from './cpu-ecom.component';
import { CpuEcomDetailComponent } from './cpu-ecom-detail.component';
import { CpuEcomUpdateComponent } from './cpu-ecom-update.component';
import { CpuEcomDeletePopupComponent } from './cpu-ecom-delete-dialog.component';
import { ICpuEcom } from 'app/shared/model/cpu-ecom.model';

@Injectable({ providedIn: 'root' })
export class CpuEcomResolve implements Resolve<ICpuEcom> {
  constructor(private service: CpuEcomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICpuEcom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CpuEcom>) => response.ok),
        map((cpu: HttpResponse<CpuEcom>) => cpu.body)
      );
    }
    return of(new CpuEcom());
  }
}

export const cpuRoute: Routes = [
  {
    path: '',
    component: CpuEcomComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'projetEcomApp.cpu.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CpuEcomDetailComponent,
    resolve: {
      cpu: CpuEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.cpu.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CpuEcomUpdateComponent,
    resolve: {
      cpu: CpuEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.cpu.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CpuEcomUpdateComponent,
    resolve: {
      cpu: CpuEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.cpu.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const cpuPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CpuEcomDeletePopupComponent,
    resolve: {
      cpu: CpuEcomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetEcomApp.cpu.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

import { Component, OnInit, OnDestroy } from '@angular/core';
import { JhiAlertService, JhiParseLinks, JhiEventManager } from 'ng-jhipster';
import { HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { Subscription } from 'rxjs';
import { AccountService } from 'app/core/auth/account.service';
import { IProductEcom } from 'app/shared/model/product-ecom.model';
import { ProductEcomService } from 'app/entities/product-ecom/product-ecom.service';
import { Options, ChangeContext } from 'ng5-slider';
import { CpuEcomService } from 'app/entities/cpu-ecom/cpu-ecom.service';
import { CpuEcom } from 'app/shared/model/cpu-ecom.model';

@Component({
  selector: 'jhi-catalogue-view',
  templateUrl: './catalogue-view.component.html',
  styleUrls: ['./catalogue-view.component.scss']
})
export class CatalogueViewComponent implements OnInit, OnDestroy {
  currentAccount: any;
  products: IProductEcom[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  currentSearch: string;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  cpus: CpuEcom[];
  minValue = 0;
  maxValue = 10000;
  options: Options = {
    floor: 0,
    ceil: 10000
  };

  image: String;

  constructor(
    protected productService: ProductEcomService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected cpuService: CpuEcomService,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
    /*this.cpus = [];*/
  }

  onUserChange(changeContext: ChangeContext): void {
    this.loadAll();
  }

  loadAll() {
    if (this.currentSearch) {
      this.productService
        .search({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<IProductEcom[]>) => this.paginateProducts(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      return;
    }
    this.productService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        'proPrice.greaterOrEqualThan': this.minValue,
        'proPrice.lessOrEqualThan': this.maxValue
      })
      .subscribe(
        (res: HttpResponse<IProductEcom[]>) => this.paginateProducts(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    /*
    this.cpuService
      .query()
      .subscribe(
        (res: HttpResponse<CpuEcom[]>) => (this.cpus = res.body), 
        (res: HttpErrorResponse) => this.onError(res.message)
      );*/
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
    window.scroll(0, 0);
  }

  transition() {
    this.router.navigate([this.getRoute()], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        search: this.currentSearch,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([this.getRoute()], {
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([this.getRoute()], {
      queryParams: {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInProducts();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IProductEcom) {
    return item.id;
  }

  registerChangeInProducts() {
    this.eventSubscriber = this.eventManager.subscribe('productListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateProducts(data: IProductEcom[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.products = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  protected getRoute(): String {
    return this.router.url.split('?')[0];
  }

  protected loadImage() {
    this.products[0].picture;
  }
}

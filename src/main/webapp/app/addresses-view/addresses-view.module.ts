import { NgModule } from '@angular/core';
import { AddressCardComponent } from 'app/address-card/address-card.component';
import { ProjetEcomSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [],
  imports: [ProjetEcomSharedModule],
  exports: []
})
export class AddressesViewModule {}

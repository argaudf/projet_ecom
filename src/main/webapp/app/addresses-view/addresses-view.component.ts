import { Component, OnInit, Input } from '@angular/core';
import { UserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { JhiAlertService } from 'ng-jhipster';
import { AddressEcomService } from 'app/entities/address-ecom/address-ecom.service';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AccountService } from 'app/core/auth/account.service';
import { UserService } from 'app/core/user/user.service';
import { Account } from 'app/core/user/account.model';
import { User } from 'app/core/user/user.model';
import { AddressEcom } from 'app/shared/model/address-ecom.model';

@Component({
  selector: 'jhi-addresses-view',
  templateUrl: './addresses-view.component.html',
  styleUrls: ['./addresses-view.component.scss']
})
export class AddressesViewComponent implements OnInit {
  addresses: AddressEcom[] = [];
  userId;
  @Input() showBackButton = true;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected addressService: AddressEcomService,
    protected userInfoService: UserInfoEcomService,
    protected accountService: AccountService,
    protected usersService: UserService
  ) {}

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    if (this.accountService.isAuthenticated()) {
      this.accountService.identity().then((account: Account) => {
        this.usersService
          .find(account.login)
          .subscribe(
            (res: HttpResponse<User>) => this.loadAccountsInfo(res.body.id),
            (res: HttpErrorResponse) => this.onError(res.message)
          );
      });
    }
  }

  loadAccountsInfo(userId) {
    this.userInfoService
      .query({
        'userId.equals': userId
      })
      .subscribe(
        (res: HttpResponse<UserInfoEcom[]>) => this.loadAddresses(res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadAddresses(accounts) {
    this.userId = accounts[0].id;
    this.addressService
      .query({
        'userId.equals': this.userId
      })
      .subscribe((res: HttpResponse<AddressEcom[]>) => (this.addresses = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  newAddr() {
    const newAddress = new AddressEcom();
    newAddress.userId = this.userId;
    this.addresses.push(newAddress);
  }

  back() {
    window.history.back();
  }
}

import { Route } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { AddressesViewComponent } from './addresses-view.component';

export const AddressesRoute: Route = {
  path: 'addresses',
  component: AddressesViewComponent,
  data: {
    authorities: [],
    pageTitle: 'addresses-view.title'
  },
  resolve: {
    pagingParams: JhiResolvePagingParams
  }
};

import { Route } from '@angular/router';

import { HomeComponent } from './home.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const HOME_ROUTE: Route = {
  path: '',
  component: HomeComponent,
  data: {
    authorities: [],
    pageTitle: 'home.title'
  },
  resolve: {
    pagingParams: JhiResolvePagingParams
  }
};

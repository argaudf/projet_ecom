import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { CatalogueViewComponent } from 'app/catalogue-view/catalogue-view.component';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Ng5SliderModule } from 'ng5-slider';

export const routes: Routes = [
  HOME_ROUTE,
  {
    path: 'catalogue',
    component: CatalogueViewComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    }
  }
];

@NgModule({
  imports: [ProjetEcomSharedModule, RouterModule.forChild(routes), Ng5SliderModule],
  declarations: [HomeComponent, CatalogueViewComponent]
})
export class ProjetEcomHomeModule {}

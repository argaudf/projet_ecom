import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { ProjetEcomSharedModule } from 'app/shared/shared.module';
import { ProjetEcomCoreModule } from 'app/core/core.module';
import { ProjetEcomAppRoutingModule } from './app-routing.module';
import { ProjetEcomHomeModule } from './home/home.module';
import { ProjetEcomEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { ShopcartComponent } from './shopcart/shopcart.component';
import { PaiementComponent } from './paiement/paiement.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { AddressesViewComponent } from './addresses-view/addresses-view.component';
import { Ng5SliderModule } from 'ng5-slider';
import { AddressesViewModule } from './addresses-view/addresses-view.module';
import { ProjetEcomAccountModule } from './account/account.module';
import { ConsulterCommandesComponent } from './consulter-commandes/consulter-commandes.component';

@NgModule({
  imports: [
    BrowserModule,
    ProjetEcomSharedModule,
    ProjetEcomCoreModule,
    ProjetEcomHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ProjetEcomEntityModule,
    ProjetEcomAppRoutingModule,
    Ng5SliderModule,
    ProjetEcomAccountModule
  ],
  declarations: [
    JhiMainComponent,
    NavbarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    ShopcartComponent,
    PaiementComponent,
    ProductViewComponent,
    ConsulterCommandesComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true
    }
  ],
  bootstrap: [JhiMainComponent]
})
export class ProjetEcomAppModule {}

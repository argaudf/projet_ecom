import { Component, OnInit, Input } from '@angular/core';
import { IAddressEcom, AddressEcom } from 'app/shared/model/address-ecom.model';
import { FormBuilder } from '@angular/forms';
import { IUserInfoEcom } from 'app/shared/model/user-info-ecom.model';
import { JhiAlertService } from 'ng-jhipster';
import { AddressEcomService } from 'app/entities/address-ecom/address-ecom.service';
import { UserInfoEcomService } from 'app/entities/user-info-ecom/user-info-ecom.service';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

/* eslint-disable no-console */

@Component({
  selector: 'jhi-address-card',
  templateUrl: './address-card.component.html',
  styleUrls: ['./address-card.component.scss']
})
export class AddressCardComponent implements OnInit {
  isSaving: boolean;

  @Input() address: AddressEcom;
  @Input() counter;

  userinfos: IUserInfoEcom[];

  editForm = this.fb.group({
    id: [],
    addressComplete: [],
    receiveName: [],
    receiveTel: [],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected addressService: AddressEcomService,
    protected userInfoService: UserInfoEcomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.updateForm(this.address);
    this.userInfoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUserInfoEcom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUserInfoEcom[]>) => response.body)
      )
      .subscribe((res: IUserInfoEcom[]) => (this.userinfos = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(address: IAddressEcom) {
    this.editForm.patchValue({
      id: address.id,
      addressComplete: address.addressComplete,
      receiveName: address.receiveName,
      receiveTel: address.receiveTel,
      userId: address.userId
    });
  }

  previousState() {
    this.updateForm(this.address);
  }

  save() {
    this.isSaving = true;
    const address = this.createFromForm();
    if (address.id !== undefined) {
      this.subscribeToSaveResponse(this.addressService.update(address));
    } else {
      this.subscribeToSaveResponse(this.addressService.create(address));
    }
  }

  private createFromForm(): IAddressEcom {
    return {
      ...new AddressEcom(),
      id: this.editForm.get(['id']).value,
      addressComplete: this.editForm.get(['addressComplete']).value,
      receiveName: this.editForm.get(['receiveName']).value,
      receiveTel: this.editForm.get(['receiveTel']).value,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAddressEcom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.jhiAlertService.success(null, null, null);
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserInfoById(index: number, item: IUserInfoEcom) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
